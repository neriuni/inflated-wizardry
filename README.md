# はじめに
inflated-wizardryは、pyxelライブラリを用いて作ったWizardryの劣化クローンである。Pythonの勉強のために作成。

# 仕様
- 狂王の訓練場に相当
- 抵抗周りのルールはD&D1eではなく、D&D3eの劣化版
- 装備品以外のアイテムは共用

# 実行環境
pyxel2で動作する。開発環境は以下の通り
- python 3.12.2
- pyxel 2.0.7

# 実行方法
web版等は現状なし。不便だと思われるが、以下で実行
- pyxel run main
