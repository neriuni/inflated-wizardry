import gfx

import rule
import battle

data = {
    'arcane':[
        # level 1 mage spell
        {
            'halito':{
                'name':'HALITO',
                'namejp':'ハリト',
                'where':'combat',
                'type':'dd',
                'target':'single',
                'roll':(1,8,0),
                'resist':'fire',
                'message':'aは-掌から火球を放った。-',
                'notwork':'dは,-呪文を邪魔した。-',
            },
            'mogref':{
                'name':'MOGREF',
                'namejp':'モグレフ',
                'where':'combat',
                'type':'ac',
                'target':'self',
                'roll':(0,0,2),
                'resist':'unresist',
                'message':'aは-守られた。-',
            },
            'katino':{
                'name':'KATINO',
                'namejp':'カティノ',
                'where':'combat',
                'type':'sleep',
                'target':'group',
                'roll':(0,0,0),
                'resist':'sleep',
                'message':'dを-眠りに誘った。-',
                'notwork':'dは,-呪文を邪魔した。-',
                'success':'dは-眠ってしまった。-',
                'fail':'dは-眠らなかった。-',
            },
            'dumapic':{
                'name':'DUMAPIC',
                'namejp':'デュマピック',
                'where':'camp',
                'type':'loc',
                'target':'self',
                'roll':(0,0,0),
                'resist':'unresist',
            },
        },
        # level 2 mage spell
        {
            'dilto':{
                'name':'DILTO',
                'namejp':'ディルト',
                'where':'combat',
                'type':'ac',
                'target':'group',
                'roll':(0,0,-2),
                'resist':'unresist',
                'message':'dを-暗闇で包んだ。-',
            },
            'sopic':{
                'name':'SOPIC',
                'namejp':'ソピック',
                'where':'combat',
                'type':'ac',
                'target':'self',
                'roll':(0,0,4),
                'resist':'unresist',
                'message':'aは-透明になった。-',
            },
        },
        # level 3 mage spell
        {
            'mahalito':{
                'name':'MAHALITO',
                'namejp':'マハリト',
                'where':'combat',
                'type':'dd',
                'target':'group',
                'roll':(4,6,0),
                'resist':'fire',
                'message':'aは-爆発を発した。-',
                'notwork':'dは,-呪文を邪魔した。-',
            },
            'molito':{
                'name':'MOLITO',
                'namejp':'モリト',
                'where':'combat',
                'type':'dd',
                'target':'group',
                'roll':(3,6,0),
                'resist':'fire',
                'message':'aは-火花の嵐を起こした。-',
                'notwork':'dは,-呪文を邪魔した。-',
            },
        },
        # level 4 mage spell
        {
            'morlis':{
                'name':'MORLIS',
                'namejp':'モーリス',
                'where':'combat',
                'type':'ac',
                'target':'group',
                'roll':(0,0,-4),
                'resist':'unresist',
                'message':'dを-驚かせた。-',
            },
            'dalto':{
                'name':'DALTO',
                'namejp':'ダルト',
                'where':'combat',
                'type':'dd',
                'target':'group',
                'roll':(6,6,0),
                'resist':'cold',
                'message':'aは-冷気の嵐を-起こした。-',
                'notwork':'dは,-呪文を邪魔した。-',
            },
            'lahalito':{
                'name':'LAHALITO',
                'namejp':'ラハリト',
                'where':'combat',
                'type':'dd',
                'target':'group',
                'roll':(6,6,0),
                'resist':'fire',
                'message':'aは-巨大な炎の嵐を-起こした。-',
                'notwork':'dは,-呪文を邪魔した。-',
            },
        },
        # level 5 mage spell
        {
            'mamorlis':{
                'name':'MAMORLIS',
                'namejp':'マモーリス',
                'where':'combat',
                'type':'ac',
                'target':'all',
                'roll':(0,0,-4),
                'resist':'unresist',
                'message':'dを-驚かせた。-',
            },
            'makanito':{
                'name':'MAKANITO',
                'namejp':'マカニト',
                'where':'combat',
                'type':'dead',
                'level':8,
                'target':'all',
                'roll':(0,0,0),
                'resist':'death',
                'message':'aは-空気を汚染させた。-',
                'notwork':'dは,-呪文を邪魔した。-',
                'success':'dは,-窒息した。-',
                'fail':'dには,-呪文が効かなかった。-',
            },
            'madalto':{
                'name':'MADALTO',
                'namejp':'マダルト',
                'where':'combat',
                'type':'dd',
                'target':'group',
                'roll':(8,6,0),
                'resist':'cold',
                'message':'aは-凍てつく-冷気の嵐を-起こした-',
                'notwork':'dは,-呪文を邪魔した。-',
            },
        },
        # level 6 mage spell
        {
            'lakanito':{
                'name':'LAKANITO',
                'namejp':'マカニト',
                'where':'combat',
                'type':'dead',
                'target':'group',
                'roll':(0,0,0),
                'resist':'death',
                'message':'aは-周囲を-真空状態にした。-',
                'notwork':'dは,-呪文を邪魔した。-',
                'success':'dは,-窒息した。-',
                'fail':'dには,-呪文が効かなかった-',
            },
            'zilwan':{
                'name':'ZILWAN',
                'namejp':'ジルワン',
                'where':'combat',
                'type':'dd',
                'race':'undead',
                'target':'single',
                'roll':(10,200,0),
                'resist':'death',
                'message':'dを-消滅させようとした。-',
                'notwork':'dは,-呪文を邪魔した。-',
            },
            'masopic':{
                'name':'MASOPIC',
                'namejp':'マソピック',
                'where':'combat',
                'type':'ac',
                'target':'mygroup',
                'roll':(0,0,4),
                'resist':'unresist',
                'message':'aの-パーティは-透明になった。-',
            },
            'haman':{
                'name':'HAMAN',
                'namejp':'ハマン',
                'where':'combat',
                'type':'lesserwish',
                'target':'self',
                'roll':(0,0,0),
                'resist':'unresist',
            },
        },
        # level 7 mage spell
        {
            'malor':{
                'name':'MALOR',
                'namejp':'マラー',
                'where':'combat camp',
                'type':'teleport',
                'target':'self',
                'roll':(0,0,0),
                'resist':'unresist',
            },
            'mahaman':{
                'name':'MAHAMAN',
                'namejp':'マハマン',
                'where':'combat',
                'type':'wish',
                'target':'self',
                'roll':(0,0,0),
                'resist':'unresist',
            },
            'tiltowait':{
                'name':'TILTOWAIT',
                'namejp':'ティルトウェイト',
                'where':'combat',
                'type':'dd',
                'target':'all',
                'roll':(10,15,0),
                'resist':'nuke',
                'message':'aは-核爆発を起こした。-',
                'notwork':'dは,-呪文を邪魔した。-',
            },
        },
    ],
    'divine':[
        # level 1 priest spell
        {
            'kalki':{
                'name':'KALKI',
                'namejp':'カルキ',
                'where':'combat',
                'type':'ac',
                'target':'mygroup',
                'roll':(0,0,1),
                'resist':'unresist',
                'message':'aは-パーティを-祝福した。-',
            },
            'dios':{
                'name':'DIOS',
                'namejp':'ディオス',
                'where':'combat camp',
                'type':'heal',
                'target':'member',
                'roll':(1,8,0),
                'resist':'unresist',
                'message':'dを-癒そうとした。-',
            },
            'badios':{
                'name':'BADIOS',
                'namejp':'バディオス',
                'where':'combat',
                'type':'dd',
                'target':'single',
                'roll':(1,8,0),
                'resist':'negative',
                'message':'aは-呪いの言葉を-放った。-',
                'notwork':'dは,-呪文を邪魔した。-',
            },
            'milwa':{
                'name':'MILWA',
                'namejp':'ミルワ',
                'where':'combat camp',
                'type':'light',
                'target':'self',
                'roll':(0,0,30),
                'resist':'unresist',
                'message':'aは-魔法の灯りを生んだ。-',
            },
            'porfic':{
                'name':'PORFIC',
                'namejp':'ポーフィック',
                'where':'combat',
                'type':'ac',
                'target':'self',
                'roll':(0,0,4),
                'resist':'unresist',
                'message':'aは-魔法の障壁を-発生させた。-',
            },
        },
        # level 2 priest spell
        {
            'matu':{
                'name':'MATU',
                'namejp':'マツ',
                'where':'combat',
                'type':'ac',
                'target':'mygroup',
                'roll':(0,0,2),
                'resist':'unresist',
                'message':'aは-パーティを-祝福した。-',
            },
            'calfo':{
                'name':'CALFO',
                'namejp':'カルフォ',
                'where':'chest',
                'type':'findtrap',
                'target':'self',
                'roll':(0,0,0),
                'resist':'unresist',
            },
            'manifo':{
                'name':'MANIFO',
                'namejp':'マニフォ',
                'where':'combat',
                'type':'sleep',
                'target':'group',
                'roll':(0,0,0),
                'resist':'sleep',
                'message':'dを-縛った。-',
                'notwork':'dは,-呪文を邪魔した。-',
                'success':'dは-動けなくなった。-',
                'fail':'dには-呪文が効かなかった。-',
            },
            'montino':{
                'name':'MONTINO',
                'namejp':'モンティノ',
                'where':'combat',
                'type':'silence',
                'target':'group',
                'roll':(0,0,0),
                'resist':'silence',
                'message':'dを-静寂で包み込んだ。-',
                'notwork':'dは,-呪文を邪魔した。-',
                'success':'dは-沈黙した。-',
                'fail':'dには-呪文が効かなかった。-',
            },
        },
        # level 3 priest spell
        {
            'lomilwa':{
                'name':'LOMILWA',
                'namejp':'ロミルワ',
                'where':'combat camp',
                'type':'greaterlight',
                'target':'self',
                'roll':(0,0,0),
                'resist':'unresist',
                'message':'aは-魔法の灯りを生んだ。-',
            },
            'dialko':{
                'name':'DIALKO',
                'namejp':'ディアルコ',
                'where':'combat camp',
                'type':'cure',
                'target':'member',
                'roll':(0,0,0),
                'resist':'unresist',
                'ext':'paralysis',
                'message':'dを-治療した。-',
                'success':'dは-動けるようになった。-',
            },
            'latumapic':{
                'name':'LATUMAPIC',
                'namejp':'ラツマピック',
                'where':'combat camp',
                'type':'identify',
                'target':'self',
                'roll':(0,0,0),
                'resist':'unresist',
                'message':'aは-怪物の正体が-わかった。-',
            },
            'bamatu':{
                'name':'BAMATU',
                'namejp':'バマツ',
                'where':'combat',
                'type':'ac',
                'target':'mygroup',
                'roll':(0,0,4),
                'resist':'unresist',
                'message':'aは-パーティを-祝福した。-',
            },
        },
        # level 4 priest spell
        {
            'dial':{
                'name':'DIAL',
                'namejp':'ディアル',
                'where':'combat camp',
                'type':'heal',
                'target':'member',
                'roll':(2,8,0),
                'resist':'unresist',
                'message':'dを-癒そうとした。-',
            },
            'badial':{
                'name':'BADIAL',
                'namejp':'バディアル',
                'where':'combat',
                'type':'dd',
                'target':'single',
                'roll':(2,8,0),
                'resist':'negative',
                'message':'aは-呪いの言葉を-放った。-',
                'notwork':'dは,-呪文を邪魔した。-',
            },
            'latumofis':{
                'name':'LATUMOFIS',
                'namejp':'ラツモフィス',
                'where':'combat camp',
                'type':'cure',
                'target':'member',
                'roll':(0,0,0),
                'resist':'unresist',
                'ext':'poison',
                'message':'dを-治療した。-',
                'success':'dは-解毒された。-',
            },
            'maporfic':{
                'name':'MAPORFIC',
                'namejp':'マポーフィック',
                'where':'combat camp',
                'type':'greaterac',
                'target':'mygroup',
                'roll':(0,0,-2),
                'resist':'unresist',
                'message':'aは-魔法の盾を作った。-',
            },
        },
        # level 5 priest spell
        {
            'dialma':{
                'name':'DIALMA',
                'namejp':'ディアルマ',
                'where':'combat camp',
                'type':'heal',
                'target':'member',
                'roll':(3,8,0),
                'resist':'unresist',
                'message':'dを-癒そうとした。-',
            },
            'badialma':{
                'name':'BADIALMA',
                'namejp':'バディアルマ',
                'where':'combat',
                'type':'dd',
                'target':'single',
                'roll':(3,8,0),
                'resist':'negative',
                'message':'aは-呪いの言葉を-放った。-',
                'notwork':'dは,-呪文を邪魔した。-',
            },
            'litokan':{
                'name':'LITOKAN',
                'namejp':'リトカン',
                'where':'combat',
                'type':'dd',
                'target':'group',
                'roll':(3,8,0),
                'resist':'fire',
                'message':'aは-火柱を-発生させた。-',
                'notwork':'dは,-呪文を邪魔した。-',
            },
            'kandi':{
                'name':'KANDI',
                'namejp':'カンディ',
                'where':'camp',
                'type':'findcorpse',
                'target':'self',
                'roll':(0,0,0),
                'resist':'unresist',
            },
            'di':{
                'name':'DI',
                'namejp':'ディ',
                'where':'combat camp',
                'type':'lesserresurrect',
                'target':'member',
                'roll':(0,0,0),
                'resist':'unresist',
                'message':'dを-蘇生させようとした。-',
                'success':'dは-生き返った!-',
                'fail':'dは-生き返らなかった!-',
            },
            'badi':{
                'name':'BADI',
                'namejp':'バディ',
                'where':'combat',
                'type':'dead',
                'target':'single',
                'roll':(0,0,0),
                'resist':'death',
                'message':'aは-呪いの言葉を-放った。-',
                'notwork':'dは,-呪文を邪魔した。-',
                'success':'dは-死んだ!-',
                'fail':'dには-呪文が効かなかった。-',
            },
        },
        # level 6 priest spell
        {
            'lorto':{
                'name':'LORTO',
                'namejp':'ロルト',
                'where':'combat',
                'type':'dd',
                'target':'group',
                'roll':(6,6,0),
                'resist':'physical',
                'message':'aは-真空の刃を-生み出した。-',
                'notwork':'dは,-呪文を邪魔した。-',
            },
            'madi':{
                'name':'MADI',
                'namejp':'マディ',
                'where':'combat camp',
                'type':'healall',
                'target':'member',
                'roll':(0,0,0),
                'resist':'unresist',
                'message':'dを-癒そうとした。-',
            },
            'mabadi':{
                'name':'MABADI',
                'namejp':'マバディ',
                'where':'combat',
                'type':'steallife',
                'target':'single',
                'roll':(1,8,0),
                'resist':'negative',
                'message':'aは-生命を-吸い取った。-',
                'notwork':'dは,-呪文を邪魔した。-',
            },
            'loktofeit':{
                'name':'LOKTOFEIT',
                'namejp':'ロクトフェイト',
                'where':'combat camp',
                'type':'gohome',
                'target':'self',
                'roll':(0,0,0),
                'resist':'unresist',
            },
        },
        # level 7 priest spell
        {
            'malikto':{
                'name':'MALIKTO',
                'namejp':'マリクト',
                'where':'combat',
                'type':'dd',
                'target':'all',
                'roll':(6,12,0),
                'resist':'positive',
                'message':'aは-神々の怒りを-呼び起こした。-',
                'notwork':'dは,-呪文を邪魔した。-',
            },
            'kadorto':{
                'name':'KADORTO',
                'namejp':'カドルト',
                'where':'combat camp',
                'type':'resurrect',
                'target':'member',
                'roll':(0,0,0),
                'resist':'unresist',
                'message':'dを-蘇生させようとした。-',
                'success':'dは-生き返った!-',
                'fail':'dは-生き返らなかった!-',
            },
        }
    ]
}

BOOKSTR = {
    'arcane':'魔法使い',
    'divine':'僧侶',
}

def getbyname(name, type='arcane', lvl=-1):
    global data
    if lvl != -1:
        if name in data[type][lvl]:
            return data[type][lvl][name]

    for spl in data[type]:
        if name in spl:
            return spl[name]
    return None

def schoolbyname(name):
    schooltype = ['arcane', 'divine']
    for school in schooltype:
        for lvl, spllist in enumerate(data[school]):
            if name in spllist:
                return (lvl, school)
    return (-1, None)

def selectspell(c, where='battle'):
    global BOOKSTR
    isarcane = False
    isdivine = False
    for sp in c['arcane']:
        if sp > 0:
            isarcane = True
            break
    for sp in c['divine']:
        if sp > 0:
            isdivine = True
            break
    if not isarcane and not isdivine:
        return False
    if isarcane:
        school = 'arcane'
    else:
        school = 'divine'
    spllvl = 0

    dlist = gfx.qgetlist()
    chkey = ['a', 'b', 'up', 'down', 'left', 'right']
    pos = 0
    while True:
        gfx.qsendlist(dlist)
        # spell book
        gfx.qdraw(gfx.frame, (112, 64, 18, 14,True))
        gfx.qdraw(gfx.text, (112+8, 80, f"{'どの呪文を唱えますか？':^14}"))
        bk = BOOKSTR[school]
        gfx.qdraw(gfx.text, (120, 88, f" {bk:>4}の呪文 L{spllvl+1}({c[school][spllvl]})"))
        spllst = [getbyname(s, school, spllvl)['name'] for s in c[school+'list'][spllvl]]
        for i, s in enumerate(spllst):
            gfx.qdraw(gfx.text, (160, 104+i*8, s))

        key = gfx.getkey(chkey)
        if key == 'a':
            pos = 0
            while True:
                gfx.qsendlist(dlist)
                # spell book
                gfx.qdraw(gfx.frame, (112, 64, 18, 14,True))
                gfx.qdraw(gfx.text, (112+8, 80, f"{'どの呪文を唱えますか？':^14}"))
                bk = BOOKSTR[school]
                gfx.qdraw(gfx.text, (120, 88, f" {bk:>4}の呪文 L{spllvl+1}({c[school][spllvl]})"))
                spllst = [getbyname(s, school, spllvl)['name'] for s in c[school+'list'][spllvl]]
                for i, s in enumerate(spllst):
                    gfx.qdraw(gfx.text, (160, 104+i*8, s))
                key, pos = gfx.select(152, 104, len(spllst), pos)
                if key == 'a':
                    spl = getbyname(c[school+'list'][spllvl][pos], school)
                    if where in spl['where']:
                        c['casting'] = getbyname(c[school+'list'][spllvl][pos], school)
                        return True
                elif key == 'b':
                    break
        elif key == 'b':
            if 'casting' in c:
                c['casting'].pop()
            return False
        elif key == 'up':
            spllvl -= 1
            if spllvl < 0:
                spllvl = c[school+'maxlvl'] - 1
        elif key == 'down':
            spllvl += 1
            if spllvl == c[school+'maxlvl']:
                spllvl = 0
        elif key == 'right' or 'left':
            if school == 'arcane' and isdivine:
                school = 'divine'
                spllvl = 0
            elif school == 'divine' and isarcane:
                school = 'arcane'
                spllvl = 0

#### spell casting
def cast(cmd, spl):
    if not rule.manacheck(cmd, spl):
        ftxt = f"{cmd['name']}は-{spl['name']}を-唱えようとした。"
        ftxt += f"-しかし,-呪文の詠唱に-失敗した。"
        gfx.log(ftxt, True, True)
        gfx.getkey(['a', 'b'], 4)
        return

    casttxt = f"{cmd['name']}は-{spl['name']}を-唱えた。"
    if spl['target'] == 'single':
        t = None
        if 'pc' in cmd:
            for tm in battle.monster[cmd['target']]:
                if rule.status.istreatasdead(tm) == False:
                    if tm['hp'] > 0:
                        t = tm
                        break
        elif 'mob' in cmd:
            tm = rule.group.member[cmd['target']]
            if rule.status.isdead(tm) == False:
                if tm['hp'] > 0:
                    t = tm

        if t is None:
            return
        else:
            grp = [t]
    elif spl['target'] == 'group':
        if 'pc' in cmd:
            grp = battle.crowd.member[cmd['target']]
        elif 'mob' in cmd:
            grp = rule.group.member
    elif spl['target'] == 'all':
        if 'pc' in cmd:
            grp = []
            for g in battle.crowd.member:
                grp += g
        elif 'mob' in cmd:
            grp = rule.group.member
    elif spl['target'] == 'self':
        grp = [cmd]
    elif spl['target'] == 'mygroup':
        if 'pc' in cmd:
            grp = rule.group.member
        elif 'mob' in cmd:
            grp = battle.battle.monster[cmd['pos']]
    elif spl['target'] == 'member':
        if 'pc' in cmd:
            grp = [rule.group.member[cmd['target']]]
        elif 'mob' in cmd:
            grp = [cmd]
    else:
        return
    #print(f"spellcasting {cmd['name']}-{spl['name']} tt:{cmd['target']} st:{spl['target']} ")
    #print(grp)
    dlist = gfx.qgetlist()

    casted = False
    if spl['type'] == 'dd':
        for d in grp:
            if rule.status.isdead(d): continue
            gfx.qsendlist(dlist)
            if casted == False: casttxt += f"-{rule.addwho(cmd['name'], d['name'], spl['message'])}"
            dmg = rule.dice.roll(spl['roll']) + cmd[cmd['command']+'power']
            if rule.dice.vsroll(cmd, d, cmd['command']+'power', rule.RESIST2SAVING[spl['resist']]) == False:
                dmg = dmg//2
            if 'protect' in d and spl['resist'] in d['protect']:
                    dmg = dmg//2
            if 'level' in spl and spl['level'] < d['level']:
                    dtxt = f"-{rule.addwho(cmd['name'], d['name'], spl['nowork'])}"
            elif 'race' in spl and spl['race'] != d['race']:
                    dtxt = f"-{rule.addwho(cmd['name'], d['name'], spl['nowork'])}"
            else:
                dtxt = f"{d['name']}は-{dmg}のダメージを-うけた。-"
                d['hp'] -= dmg
                if rule.status.isdead(d) == False:
                    if d['hp'] < 0:
                        d['dead'] = True
                        battle.addexp(d)
                        dtxt += f"{d['name']}{rule.PROCSTR['dead']}"
            gfx.log(casttxt+'+'+dtxt, True, True)
            casted = True
            # wait for key
            gfx.getkey(['a', 'b'], 4)
            
    elif spl['type'] == 'ac':
        for d in grp:
            if rule.status.isdead(d): continue
            dmg = rule.dice.roll(spl['roll'])
            if rule.dice.vsroll(cmd, d, spl['resist']) == False:
                dmg = dmg//2
            if 'shield' in d:
                d['shield'] += dmg
            else:
                d['shield'] = dmg
            rule.refresh(d)
            casted = True
        if casted:
            casttxt += f"-{rule.addwho(cmd['name'], d['name'], spl['message'])}"
            gfx.log(casttxt, True, True)
        # wait for key
        gfx.getkey(['a', 'b'], 4)
    elif spl['type'] == 'sleep' or spl['type'] == 'silence' or spl['type'] == 'dead':
        for d in grp:
            if rule.status.isdead(d): continue
            gfx.qsendlist(dlist)
            if casted == False: casttxt += f"-{rule.addwho(cmd['name'], d['name'], spl['message'])}"
            #print(f"{cmd['name']} to {d['name']} - {spl['type']}")
            if rule.dice.vsroll(cmd, d, cmd['command']+'power', rule.RESIST2SAVING[spl['resist']]) == False:
                #dtxt = f"{d['name']}は-{rule.PROCSTR[spl['type']]}"
                dtxt = f"-{rule.addwho(cmd['name'], d['name'], spl['fail'])}"
            elif 'protect' in d and spl['resist'] in d['protect']:
                dtxt = f"-{rule.addwho(cmd['name'], d['name'], spl['nowork'])}"
            elif 'level' in spl and spl['level'] < d['level']:
                dtxt = f"-{rule.addwho(cmd['name'], d['name'], spl['nowork'])}"
            elif 'race' in spl and spl['race'] != d['race']:
                dtxt = f"-{rule.addwho(cmd['name'], d['name'], spl['nowork'])}"
            else:
                #dtxt = f"{d['name']}は-{rule.PROCSTR[spl['type']]}"
                dtxt = rule.addwho(cmd['name'], d['name'], spl['success'])
                if spl['type'] == 'dead':
                    battle.addexp(d)
                    d[spl['type']] = True
                else:
                    d[spl['type']] = rule.dice.lastrate
            gfx.log(casttxt+'+'+dtxt, True, True)
            casted = True
            # wait for key
            gfx.getkey(['a', 'b'], 4)
    elif spl['type'] == 'cure':
        for d in grp:
            if spl['ext'] in d:
                gfx.qsendlist(dlist)
                if casted == False: casttxt += f"-{rule.addwho(cmd['name'], d['name'], spl['message'])}"
                if 'level' in spl and spl['level'] < d['level']:
                    dtxt = f"-{rule.addwho(cmd['name'], d['name'], spl['nowork'])}"
                elif 'race' in spl and spl['race'] != d['race']:
                    dtxt = f"-{rule.addwho(cmd['name'], d['name'], spl['nowork'])}"
                else:
                    #dtxt = f"{d['name']}は-癒やされた。"
                    dtxt = rule.addwho(cmd['name'], d['name'], spl['success'])
                    d.pop(spl['ext'])
                gfx.log(casttxt+'+'+dtxt, True, True)
                casted = True
                # wait for key
                gfx.getkey(['a', 'b'], 4)
    elif spl['type'] == 'light' or spl['type'] == 'greaterlight':
        if 'pc' in cmd:
            if spl['type'] == 'greaterlight':
                rule.group.buff['light'] = 32000
            else:
                ld = rule.dice.roll((1,15,30))
                if 'light' not in rule.group.buff:
                    rule.group.buff['light'] = 0
                if rule.group.buff['light'] < ld: rule.group.buff['light'] = ld
            gfx.log(casttxt+'+'+f"{rule.addwho(cmd['name'], None, spl['message'])}", True, True)
            casted = True
            # wait for key
            gfx.getkey(['a', 'b'], 4)
    elif spl['type'] == 'identify':
        if 'pc' in cmd:
            rule.group.buff['identify'] = True
            gfx.log(casttxt+'+'+f"{rule.addwho(cmd['name'], None, spl['message'])}", True, True)
            casted = True
            # wait for key
            gfx.getkey(['a', 'b'], 4)
    elif spl['type'] == 'heal':
        dlist = gfx.qgetlist()
        for d in grp:
            if rule.status.isdead(d): continue
            gfx.qsendlist(dlist)
            if casted == False: casttxt += f"-{rule.addwho(cmd['name'], d['name'], spl['message'])}"
            dmg = rule.dice.roll(spl['roll']) + cmd[cmd['command']+'power']
            if d['race'] == 'undead':
                if rule.dice.vsroll(cmd, d, 'positive') == False:
                    dmg = dmg//2
                if 'protect' in d and 'positive' in d['protect']:
                    dmg = dmg//2
                if 'level' in spl and spl['level'] < d['level']:
                    dtxt = f"-{rule.addwho(cmd['name'], d['name'], spl['nowork'])}"
                elif 'race' in spl and spl['race'] != d['race']:
                    dtxt = f"-{rule.addwho(cmd['name'], d['name'], spl['nowork'])}"
                else:
                    d['hp'] -= dmg
                    dtxt = f"{d['name']}は-{dmg}のダメージを-うけた。-"
                    if rule.status.isdead(d) == False:
                        if d['hp'] < 0:
                            d['dead'] = True
                            dtxt += f"{d['name']}{rule.PROCSTR['dead']}"
            else:
                d['hp'] += dmg
                if d['hp'] > d['hpmax']:
                    d['hp'] = d['hpmax']
                    dtxt = f"{d['name']}は-全快した。"
                else:
                    dtxt = f"{d['name']}は-{dmg}ポイント回復した。"
            gfx.log(casttxt+'+'+dtxt, True, True)
            casted = True
            # wait for key
            gfx.getkey(['a', 'b'], 4)
    elif spl['type'] == 'greaterac':
        casttxt += f"-{rule.addwho(cmd['name'], None, spl['message'])}"
        if 'pc' in cmd:
            rule.group.buff['shield'] = rule.dice.roll(spl['roll'])
            for d in rule.group.member:
                rule.refresh(d)
        # wait for key
        gfx.getkey(['a', 'b'], 4)
    elif spl['type'] == 'lesserresurrect':
        for d in grp:
            if 'lost' in d: continue
            if 'ashed' in d: continue
            if 'dead' in d:
                gfx.qsendlist(dlist)
                if 'level' in spl and spl['level'] < d['level']:
                    dtxt = f"-{rule.addwho(cmd['name'], d['name'], spl['nowork'])}"
                elif 'race' in spl and spl['race'] != d['race']:
                    dtxt = f"-{rule.addwho(cmd['name'], d['name'], spl['nowork'])}"
                else:
                    if rule.dice.check(d['vit']*4) and d['vit'] > 3:
                        d['vid'] -= 1
                        d['age'] += rule.dice.roll((1, 52, 0))
                        dtxt = f"-{rule.addwho(cmd['name'], d['name'], spl['success'])}"
                        if 'dead' in d: d.pop('dead')
                        d['hp'] = 1
                    elif 'dead' in d:
                            d['ashed'] = True
                    dtxt = f"-{rule.addwho(cmd['name'], d['name'], spl['fail'])}"
                    if casted == False: casttxt += f"-{rule.addwho(cmd['name'], d['name'], spl['message'])}"
                gfx.log(casttxt+'+'+dtxt, True, True)
                casted = True
                # wait for key
                gfx.getkey(['a', 'b'], 4)
    elif spl['type'] == 'resurrect':
        for d in grp:
            if 'lost' in d: continue
            if 'dead' in d or 'ashed' in d:
                gfx.qsendlist(dlist)
                if 'level' in spl and spl['level'] < d['level']:
                    dtxt = f"-{rule.addwho(cmd['name'], d['name'], spl['nowork'])}"
                elif 'race' in spl and spl['race'] != d['race']:
                    dtxt = f"-{rule.addwho(cmd['name'], d['name'], spl['nowork'])}"
                else:
                    if rule.dice.check(d['vit']*4) and d['vit'] > 3:
                        d['vid'] -= 1
                        d['age'] += rule.dice.roll((1, 52, 0))
                        dtxt = f"-{rule.addwho(cmd['name'], d['name'], spl['success'])}"
                        if 'dead' in d: d.pop('dead')
                        if 'ashed' in d: d.pop('ashed')
                        d['hp'] = d['hpmax']
                    else:
                        if 'ashed' in d:
                            d['lost'] = True
                        elif 'dead' in d:
                            d['ashed'] = True
                        dtxt = f"-{rule.addwho(cmd['name'], d['name'], spl['fail'])}"
                    if casted == False: casttxt += f"-{rule.addwho(cmd['name'], d['name'], spl['message'])}"
                gfx.log(casttxt, dtxt, True, True)
                casted = True
                # wait for key
                gfx.getkey(['a', 'b'], 4)
    elif spl['type'] == 'healall':
        for d in grp:
            if d['race'] == 'undead':
                continue
            else:
                gfx.qsendlist(dlist)
                casttxt += f"-{rule.addwho(cmd['name'], d['name'], spl['message'])}"
                for mc in rule.MADICURE:
                    if mc in d:
                        if 'level' in spl and spl['level'] < d['level']:
                            dtxt = f"-{rule.addwho(cmd['name'], d['name'], spl['nowork'])}"
                        elif 'race' in spl and spl['race'] != d['race']:
                            dtxt = f"-{rule.addwho(cmd['name'], d['name'], spl['nowork'])}"
                        else:
                            d.pop(mc)
                d['hp'] = d['hpmax']
                dtxt = f"{d['name']}は-全快した。"
                gfx.log(casttxt+'+'+dtxt, True, True)
                casted = True
                # wait for key
                gfx.getkey(['a', 'b'], 4)
    elif spl['type'] == 'steallife':
        for d in grp:
            if rule.status.isdead(d): continue
            gfx.qsendlist(dlist)
            casttxt += f"-{rule.addwho(cmd['name'], d['name'], spl['message'])}"
            dmg = rule.dice.roll(spl['roll'])
            if d['race'] == 'undead':
                d['hp'] = d['hpmax']
                dtxt = f"{d['name']}は-全快した。"
            else:
                dmg = rule.dice.roll(spl['roll']) - cmd[cmd['command']]
                if dmg < 0: dmg = 1
                if rule.dice.vsroll(cmd, d, spl['resist']) == False:
                    dtxt = f"-{rule.addwho(cmd['name'], d['name'], spl['fail'])}"
                elif 'protect' in d and spl['resist'] in d['protect']:
                    dtxt = f"-{rule.addwho(cmd['name'], d['name'], spl['nowork'])}"
                elif 'level' in spl and spl['level'] < d['level']:
                    dtxt = f"-{rule.addwho(cmd['name'], d['name'], spl['nowork'])}"
                elif 'race' in spl and spl['race'] != d['race']:
                    dtxt = f"-{rule.addwho(cmd['name'], d['name'], spl['nowork'])}"
                else:
                    d['hp'] = dmg
                    if d['hp'] > d['hpmax']:
                        d['hp'] = d['hpmax']
                        txt = None
                    dtxt = f"{d['name']}は{d['hpmax'] - d['hp']}のダメージをうけた。"
            gfx.log(casttxt+'+'+dtxt, True, True)
            casted = True
            # wait for key
            gfx.getkey(['a', 'b'], 4)
    elif spl['type'] == 'teleport':
        pass
    elif spl['type'] == 'gohome':
        pass
    elif spl['type'] == 'wish' or spl['type'] == 'lesserwish':
        pass
    else:
        # other spells like 'loc'. do nothing
        pass
    if casted == False:
        gfx.log(casttxt, True, True)

