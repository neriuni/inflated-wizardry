import gfx

import rule

import spell
import dungeon
import monster
import item
import city

class crowd:
    member = []
    known = []

    monlines = []
    monnums = []
    def refresh():
        crowd.monlines = []
        crowd.monnums = []
        for i, mobs in enumerate(crowd.member):
            mobnum = 0
            mobok = 0
            for m in mobs:
                if rule.status.istreatasdead(m) == False:
                    mobnum += 1
                if rule.status.isactive(m):
                    mobok += 1
            crowd.monlines.append(" %d %s" % (mobnum,mobs[0]['name']))
            crowd.monnums.append("(%d)" % (mobok))
    def draw(update = False):
        #if update or len(crowd.monlines) == 0:
        #    crowd.refresh()
        crowd.refresh()
        gfx.qdraw(gfx.frame, (16,0,256//8-4,6+2,True))
        for i in range(len(crowd.monlines)):
            gfx.qdraw(gfx.text, (24, 8+i*8, crowd.monlines[i]))
            gfx.qdraw(gfx.text, (256-48, 8+i*8, crowd.monnums[i]))

    def show(no = 0):
        m = crowd.member[no][0]
        gfx.qdraw(gfx.frame, (0, 64, 14, 14,True))
        if crowd.known[no]:
            gfx.qdraw(gfx.show, (m['image'][0], 2, 64+2))
        else:
            gfx.qdraw(gfx.show, (m['image'][1], 2, 64+2))

    def select(number = len(member), pos = 0):
        x = 24
        y = 8
        number = len(crowd.member)
        dlist = gfx.qgetlist()
        chkey = ['a', 'b', 'up', 'down']
        while True:
            gfx.qsendlist(dlist)
            crowd.draw()
            crowd.show(pos)
            gfx.qdraw(gfx.draw, (x, y + pos*8, 10, 0), True)
            key = gfx.getkey(chkey)
            if key == 'a' or key == 'b':
                return (key, pos)
            elif key == 'up':
                pos -= 1
                if pos < 0:
                    pos = number -1
            elif key == 'down':
                pos += 1
                if pos == number:
                    pos = 0

    def moral():
        mlvl = 0
        for mg in crowd.member:
            for m in mg:
                if rule.status.isactive(m):
                    mlvl += m['level']
        return mlvl

    def targetpc(r):
        tgt = []
        for t in range(r):
            if rule.status.isdead(rule.group.member[t]) == False:
                tgt.append(t)
        return tgt[rule.dice.roll((1, len(tgt), -1))]

    MOBSPELLSEL = {
        'arcane':[['katino', 'halito'], ['dilto', 'halito'], ['mahalito', 'molito'], ['dalto', 'lahalito'], ['lahalito', 'madalto'], ['madalto', 'dalto'], ['tiltowait', 'tiltowait']],
        'divine':[['badios', 'badios'], ['montino', 'montino'], ['badial', 'montino'], ['badial', 'badial'], ['litokan', 'badi'], ['lorto', 'mabadi'], ['malikto', 'malikto']],
    }
    def selectspell(cmd):
        canarcane = False
        candivine = False
        if 'arcane' in cmd:
            canarcane = False
            for a in cmd['arcane']:
                if a > 0:
                    canarcane = True
                    break
        if 'divine' in cmd:
            candivine = False
            for a in cmd['divine']:
                if a > 0:
                    candivine = True
                    break
        elif canarcane == False:
            return False
        doarcane = False
        dodivine = False

        #print(f"{cmd['name']} {canarcane} {candivine}")

        # Is the mob going to cast?
        if cmd['range'] == 0:
            # need to cast anyway
            if rule.dice.check(75):
                doarcane = True
            else:
                dodivine = True
        else:
            if canarcane:
                if rule.dice.check(75):
                    doarcane = True
            if doarcane == False and candivine:
                if rule.dice.check(75):
                    dodivine = True
        if doarcane:
            school = 'arcane'
        elif dodivine:
            school = 'divine'
        else:
            return False
        cmd['comamnd'] = school

        # select spell level
        clroll = rule.dice.roll()
        if clroll < 71:
            cl = 0
        elif clroll < 91:
            cl = 1
        elif clroll < 97:
            cl = 2
        elif clroll < 98:
            cl = 3
        elif clroll < 99:
            cl = 4
        else:
            cl = 5

        cl = cmd[school] - cl - cmd[school+'mana'] -1
        if cl < 0: cl = 0

        # select spell to cast
        if rule.dice.check(66):
            ws = 0
        else:
            ws = 1
        cmd['casting'] = spell.getbyname(crowd.MOBSPELLSEL[school][cl][ws], school)
        cmd['target'] = crowd.targetpc(len(rule.group.member))
        return True

class battle:
    OPTIONSTR = {
        'fight':'戦う    ',
        'hide':'隠れる   ',
        'backstab':'奇襲    ',
        'spell':'呪文を唱える',
        'dispell':'呪いを解く ',
        'use':'アイテムを使う',
        'parry':'身を守る  ',
        'run':'逃げる',
        'return':'はじめに戻る',
        'start':'戦闘をはじめる',
    }


    turn = 0
    totalexp = 0

    ambush = 0

    pos = 0
    command = []
    commandstr = []

def encounter(mobname, mobgroupnum=6, chest=False):
    # initialization
    battle.turn = 0
    battle.totalexp = 0

    # initialize mob group
    crowd.member.clear()
    crowd.known.clear()
    mbase = monster.data[mobname]
    for i in range(mobgroupnum):
        num = rule.dice.roll(mbase['number'])
        if num > 9: num = 9
        elif num < 1: num = 1

        encs = []
        for j in range(num):
            mob = mbase.copy()
            mob['mob'] = True
            mob['pos'] = i
            mob['name'] = mob['unknownjp'][0]
            mob['hp'] = rule.dice.roll(mob['hd'])
            mob['hpmax'] = mob['hp']
            mob['divinemana'] = 0
            mob['arcanemana'] = 0
            mob['shield'] = 0
            encs.append(mob)
        crowd.member.append(encs)
        crowd.known.append(False)
        if mbase['next'] is not None and rule.dice.check(mbase['nextrate']):
            mbase = monster.data[mbase['next']]
        else:
            break

    # friendly check
    if rule.friendlycheck(mbase):
        for k in range(len(crowd.known)):
            crowd.known[k] = True
        for i, en in enumerate(crowd.member):
            for m in en:
                m['name'] = m['namejp'][0]

        crowd.draw()
        crowd.show()
        rule.group.draw()
        gfx.log([
            f'戦闘意思のない',
             ' ',
            f"{crowd.member[0][0]['name']:^16}",
            ' ',
             '     戦う',
             '     立ち去る',
        ])

        key, pos = gfx.select(152, 104, 2)
        if (key == 'a' and pos == 1) or key == 'b':
            return(dungeon.wandering, [])

    # ambush check
    battle.ambush = rule.surprisecheck(mbase) < 0

    __upkeep()
    return(commandinput, ['start'])

def commandinput(cmd, param = 0):
    if cmd == 'start':
        battle.command.clear()
        battle.commandstr.clear()
        battle.pos = 0
        for c in rule.group.member:
            if 'parry' in c: c.pop('parry')
            if 'casting' in c: c.pop('casting')


    if battle.ambush < 0:
        # pc cannot do any action
        return(monsteroption, [])
    elif battle.pos == len(rule.group.member):
        # final confirmation
        crowd.draw()
        crowd.show()
        rule.group.draw()
        __combatoptions()
        gfx.log([' ', ' ', ' ', ' ', f'     {battle.OPTIONSTR["start"]}', f'     {battle.OPTIONSTR["return"]}'])
        key, pos = gfx.select(152, 104, 2)
        if key == 'a':
            if pos == 0:
                return(monsteroption, [])
            else:
                return(commandinput, ['start'])
        else:
            battle.pos -= 1
            return(commandinput, ['options'])

    # do PC options
    cbase = rule.group.member[battle.pos]
    if rule.status.isactive(cbase):
        topt = []
        c = cbase.copy()

        # can fight?
        if rule.weaponrange(c, len(crowd.member)) > 0:
            topt.append('fight')
        
        # can hide or backstab?
        if 'hide' in c:
            if c['hide']:
                topt.append('backstab')
            else:
                topt.append('hide')
        
        # can cast spell?
        if battle.ambush == 0:
            if rule.status.cancast(c):
                for sp in c['arcane']:
                    if sp > 0:
                        topt.append('spell')
                        break
                if 'spell' not in topt:
                    for sp in c['divine']:
                        if sp > 0:
                            topt.append('spell')
                            break
                # can dispel?
                if 'special' in c and 'dispell' in c['special'] and c['special']['dispell'] <= c['level']:
                    topt.append('dispell')

        # use item
        topt.append('use')
        # parry
        topt.append('parry')
        # run away
        topt.append('run')
        # start over
        if battle.pos != 0:
            topt.append('return')
        optionstr = []
        for o in topt:
            optionstr.append(battle.OPTIONSTR[o])

        crowd.draw()
        crowd.show()
        rule.group.draw()
        __combatoptions()
        ltxt = []
        ltxt.append(' ')
        n = '  %s は' % c['name']
        ltxt.append(f"{n:^14}")
        ltxt.append(f"{' どうしますか？':^14}")
        ltxt.append(' ')
        for i, t in enumerate(optionstr):
            ltxt.append(f'     {t}')
        gfx.log(ltxt)
        key, pos = gfx.select(152, 104, len(topt))
        if key == 'b':
            return(commandinput, ['start'])
        elif key == 'a':
            if topt[pos] == 'fight':
                rule.group.draw()
                __combatoptions()
                r = selecttarget('どのモンスターと戦いますか?', c['range'])
                if r == -1:
                    return(commandinput, ['options'])
                c['command'] = 'fight'
                c['target'] = r
                battle.command.append(c)
                battle.commandstr.append(battle.OPTIONSTR[topt[pos]])
                rule.speed(c)
            elif topt[pos] == 'spell':
                crowd.draw()
                crowd.show()
                rule.group.draw()
                __combatoptions()
                if spell.selectspell(c):
                    spl = c['casting']
                    tgt = spl['target']
                    lvl, school = spell.schoolbyname(spl['name'].lower())
                    if tgt == 'self' or tgt == 'mygroup' or tgt == 'all':
                        c['target'] = -1
                    elif tgt == 'member':
                        crowd.draw()
                        rule.group.draw()
                        __combatoptions()
                        r = rule.selectmember('誰に唱えますか?')
                        if r == -1:
                            c.pop('casting')
                            return(commandinput, ['options'])
                        else:
                            c['target'] = r
                    elif tgt == 'single' or tgt == 'group':
                        rule.group.draw()
                        __combatoptions()
                        r = selecttarget('どのモンスターに唱えますか?')
                        if r == -1:
                            c.pop('casting')
                            return(commandinput, ['options'])
                        else:
                            c['target'] = r
                    c['command'] = school
                    battle.command.append(c)
                    battle.commandstr.append(spl['name'])
                    rule.speed(c)
                else:
                    return(commandinput, ['options'])
            elif topt[pos] == 'parry':
                c['command'] = 'parry'
                c['parry'] = True
                rule.refresh(c)
                battle.command.append(c)
                battle.commandstr.append(battle.OPTIONSTR[topt[pos]])
                rule.speed(c)
            elif topt[pos] == 'dispell':
                c['command'] = 'dispell'
                rule.group.draw()
                __combatoptions()
                r = selecttarget('どのモンスターを解呪しますか?', c['range'])
                if r == -1:
                    return(commandinput, ['options'])
                else:
                    c['target'] = r
                battle.command.append(c)
                battle.commandstr.append(battle.OPTIONSTR[topt[pos]])
                rule.speed(c)
            elif topt[pos] == 'use':
                pass
            elif topt[pos] == 'hide':
                pass
            elif topt[pos] == 'backstab':
                pass
            elif topt[pos] == 'run':
                if rule.retreatcheck(crowd.member):
                    for c in rule.group.member:
                        if 'parry' in c: c.pop('parry')
                        if 'casting' in c: c.pop('casting')
                    return(dungeon.wandering, [dungeon.dungeon.oldx, dungeon.dungeon.oldy])                                        
                else:
                    battle.command.clear()
                    battle.commandstr.clear()
                    battle.pos = 0
                    for c in rule.group.member:
                        if 'parry' in c: c.pop('parry')
                        if 'casting' in c: c.pop('casting')
                    return(monsteroption, [])

            elif topt[pos] == 'return':
                return(commandinput, ['start'])
    else:
        battle.commandstr.append('yyy')
    battle.pos += 1
    return(commandinput, ['options'])

def monsteroption():
    if battle.ambush <= 0:
        for i, mgroup in enumerate(crowd.member):
            for m in mgroup:
                if 'casting' in m: m.pop('casting')
                if 'command' in m: m.pop('command')
            
                if rule.status.isactive(m):
                    r = rule.weaponrange(m, len(rule.group.member))
                    if 'special' in m:
                        if 'retreat' in m['special']:
                            if rule.dice.check(m['special']['retreat']):
                                if rule.group.moral() > crowd.moral():
                                    m['command'] = 'run'
                                    m['target'] = -1
                                    rule.speed(m)
                                    battle.command.append(m)
                                    continue
                        if 'breath' in m['special']:
                            if rule.dice.check(60):
                                m['command'] = 'breath'
                                m['target'] = -1
                                rule.speed(m)
                                battle.command.append(m)
                                continue
                    if battle.ambush < 0:
                        if rule.status.cancast(m):
                            if crowd.selectspell(m):
                                rule.speed(m)
                                battle.command.append(m)
                                continue
                    if r != 0:
                        m['command'] = 'fight'
                        m['target'] = crowd.targetpc(r)
                        rule.speed(m)
                        battle.command.append(m)
    return(processturn, [])

def processturn():
    # sort by speed
    battle.command = sorted(battle.command, key=lambda x:x['speed'], reverse=True)

    # show result for debug purpose
    if rule.DEBUG:
        print(f"turn {battle.turn} start")
        for i in battle.command:
            if 'anchor' in i:
                # anchor
                print("ANCHOR")
            else:
                if 'pc' in i:
                    ty = 'pc'
                elif 'mob' in i:
                    ty = 'mob'
                else:
                    ty = 'xxx'
                if 'casting' in i:
                    print(f"{ty}({i['pos']}) {i['name']} spd:{i['speed']} cmd:{i['command']} tgt:{i['target']} cst:{i['casting']}")
                elif 'target' in i:
                    print(f"{ty}({i['pos']}) {i['name']} spd:{i['speed']} cmd:{i['command']} tgt:{i['target']}")
                else:
                    print(f"{ty}({i['pos']}) {i['name']} spd:{i['speed']} cmd:{i['command']}")

    # start combat process
    for cmd in battle.command:
        gfx.qclear()
        if 'command' not in cmd: continue
        if rule.status.isactive(cmd):
            rule.refresh(cmd)
            # draw
            if 'pc' in cmd:
                if 'casting' in cmd:
                    spl = cmd['casting']
                    if spl['target'] == 'member':
                        rule.group.show(cmd['target'])
                    elif spl['target'] == 'self' or spl['target'] == 'mygroup':
                        rule.group.show(cmd['pos'])
                    elif spl['target'] == 'all':
                        crowd.show(0)
                    elif spl['target'] == 'single' or spl['target'] == 'group':
                        crowd.show(cmd['target'])
                elif cmd['command'] == 'dispell' or cmd['command'] == 'fight':
                    crowd.show(cmd['target'])
                elif cmd['command'] == 'parry':
                    rule.group.show(cmd['pos'])
            elif 'mob' in cmd:
                crowd.show(cmd['pos'])
            crowd.draw()
            rule.group.draw()
            __combatoptions()

            wait = True
            if cmd['command'] == 'fight':
                # get target
                t = None
                if 'pc' in cmd:
                    for tm in crowd.member[cmd['target']]:
                        if rule.status.istreatasdead(tm) == False:
                            if tm['hp'] > 0:
                                t = tm
                                break
                elif 'mob' in cmd:
                    tm = rule.group.member[cmd['target']]
                    if rule.status.isdead(tm) == False:
                        if tm['hp'] > 0:
                            t = tm

                if t is None:
                    continue
                else:
                    ftxt = ''
                    swing, dmg, proc = rule.hitcheck(cmd, t)
                    # build message
                    ftxt += f"{cmd['name']}は,-{t['name']}{getfightingtext(cmd)}。-"
                    if swing > 0:
                        ftxt += f"そして,-{swing}回あたり,-{dmg}のダメージ。"
                    else:
                        ftxt += "しかし,外れた。"
                    dtxt = ''
                    for pm in proc.keys():
                        if pm == 'drain':
                            if proc['drain'] > 1:
                                dtxt += f"{t['name']}は,-{proc['drain']}{rule.PROCSTR['drains']}-"
                            else:
                                dtxt += f"{t['name']}は,-1{rule.PROCSTR[pm]}-"
                        else:
                            ftxt += f"{t['name']}{rule.PROCSTR[pm]}-"

                    # apply status
                    for pm in proc.keys():
                        if pm == 'drain':
                            t['drained'] += proc[pm]
                        elif pm == 'critical':
                            t['dead'] = True
                            if 'rip' in t: t['rip'] += 1
                        elif pm not in t:
                            t[pm] = proc[pm]
                    # apply damage
                    t['hp'] -= dmg
                    if rule.status.isdead(t) == False:
                        if t['hp'] <= 0:
                            t['hp'] = 0
                            t['dead'] = True
                            if 'rip' in t: t['rip'] += 1
                            addexp(t)
                            dtxt += f"{t['name']}{rule.PROCSTR['dead']}-"
                    if 'lost' not in t:
                        if 'drained' in t:
                            if t['level'] - t['drained'] < 0:
                                t['lost'] = True
                                dtxt += f"{t['name']}{rule.PROCSTR['lost']}-"
                    gfx.log(ftxt+'+'+dtxt, True, True)
            elif cmd['command'] == 'dispell':
                dlist = gfx.qgetlist()
                # get target
                t = None
                dispelled = 0
                if 'pc' in cmd:
                    for tm in crowd.member[cmd['target']]:
                        if not rule.status.istreatasdead(tm) and tm['race'] == 'undead':
                            if rule.dice.check(50 + cmd['level']*5 - tm['level']*10):
                                dispelled += 1
                                tm['dead'] = True
                    ftxt = f"{cmd['name']}は-{tm['name']}を-解除しようとした。"
                    if dispelled == 0:
                        dtxt = "しかし-効果がなかった。"
                    else:
                        dtxt = f"{dispelled}体-解除した。"
                    gfx.log(ftxt+'+'+dtxt, True, True)
            elif cmd['command'] == 'breath':
                # breath weapon does not take target
                dlist = gfx.qgetlist()
                for c in rule.group.member:
                    gfx.qsendlist(dlist)
                    if rule.status.isdead(c): continue
                    dmg, sf = rule.breath(cmd, c)
                    c['hp'] -= dmg
                    ftxt = f"{cmd['name']}{rule.BREATHSTR[cmd['special']['breath']]}"
                    dtxt = f"{c['name']}は{dmg}のダメージをうけた。"
                    if sf is not None: txt = txt + sf
                    if rule.status.isdead(c) == False:
                        if c['hp'] < 0:
                            c['dead'] = True
                            if 'rip' in t: t['rip'] += 1
                            addexp(c)
                            dtxt += f"{c['name']}{rule.PROCSTR['dead']}"
                    gfx.log(ftxt+'+'+dtxt, True, True)
                    # wait for key
                    gfx.getkey(['a', 'b'], 4)
                wait = False

            elif cmd['command'] == 'arcane' or cmd['command'] == 'divine':
                spl = cmd['casting']
                if spl is not None:
                    spell.cast(cmd, spl)
                    wait = False
            elif cmd['command'] == 'run':
                ftxt = f"{cmd['name']}は逃げた。"
                gfx.log(ftxt, True, True)
                cmd['runaway'] = True
                cmd['dead'] = True
            else:
                continue

            # wait for key
            if wait: gfx.getkey(['a', 'b'], 4)
            
            # wipe check
            allwiped = True
            for i, mg in enumerate(crowd.member):
                for m in mg:
                    if rule.status.istreatasdead(m) == False:
                        allwiped = False
                        break
            if allwiped:
                # mob wiped. earn exp
                earned = []
                draintxt = []
                for c in rule.group.member:
                    if rule.status.isactive(c):
                        earned.append(c)
                    if 'drained' in c:
                        draintxt += c['name']+'は'+{c['drained']}+'、'
                        c['level'] -= c['drained']
                        if c['level'] < 0:
                            c['lost'] = True
                        c.pop('drained')
                if len(draintxt) > 0:
                    draintxt += 'レベルを下げられた。'
                battle.totalexp = battle.totalexp//len(earned)
                for c in earned:
                    c['exp'] = battle.totalexp

                gfx.qdraw(gfx.frame, (16,0,256//8-4,8,True))
                gfx.qdraw(gfx.frame, (0, 64, 112//8, 112//8, True))
                gfx.qdraw(gfx.frame, (0, 64, 14, 14,True))
                gfx.qdraw(gfx.frame, (112, 64, 18, 14,True))
                rule.group.draw()
                gfx.qdraw(gfx.text, (0, 2*8, f"{'生き残ったメンバーは':^32}"))
                e = f"{battle.totalexp}E.P.を得た。"
                gfx.qdraw(gfx.text, (0, 3*8, f"{e:^32}"))
                if len(draintxt):
                    gfx.qdraw(gfx.text, (0, 5*8, f"{draintxt:^32}"))
                gfx.getkey(['a', 'b'])

                return(dungeon.wandering, [])
            if rule.group.iswiped():
                # wiped
                wiped(rule.group.member)
                rule.ootown.save(wiped=True)
                return (city.town, [])
    # end of turn
    # reorder mobs
    newmonster = []
    newknown = []
    newpos = 0
    for i, mg in enumerate(crowd.member):
        gwiped = True
        for m in mg:
            if rule.status.istreatasdead(m) == False:
                gwiped = False
                break
        if gwiped == False:
            for m in mg: m['pos'] = newpos
            newpos += 1
            newmonster.append(mg)
            newknown.append(crowd.known[i])
    crowd.member = newmonster
    crowd.known = newknown

    __upkeep()
    return(commandinput, ['start'])

def __combatoptions():
    for i, c in enumerate(rule.group.member):
        if i < len(battle.commandstr):
            t = battle.commandstr[i]
            if t == 'yyy':
                continue
            elif t == 'skip':
                continue
        else:
            if rule.status.isactive(c):
                t = '??????'
            else:
                continue
        gfx.qdraw(gfx.text, (200, 192+i*8, f"{t:.6}", True))

def __upkeep():
    # update turn
    battle.turn += 1

    for c in rule.group.member:
        rule.refresh(c)
    for mobs in crowd.member:
        for m in mobs:
            rule.refresh(m)

    # reorder in-active member
    rule.group.battleorder()

    if battle.ambush >= 0:
        if 'identify' in rule.group.buff:
            for k in len(crowd.known):
                crowd.known[k] = True
        else:
            for t in range(rule.monsteridentify()):
                crowd.known[rule.dice.roll((1, len(crowd.known), -1))] = True
        for i, en in enumerate(crowd.member):
            for m in en:
                # rename identified monster
                if crowd.known[i]: m['name'] = m['namejp'][0]
    if battle.ambush <= 0:
        # recover from sleep
        if 'sleep' in m:
            if rule.dice.check(m['sleep']) == False: m.pop('sleep')

def addexp(m):
    if 'mob' in m:
        #print(f"killed {m['name']} {m['exp']}")
        battle.totalexp += m['exp']


def selecttarget(t, range=len(crowd.member)):
    if len(crowd.member) > 1:
        rule.group.draw()
        __combatoptions()
        gfx.log([
            '',
            f' {t}',
        ])

        key, pos = crowd.select(range)
        if key == 'a':
            return pos
        else:
            return -1
    else:
        return 0

#### get fighting style text
def getfightingtext(c, hand='main'):
    if 'pc' in c:
        if hand in c['equipment']:
            w = item.getitembyname(c['equipment'][hand])
            tt = 'sword'
            for wt in item.MELEESTR.keys():
                if wt in w['type']:
                    tt = wt
            return item.MELEESTR[tt][rule.dice.roll((1,len(item.MELEESTR[tt]),-1))]
        else:
            return item.MELEESTR['hand'][rule.dice.roll((1,len(item.MELEESTR['hand']),-1))]
    elif 'mob' in c:
        tt = c['class']
        if tt not in item.MELEESTR:
            tt = 'predetor'
        return item.MELEESTR[tt][rule.dice.roll((1,len(item.MELEESTR[tt]),-1))]

#### wiped screen
def wiped(deadmen):
    gfx.qdraw(gfx.frame, (8,24, (256-16)//8, (192+16)//8))
    gfx.qdraw(gfx.frame, (0, 256-5*8, 32, 5, True))
    gfx.qdraw(gfx.show, ('monster/event2', 32, 80))
    for i, c in enumerate(deadmen):
        gfx.qdraw(gfx.text, (136, 80+i*8, f"{c['name']}"))
    gfx.qdraw(gfx.text, (8, 256-5*8+8,  f"{'パーティは全滅した。':^30}"))
    gfx.qdraw(gfx.text, (8, 256-5*8+24, f"{'ボタンを押せば、墓場から抜けます!':^30}"))
    gfx.getkey(['a', 'b'])
