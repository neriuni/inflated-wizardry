import threading

import gfx

import rule
import item
import dungeon
import maze
import city
import storage

import os

def title():
    # title screen
    monsters = gfx.image.filelist('monster')
    while True:
        sm = monsters[rule.dice.roll((1, len(monsters), -1))]
        gfx.qdraw(gfx.frame, (8,24, (240)//8, (208)//8))
        gfx.qdraw(gfx.show, ('base', 32, 32))
        gfx.qdraw(gfx.text, (0, 72, f"{maze.maze.title:^32}"))
        gfx.qdraw(gfx.show, ('monster/'+sm, 74, 88))
        gfx.qdraw(gfx.frame, (0, 256-6*8, 32, 5, True))
        gfx.qdraw(gfx.text, (8, 256-6*8+8,  f"{'Wizardryの世界へようこそ!':^30}"))
        gfx.qdraw(gfx.text, (8, 256-6*8+24, f"{'[A][START] でゲームを始めます':^30}"))
        key = gfx.getkey(['a', 'b', 'start'], 4)
        if key == 'a' or key == 'start':
            break


class app(threading.Thread):
    def run(self):
        item.init()
        storage.loadall()

        '''
        rule.group.add(rule.character.data[0])
        rule.group.add(rule.character.data[1])
        rule.group.add(rule.character.data[3])
        rule.group.add(rule.character.data[2])
        rule.group.add(rule.character.data[4])
        rule.group.totalgold = 2000
        '''
        title()

        # main loop
        proc = city.town
        args = []
        while True:
            proc, args = proc(*args)

if __name__ == "__main__":
    g = gfx.core(256, 256, 'Wizardry')
    a = app()

    g.start()
    a.start()
