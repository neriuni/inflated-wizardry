from pathlib import Path
import json

import rule
import item

save_dir = Path('./save')
save_dir.mkdir(exist_ok=True)
def save(data, filename):
    filename = (save_dir / Path(filename +'.json'))
    with open(filename, mode='wt', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)

def load(filename):
    filename = (save_dir / Path(filename +'.json'))
    with open(filename, mode='rt', encoding='utf-8') as f:
        try:
            return json.load(f)
        except FileNotFoundError as err:
            return None

def loadall():
    d = load('roster')
    if d is not None: rule.roster.member = d
    d = load('groups')
    if d is not None: rule.ootown.groups = d
    d = load('shop')
    if d is not None: item.TOWNSHOP = d
    d = load('items')
    if d is not None: rule.group.storage = d
    d = load('golds')
    if d is not None: rule.group.totalgold = d

def saveall():
    save(rule.roster.member, 'roster')
    save(rule.ootown.groups, 'groups')
    save(item.TOWNSHOP, 'shop')
    save(rule.group.storage, 'items')
    save(rule.group.totalgold, 'golds')
