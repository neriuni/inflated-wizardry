import gfx

import rule
import dungeon

class maze:

    firstfloor = [
        '####+######################g###-############################',
        '# -- ## ## +# ## +# -- -- ##2++*## ++ -- ++ -- ++ -- ## ## #',
        '#######+#####+#####-------#####-##-##+##+########----##+##+#',
        '#######+#####+#####-------#####-##-##+#f+########----##+##+#',
        '# -- ++ -- -- -- ++ -- -- ##*--*## ## ##2## -- ## -- ## -- #',
        '#+#####+##-########-------##----##-###########+########----#',
        '#+#####+##-########-------##----##-###########+########----#',
        '# #+ ## ## #+ -- ## -- -- ##*--*## #+ ## ++ -- ++ -- ++ -- #',
        '##########-##############+##-##-##-##-##-###################',
        '##########-##############+##-##-##-##-##-###################',
        '# -- -- ## ## -- -- #+ ## ##*##*## ## ## #+ -- ## -- ## -- #',
        '#-------##-##-------##-##-#####-##-##-##-##-########+#######',
        '#-------##-##-------##-##-#####-##-##-##-##-########+#######',
        '# -- -- ++ ++ -- -- ## ## ##*--*## ## ## ## ## ++ -- ++ -- #',
        '#-------##+##-------##-##-##----##-##-##-##-##-########+##+#',
        '#-------##+##-------##-##-##----##-##-##-##-##-########+##+#',
        '# -- -- ## ## -- -- ## ## ##*--*## ## ## ## ## #+ -- ## ## #',
        '##########-###########-##-##----##-##-##-##-##-#############',
        '##########-##+########-##-#b----##-##-##-##-##-#############',
        '# +# -- -- ## ## ## -- ## ##*--*## ## ## ## ## #+ -- ++ -- #',
        '####+#####-#####-########-##-##+##-##-##-##-##-##+##+##----#',
        '####+#####-#####-########-#a-#####-##-##-##-##-##+##+##----#',
        '# -- -- ## -- ++ ++ -- ## ##1-- ## ## ## ## ## ## ## ## -- #',
        '#-------##----##-##----##+##----##-##-##-##-##-##-##-##+##+#',
        '#-------##----##-##----##+##----##-##-##-##-##-##-##-##+##+#',
        '# -- -- ## -- ## ## -- ## ## -- ## ## ## ## ## ## ## ## ## #',
        '################+###########----##-##-##-##-##-#####-#######',
        'i###############+###########----##-##-##-##-##-#####-#####+#',
        '#1-- -- -- -- -- -- -- -- -- -- ## ## ## ## ## ## +# ## -- #',
        '############################----##+#####+#####+#####+#######',
        '###############c############----##+#####+#####+#####+#######',
        '# -- -- -- -- -- -- -- -- ## -- -- -- -- -- -- -- -- -- -- +',
        '############################-###############################',
        '############################-###############################',
        '# -- -- -- -- -- -- -- -- ++ ##2## -- -- -- -- -- -- -- -- #',
        '#-########-########-###########-##-##############-##########',
        '#-########-########-###########-##-##############-##########',
        '# ## -- ## ## -- ## ## -- ##*--*## ## +# #+ -- ## ## -- -- #',
        '#-##----##-##-#####+#####-##----##-#####+#####-##-##-#####-#',
        '#-##----##-##-#####+#####-##----##-##+########-##-##-#####-#',
        '# ## -- ## ## ## -- -- ## ##*--*## ## ## +# ## ## ## ## ## #',
        '#-##----##-##-##-------##-##----##-########+##+##-##-##-##-#',
        '#-##----##-##-##-------##-##----##-##+#d###+##+##-##-##-##-#',
        '# ## -- ## ## ## -- -- ## ##*--*## ## ##1++ -- ## ## ## ## #',
        '#-##----##-##-###########-##----##-########----##-##-##-##-#',
        '#-##----##-##-###########-##----##-##+#c###----##-##-##-##-#',
        '# ## -- ## ## -- -- -- -- ##*--*## ## ## ++ -- ## ## ## ## #',
        '#-##-#####+#####----------##----##-########----##-##-##-##-#',
        '#-##-#####+#####----------##----##-##+#e###----##-##-##-##-#',
        '# ## ## -- -- ## -- -- -- ##*--*## ## ##1++ -- ## ## ## ## #',
        '#-##-##-------##-###########----##-########+##+#####-##-##-#',
        '#-##-##-------##-###########----##-##+########+#####-##-##-#',
        '# ## ## -- -- ## ## -- -- ##*--*## ## #+ #+ #+ ++ -- ## ## #',
        '#-##-###########-##-------##----##-####################-##-#',
        '#-##-###########-##-------##----##-####################-##-#',
        '# ## -- -- -- -- ## -- -- ##*--*## -- -- -- -- -- -- -- ## #',
        '#-##############+#####+#####----##########################+#',
        'h-####################+#####----############################',
        '#2-- -- -- -- -- -- -- -- ##*--*#+*--*--*--*--*--*--*--*--*#',
        '###############################-############################',
    ]

    secondfloor = [
        '############################################################',
        '# -- -- -- -- -- -- -- -- -- -- -- -- ## -- -- -- -- -- -- #',
        '#-------------------------###########-##-#################-#',
        '#-------------------------#b#########-##-#################-#',
        '# -- -- -- -- -- -- -- -- ##1## ## ## ## ## -- -- -- -- ## #',
        '#----#################-#####+##+##+#####-##-###########-##-#',
        '#----#################-#####+##+##+#####-##-###########-##-#',
        '# -- ##*--*--*--*--*## ## -- ## ## -- ## ## ## -- -- ## ## #',
        '#----##-##-#####-##-##-##+#####+#####+##-##-##-#####-##-##-#',
        '#----##-##-#g###-##-##-##+#####+#####+##-##-##-#####-##-##-#',
        '# -- ##*##*##1##*##*#+ ## -- -- -- -- ## #+ ## ## +# ## ## #',
        '#----##-##-##+##-##-##-########+########-##-##-#####-##-##-#',
        '#----##-##-##+##-##-##-########+########-##-##-#####-##-##-#',
        '# -- ##*##*##*##*##*## -- -- ## ## -- -- ## ## ##1## ## ## #',
        '#----##-##-##-##-##-##-------##-##-########-##-##-##-##-##-#',
        '#----##-##-##-##-##-##-------##-##-########-##-##-##-##-##-#',
        '# -- ##*##*--*--*##*## -- -- ## ## ## ## ++ ## ## -- ## ## #',
        '#----##-###########-##-------##-##-##-#####-##-########-##-#',
        '#----##-###########-##-------##-##-##-#####-##-########-##-#',
        '# -- +#*--*--*--*--*## -- -- ## ## ## -- ## ## -- -- -- ## #',
        '#----########+########-########+##-##----##-##############-#',
        '#----#######f+########-#a######+##-##----##-##############-#',
        '# -- -- -- ##1## -- -- ##1-- -- ## ## -- +# -- -- -- -- -- #',
        '#----------##+##-------##+########+#########################',
        '#----------#e+##-------##+########+#p##j##i##h##############',
        '# -- -- -- ##1## -- -- ## -- -- -- ##1--1--1--1-- -- -- -- #',
        '#############+#####+#####-########-##############-##########',
        '#############+###########-########-##############-#####+####',
        '- -- -- -- -- -- -- -- ++ ## ++ ## ## -- -- -- +# ## -- ## -',
        '#############-###########-#####-##-##----------##-########-#',
        '#############-#####+#####-#####-##-##----------##-########-#',
        '# -- ## -- ## ## -- -- ## ## ++ ## ## -- -- -- ## ## -- ## +',
        '#-#####----##-##-------##-#####+##-#####-------##-##----##-#',
        '#-#####----##-##-------##-#####+##-#####-------##-##----##-#',
        '# ## ++ -- ## ## -- -- ## -- ## ## -- ## -- -- ## ++ -- ## #',
        '#-##-#####+##-##-------##+########----##-------##-########-#',
        '#-##-#####+##-##-------#c+########--k-##-------##-########-#',
        '# ## ## -- ## ## -- -- ##1-- -- ## --2## -- -- ## ## -- ++ #',
        '#-##-##-#####-##-------########+########-------##-##----##-#',
        '#-##-##-#####-##-------########+##+#####-------##-##----##-#',
        '# ## ## -- -- ## -- -- -- -- ## ## -- -- -- -- ## ## -- ## #',
        '#-##-########-##----###########-###########----##-########-#',
        '#-##-########-##----###########-###########----##-########-#',
        '# +# ## -- ++ ## -- ## -- ## -- -- ## -- ## -- ## ## -- ## #',
        '#-##-##-#####+#####-##-##-##-------##-##-##----##-##----##-#',
        '#-##-##-#####+#####-##-##-##-------#d-##-##----##-##----##-#',
        '# ## ## ## ## -- ## ## ## ## -- -- ##1## ## -- ## ++ -- ## #',
        '####+##-##-##----##-##-#####-------#####-##----##-########-#',
        '####+##-##-##----##-##-#####-------#####-##----##-########-#',
        '- -- ## ## ## -- ## ## -- -- -- -- -- -- ## -- ## ## -- ++ -',
        '-----##-##-########+##-########+########-########-##----##--',
        '-----##-##-########+##-########+########-########-##----##--',
        '- -- ## #+ -- -- -- ## ## ## -- -- ## ## ## -- ## ## -- ## -',
        '#######-##----------##-##-##-------##-##-##----##-########-#',
        '#+#####-##----------##-##-##-------##-##-##----##-########-#',
        '# -- ## ## -- -- -- ## -- ## -- -- ## -- ## -- ## ## -- ++ #',
        '#----##-###################################----##-##----##-#',
        '#----##-###################################----##-##----##-#',
        '# -- ## -- -- -- -- -- -- -- -- -- -- -- ++ -- ## ++ -- ## #',
        '#################################################+##########',
    ]


    ### event common
    def StairUp(ask=None, up=True):
        if up:
            t = 'のぼる'
            if ask is None:
                ask = '上へ行きますか?'
        else:
            t = 'くだる'
            if ask is None:
                ask = '下へ行きますか?'
        gfx.qdraw(gfx.frame, (80, 64, 13, 7, True))
        gfx.qdraw(gfx.text, (96, 72, f"{t}階段がある"))
        gfx.qdraw(gfx.text, (96, 80, f"{ask}"))
        gfx.qdraw(gfx.text, (96, 96, "はい"))
        gfx.qdraw(gfx.text, (96, 104, "いいえ"))
        key, pos = gfx.select(88, 96, 2)
        if key == 'a' and pos == 0:
            # search
            return True
        #cancel
        return False
    def StairDown(ask=None):
        return maze.StairUp(ask=ask, up=False)

    def searchbox():
        gfx.qdraw(gfx.frame, (80, 32, 12, 6, True))
        gfx.qdraw(gfx.text, (88, 40, f"{'探しますか?':^10}"))
        gfx.qdraw(gfx.text, (120, 56, "はい"))
        gfx.qdraw(gfx.text, (120, 64, "いいえ"))
        key, pos = gfx.select(112, 56, 2)
        if key == 'a' and pos == 0:
            # search
            return True
        #cancel
        return False


    ### event data
    #### first floor
    def edge():
        dungeon.draw()
        gfx.qdraw(gfx.frame, (0, 0, 256//8, 7, True))
        gfx.qdraw(gfx.text, (8*8, 1*8, '壁に貼ってある、大きな看板には、'))
        gfx.qdraw(gfx.text, (4*8, 3*8, '*** 回廊の終わりを越えようとしている ***'))
        gfx.qdraw(gfx.text, (12*8, 5*8, '引き返しなさい!'))
        gfx.getkey(['a', 'b'])
        return (-1,0,0,0,0,0)

    def dark():
        dungeon.draw()
        gfx.qdraw(gfx.frame, (0, 0, 256//8, 5, True))
        gfx.qdraw(gfx.text, (8, 1*8, '入るなり、周りが金色の煙で満たされた。'))
        gfx.qdraw(gfx.text, (8, 3*8, '突然冒険者は、今入ってきたほうへ出ていきたい衝動にかられた。'))
        gfx.getkey(['a', 'b'])
        return (-1,1,0,1,9,12)

    def warpc():
        return (-1, 1, 0, 3, 15, 4)

    def murphy():
        dungeon.draw()
        gfx.qdraw(gfx.show, ('monster/mage5', 74, 108, True))
        gfx.qdraw(gfx.frame, (0, 0, 256//8, 9, True))
        gfx.qdraw(gfx.text, (8, 1*8, 'フードを被った人間型の大きな彫像が見える。'))
        gfx.qdraw(gfx.text, (8, 3*8, 'フードの穴から金色の光が漏れていて彫像には、'))
        gfx.qdraw(gfx.text, (8, 5*8, '様々な形の宝石が散りばめられている。'))
        gfx.qdraw(gfx.text, (8, 7*8, '彫像の前には祭壇があり、新しいお香が焚かれている。'))
        gfx.getkey(['a', 'b'])

        dungeon.draw()
        gfx.qdraw(gfx.show, ('monster/mage5', 74, 108, True))
        if maze.searchbox():
            return (0, 3, 'murphys ghost', 1,0,0)
        else:
            return (0,0,0,0,0,0)
            
    def bronze():
        dungeon.draw()
        gfx.qdraw(gfx.frame, (0, 0, 256//8, 7, True))
        gfx.qdraw(gfx.text, (8, 1*8, '部屋には頭が猫で体が鶏の不気味なケダモノの彫像がある。'))
        gfx.qdraw(gfx.text, (8, 3*8, '彫像はブロンズで、台座はオニキスでできている。'))
        gfx.qdraw(gfx.text, (8, 5*8, '飾り台の上に不自然な傷跡がある。'))
        gfx.getkey(['a', 'b'])
        return (-1,0,0,0,0,0)
    def silver():
        dungeon.draw()
        gfx.qdraw(gfx.frame, (0, 0, 256//8, 9, True))
        gfx.qdraw(gfx.text, (8, 1*8, '部屋には、角と長い牙を持った雄豚の銀の彫像がある。'))
        gfx.qdraw(gfx.text, (8, 3*8, '彫像の横の壁には、通りすがりのエルフが書き残していったらしい'))
        gfx.qdraw(gfx.text, (8, 5*8, '消えかけた伝言がある。'))
        gfx.qdraw(gfx.text, (8, 7*8, 'ほとんど読めないが、どうやら死霊や悪魔がいるとの警告らしい。'))
        gfx.getkey(['a', 'b'])
        return (-1,0,0,0,0,0)
    def mapiro():
        dungeon.draw()
        gfx.qdraw(gfx.frame, (0, 0, 256//8, 11, True))
        gfx.qdraw(gfx.show, ('monster/mage13', 74, 108, True))
        gfx.qdraw(gfx.text, (8, 1*8, 'この部屋からは、怪しげな灯りが発せられている。'))
        gfx.qdraw(gfx.text, (8, 3*8, '真ん中に長いローブを着た小柄な男が、冒険者を振り返り叫んだ。'))
        gfx.qdraw(gfx.text, (8, 5*8, '          "異邦人達よ、去れ"'))
        gfx.qdraw(gfx.text, (8, 7*8, '彼は、手を振りはじめ、念じた。'))
        gfx.qdraw(gfx.text, (8, 9*8, '   "MAPIRO MAHAMA DIROMAT!!"'))
        gfx.getkey(['a', 'b'])
        return (-1, 4,0,0,0,0)
    def uptocastle():
        dungeon.draw()
        if maze.StairUp(ask='城砦へ戻りますか?'):
            return (-1, 4,0,0,0,0)
        else:
            return (-1, 0,0,0,0,0)
    def downtosecond():
        dungeon.draw()
        if maze.StairDown():
            return (-1, 2,'second',-1,12,7)
        else:
            return (-1, 0,0,0,0,0)

    firstfloorevent = {
        'a_event':edge,
        'b_event':dark,
        'c_event':warpc,
        'd_event':murphy,
        'e_event':bronze,
        'f_event':silver,
        'g_event':mapiro,
        'h_event':uptocastle,
        'i_event':downtosecond,
    }

    firstfloorencounter = {
        20:'bubbly slime',
        40:'orc',
        65:'kobold',
        90:'undead kobold',
        95:'rogue',
        100:'bushwacker',
    }


    #### second floor


    def needsilver():
        dungeon.draw()
        gfx.qdraw(gfx.frame, (0, 0, 256//8, 7, True))
        gfx.qdraw(gfx.text, (8, 1*8, '部屋に入ると、銀色の霧が立ち込めた。'))
        gfx.qdraw(gfx.text, (8, 3*8, '恐ろしい悪魔のような姿が、あたりを取り囲んだ。'))
        gfx.qdraw(gfx.text, (8, 5*8, 'あなたたちは恐怖のあまり部屋から逃げ出した。'))
        gfx.getkey(['a', 'b'])
        return (-1,0,0,0,0,0)
    def bear():
        dungeon.draw()
        gfx.qdraw(gfx.frame, (0, 0, 256//8, 5, True))
        gfx.qdraw(gfx.text, (8, 1*8, 'あなたは台座の上に熊の彫像をみつけた。'))
        gfx.qdraw(gfx.text, (8, 3*8, '壁には看板があり、"オレは何百万もヤツらをヤッたぞ"'))
        gfx.getkey(['a', 'b'])
        return (-1,0,0,0,0,0)
    def needbronze():
        dungeon.draw()
        gfx.qdraw(gfx.frame, (0, 0, 256//8, 5, True))
        gfx.qdraw(gfx.text, (8, 1*8, '部屋に入りなり、ブロンズ色の煙が充満した。'))
        gfx.qdraw(gfx.text, (8, 3*8, 'あたたたちはただちに出ていきたい衝動にかられた！'))
        gfx.getkey(['a', 'b'])
        return (-1,0,0,0,0,0)
    def frog():
        dungeon.draw()
        gfx.qdraw(gfx.frame, (0, 0, 256//8, 7, True))
        gfx.qdraw(gfx.text, (8, 1*8, '赤と青の外套に身を包んだ蛙の彫像が輝く銀の円盤の上にのっている。'))
        gfx.qdraw(gfx.text, (8, 3*8, '彫像は、動き出し、"イエイ..イエイ.."と叫びながら'))
        gfx.qdraw(gfx.text, (8, 5*8, '足を前後に振りながら踊っている。'))
        gfx.getkey(['a', 'b'])
        return (-1,0,0,0,0,0)
    def needbear():
        dungeon.draw()
        gfx.qdraw(gfx.frame, (0, 0, 256//8, 3, True))
        gfx.qdraw(gfx.text, (8, 1*8, 'パーティにいる誰もが扉をこじ開けることができなかった。'))
        gfx.getkey(['a', 'b'])
        return (-1,0,0,0,0,0)
    def needfrog():
        dungeon.draw()
        gfx.qdraw(gfx.frame, (0, 0, 256//8, 3, True))
        gfx.qdraw(gfx.text, (8, 1*8, 'パーティにいる誰もが扉をこじ開けることができなかった。'))
        gfx.getkey(['a', 'b'])
        return (-1,0,0,0,0,0)
    def gold():
        dungeon.draw()
        gfx.qdraw(gfx.frame, (0, 0, 256//8, 7, True))
        gfx.qdraw(gfx.text, (8, 1*8, '部屋には、鶏の体に猫の頭をもつ怪物の彫像がある。'))
        gfx.qdraw(gfx.text, (8, 3*8, '彫像はブロンズでできており、オニキスの台座の上に鎮座している。'))
        gfx.qdraw(gfx.text, (8, 5*8, '飾り板には変わったルーンが書き込まれている。'))
        gfx.getkey(['a', 'b'])
        return (-1,0,0,0,0,0)
    def nolit():
        dungeon.draw()
        gfx.qdraw(gfx.frame, (0, 0, 256//8, 3, True))
        gfx.qdraw(gfx.text, (8, 1*8, '地面近くの立て看板には、"迷宮の暗闇の中では…"'))
        gfx.getkey(['a', 'b'])
        return (-1,0,0,0,0,0)
    def light():
        dungeon.draw()
        gfx.qdraw(gfx.frame, (0, 0, 256//8, 3, True))
        gfx.qdraw(gfx.text, (8, 1*8, '地面近くの立て看板には、"灯りがないときには…"'))
        gfx.getkey(['a', 'b'])
        return (-1,0,0,0,0,0)
    def watch():
        dungeon.draw()
        gfx.qdraw(gfx.frame, (0, 0, 256//8, 3, True))
        gfx.qdraw(gfx.text, (8, 1*8, '地面近くの立て看板には、"注意せよ。さもなくば…"'))
        gfx.getkey(['a', 'b'])
        return (-1,0,0,0,0,0)
    def uptofirst():
        dungeon.draw()
        if maze.StairUp():
            return (-1, 2,'first',-1,0,10)
        else:
            return (-1, 0,0,0,0,0)

    secondfloorevent = {
        'a_event':needsilver,
        'b_event':bear,
        'c_event':needbronze,
        'd_event':frog,
        'e_event':needbear,
        'f_event':needfrog,
        'g_event':gold,
        'h_event':nolit,
        'i_event':light,
        'j_event':watch,
        'k_event':uptofirst,
        #'l_event':downtothird,
    }

    secondfloorencounter = {
        15:'highwayman',
        33:'zombie',
        52:'creeping crud',
        72:'gas cloud',
        91:'lvl 1 mage',
        96:'lvl 1 priest',
        98:'creeping coin',
        100:'lvl 1 ninja',
    }

    def openning():
        gfx.qdraw(gfx.frame, (8,24, (256-16)//8, (192+16)//8))
        gfx.qdraw(gfx.frame, (16, 80, 28, 5))
        gfx.qdraw(gfx.text, (24, 88, f'{"Entering Proving Grounds":^26}'))
        gfx.qdraw(gfx.text, (24, 104, f'{"of the Mad Overlord":^26}'))
        rule.group.draw()
        gfx.getkey(['a', 'b'])
        dungeon.dungeon.x = 0
        dungeon.dungeon.y = 0
        dungeon.dungeon.dir = dungeon.dungeon.NORTH

    #### loot table
    loot = {
        'gold0':{
            'gold':(20, 5, 0),
        },
        'gold1':{
            'gold':(40, 5, 0),
        },
        'gold2':{
            'gold':(60, 5, 0),
        },
        'gold3':{
            'gold':(80, 5, 0),
        },
        'gold4':{
            'gold':(100, 5, 0),
        },
        'gold5':{
            'gold':(120, 5, 0),
        },
        'gold6':{
            'gold':(100, 10, 0),
        },
        'gold7':{
            'gold':(100, 20, 0),
        },
        'gold8':{
            'gold':(100, 40, 0),
        },
        'gold9':{
            'gold':(100, 80, 0),
        },
        'treasure0':{
            'gold':(20, 5, 0),
            'trap':['none', 'needle', 'arrow', 'explode', 'stun'],
            'loot':[[10, '?anointed mace', '?anointed flail', '?staff', '?dagger', '?small shield', '?large shield', '?robes', '?leather armor', '?chain mail', '?breast plate', '?plate mail', '?helm', '?potion of dios', '?potion of latumofis', '?long sword+1']],
        },
        'treasure1':{
            'gold':(40, 5, 0),
            'trap':['none', 'needle', 'arrow', 'gas', 'explode', 'stun'],
            'loot':[
                [20, '?long sword', '?short sword', '?anointed mace', '?anointed flail', '?staff', '?dagger', '?small shield', '?large shield', '?robes', '?leather armor', '?chain mail', '?breast plate', '?plate mail', '?helm', '?potion of dios',],
                [10, '?long sword+1', '?short sword+1', '?mace+1', '?staff of mogref', '?scroll of katino', '?leather armor+1', '?chain mail+1', '?plate mail+1', '?shield+1', '?breast plate+1', '?scroll of badios(a)', '?scroll of halito', '?long sword-1', '?short sword-1', '?mace-1', '?staff+2',],
            ],
        },
        'treasure2':{
            'gold':(60, 5, 0),
            'trap':['none', 'needle', 'arrow', 'explode', 'stun', 'teleporter'],
            'loot':[
                [30, '?long sword', '?short sword', '?anointed mace', '?anointed flail', '?staff', '?dagger', '?small shield', '?large shield', '?robes', '?leather armor', '?chain mail', '?breast plate', '?plate mail', '?helm', '?potion of dios',],
                [15, '?long sword+1', '?short sword+1', '?mace+1', '?staff of mogref', '?scroll of katino', '?leather armor+1', '?chain mail+1', '?plate mail+1', '?shield+1', '?breast plate+1', '?scroll of badios(a)', '?scroll of halito', '?long sword-1', '?short sword-1', '?mace-1', '?staff+2',],
            ],
        },
        'treasure3':{
            'gold':(80, 5, 0),
            'trap':['none', 'needle', 'gas', 'arrow', 'explode', 'stun', 'teleporter'],
            'loot':[
                [40, '?long sword', '?short sword', '?anointed mace', '?anointed flail', '?staff', '?dagger', '?small shield', '?large shield', '?robes', '?leather armor', '?chain mail', '?breast plate', '?plate mail', '?helm', '?potion of dios',],
                [20, '?long sword+1', '?short sword+1', '?mace+1', '?staff of mogref', '?scroll of katino', '?leather armor+1', '?chain mail+1', '?plate mail+1', '?shield+1', '?breast plate+1', '?scroll of badios(a)', '?scroll of halito', '?long sword-1', '?short sword-1', '?mace-1', '?staff+2',],
            ],
        },
        'treasure4':{
            'gold':(100, 5, 0),
            'trap':['none', 'needle', 'gas', 'arrow', 'explode', 'stun', 'mage'],
            'loot':[
                [50, '?long sword', '?short sword', '?anointed mace', '?anointed flail', '?staff', '?dagger', '?small shield', '?large shield', '?robes', '?leather armor', '?chain mail', '?breast plate', '?plate mail', '?helm', '?potion of dios',],
                [30, '?long sword+1', '?short sword+1', '?mace+1', '?staff of mogref', '?scroll of katino', '?leather armor+1', '?chain mail+1', '?plate mail+1', '?shield+1', '?breast plate+1', '?scroll of badios(a)', '?scroll of halito', '?long sword-1', '?short sword-1', '?mace-1', '?staff+2',],
                [10, '?slayer of dragons', '?helm+1', '?leather-1', '?chain-1', '?breast plate-1', '?shield-1', '?amulet of jewels', '?scroll of badios(b)', '?potion of sopic', '?long sword+1', '?short sword+2', '?mace+2', '?scroll of lomilwa', '?scroll of dilto', '?gloves of copper', '?leather armor+2', '?chain mail+2', '?plate mail+2', '?shield+2'],
            ],
        },
        'treasure5':{
            'gold':(120, 5, 0),
            'trap':['none', 'needle', 'gas', 'arrow', 'explode', 'stun', 'alarm'],
            'loot':[
                [100, '?long sword', '?short sword', '?anointed mace', '?anointed flail', '?staff', '?dagger', '?small shield', '?large shield', '?robes', '?leather armor', '?chain mail', '?breast plate', '?plate mail', '?helm', '?potion of dios',],
                [50, '?long sword+1', '?short sword+1', '?mace+1', '?staff of mogref', '?scroll of katino', '?leather armor+1', '?chain mail+1', '?plate mail+1', '?shield+1', '?breast plate+1', '?scroll of badios(a)', '?scroll of halito', '?long sword-1', '?short sword-1', '?mace-1', '?staff+2',],
                [20, '?slayer of dragons', '?helm+1', '?leather-1', '?chain-1', '?breast plate-1', '?shield-1', '?amulet of jewels', '?scroll of badios(b)', '?potion of sopic', '?long sword+1', '?short sword+2', '?mace+2', '?scroll of lomilwa', '?scroll of dilto', '?gloves of copper', '?leather armor+2', '?chain mail+2', '?plate mail+2'],
            ],
        },
        'treasure6':{
            'gold':(100, 10, 0),
            'trap':['none', 'needle', 'explode', 'stun', 'teleporter', 'mage', 'priest'],
            'loot':[
                [75, '?long sword+1', '?short sword+1', '?mace+1', '?staff of mogref', '?scroll of katino', '?leather armor+1', '?chain mail+1', '?plate mail+1', '?shield+1', '?breast plate+1', '?scroll of badios(a)', '?scroll of halito', '?long sword-1', '?short sword-1', '?mace-1', '?staff+2',],
                [25, '?slayer of dragons', '?helm+1', '?leather-1', '?chain-1', '?breast plate-1', '?shield-1', '?amulet of jewels', '?scroll of badios(b)', '?potion of sopic', '?long sword+1', '?short sword+2', '?mace+2', '?scroll of lomilwa', '?scroll of dilto', '?gloves of copper', '?leather armor+2', '?chain mail+2', '?plate mail+2'],
                [20, '?ehelm+2','?potion of dial', '?ring of porfic', '?were slayer', '?mage masher', '?mace of poison', '?staff of montino', '?blade cusinart', '?amulet of manifo', '?rof of flame', '?echain mail+2', '?nplate mail+2', '?eshield+3', '?amulet of makanito', '?ring of malor', '?scroll of badial', '?short sword-2', '?dagger+2', '?short sword-2', '?dagger+2', '?mace-2', '?staff-2', '?dagger of speed', '?robe of curses', '?leather armor-2', '?chain mail-2', '?breast plate-2', '?shield-2', '?helmet of curses'],
            ],
        },
        'treasure7':{
            'gold':(100, 20, 0),
            'trap':['none', 'needle', 'gas', 'teleporter', 'alarm'],
            'loot':[
                [100, '?long sword+1', '?short sword+1', '?mace+1', '?staff of mogref', '?scroll of katino', '?leather armor+1', '?chain mail+1', '?plate mail+1', '?shield+1', '?breast plate+1', '?scroll of badios(a)', '?scroll of halito', '?long sword-1', '?short sword-1', '?mace-1', '?staff+2',],
                [50, '?slayer of dragons', '?helm+1', '?leather-1', '?chain-1', '?breast plate-1', '?shield-1', '?amulet of jewels', '?scroll of badios(b)', '?potion of sopic', '?long sword+1', '?short sword+2', '?mace+2', '?scroll of lomilwa', '?scroll of dilto', '?gloves of copper', '?leather armor+2', '?chain mail+2', '?plate mail+2'],
                [15, '?ehelm+2','?potion of dial', '?ring of porfic', '?were slayer', '?mage masher', '?mace of poison', '?staff of montino', '?blade cusinart', '?amulet of manifo', '?rof of flame', '?echain mail+2', '?nplate mail+2', '?eshield+3', '?amulet of makanito', '?ring of malor', '?scroll of badial', '?short sword-2', '?dagger+2', '?short sword-2', '?dagger+2', '?mace-2', '?staff-2', '?dagger of speed', '?robe of curses', '?leather armor-2', '?chain mail-2', '?breast plate-2', '?shield-2', '?helmet of curses'],
            ],
        },
        'treasure8':{
            'gold':(100, 40, 0),
            'trap':['none', 'needle', 'gas', 'mage', 'priest'],
            'loot':[
                [70, '?slayer of dragons', '?helm+1', '?leather-1', '?chain-1', '?breast plate-1', '?shield-1', '?amulet of jewels', '?scroll of badios(b)', '?potion of sopic', '?long sword+1', '?short sword+2', '?mace+2', '?scroll of lomilwa', '?scroll of dilto', '?gloves of copper', '?leather armor+2', '?chain mail+2', '?plate mail+2'],
                [25, '?ehelm+2','?potion of dial', '?ring of porfic', '?were slayer', '?mage masher', '?mace of poison', '?staff of montino', '?blade cusinart', '?amulet of manifo', '?rof of flame', '?echain mail+2', '?nplate mail+2', '?eshield+3', '?amulet of makanito', '?ring of malor', '?scroll of badial', '?short sword-2', '?dagger+2', '?short sword-2', '?dagger+2', '?mace-2', '?staff-2', '?dagger of speed', '?robe of curses', '?leather armor-2', '?chain mail-2', '?breast plate-2', '?shield-2', '?helmet of curses'],
                [5, '?breast plate+2', '?gloves of silver', '?sword+3(e)', '?ssword+3(e)', '?dagger of thieves', '?breast plate+3', '?garb of lords', '?muramasablade', '?shrikens', '?cold chain mail', '?eplate mail+3', '?shield+3', '?ring of healing', '?priests ring'],
            ],
        },
        'treasure9':{
            'gold':(100, 80, 0),
            'trap':['none', 'needle', 'gas', 'arrow', 'explode', 'stun', 'teleporter', 'mage', 'priest', 'alarm'],
            'loot':[
                [100, '?slayer of dragons', '?helm+1', '?leather-1', '?chain-1', '?breast plate-1', '?shield-1', '?amulet of jewels', '?scroll of badios(b)', '?potion of sopic', '?long sword+1', '?short sword+2', '?mace+2', '?scroll of lomilwa', '?scroll of dilto', '?gloves of copper', '?leather armor+2', '?chain mail+2', '?plate mail+2'],
                [50, '?ehelm+2','?potion of dial', '?ring of porfic', '?were slayer', '?mage masher', '?mace of poison', '?staff of montino', '?blade cusinart', '?amulet of manifo', '?rof of flame', '?echain mail+2', '?nplate mail+2', '?eshield+3', '?amulet of makanito', '?ring of malor', '?scroll of badial', '?short sword-2', '?dagger+2', '?short sword-2', '?dagger+2', '?mace-2', '?staff-2', '?dagger of speed', '?robe of curses', '?leather armor-2', '?chain mail-2', '?breast plate-2', '?shield-2', '?helmet of curses'],
                [10, '?breast plate+2', '?gloves of silver', '?sword+3(e)', '?ssword+3(e)', '?dagger of thieves', '?breast plate+3', '?garb of lords', '?muramasablade', '?shrikens', '?cold chain mail', '?eplate mail+3', '?shield+3'],
            ],
        },
        'werdnaloot':{
            'gold':(0, 0, 0),
            'loot':[
                [100, 'amulet of werdna'],
            ],
        },
        'controlcenter':{
            'gold':(0, 0, 0),
            'trap':['needle', 'gas', 'arrow', 'explode', 'stun', 'teleporter', 'alarm'],
            'loot':[
                [100, '?potion of latumofis'],
                [100, '?ring of death'],
                [100, '?rod of flame'],
            ],
        },
    }

    #### configuration

    structure = {
        'first':['地下一階', 1, firstfloor, firstfloorevent, firstfloorencounter],
        'second':['地下二階', 2, secondfloor, secondfloorevent, secondfloorencounter],
    }

    start = 'first'
    title = '\x7f 狂王の訓練場 \x7f'
