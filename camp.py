import gfx

import rule

import spell
import dungeon

def camp():
    dungeon.draw()

    gfx.qdraw(gfx.frame, (88, 64, 10, 8, True))
    gfx.qdraw(gfx.text, (104, 72, "呪文を唱える"))
    gfx.qdraw(gfx.text, (104, 80, "アイテムを見る"))
    gfx.qdraw(gfx.text, (104, 88, "メンバーを見る"))
    gfx.qdraw(gfx.text, (104, 96, "再編成"))
    gfx.qdraw(gfx.text, (104, 104, "装備を整える"))
    gfx.qdraw(gfx.text, (104, 112, "立ち去る"))

    gfx.qdraw(gfx.frame, (104, 16, 6, 3, True))
    gfx.qdraw(gfx.text, (112, 24, "キャンプ"))
    rule.group.draw()
    dlist = gfx.qgetlist()

    key, pos = gfx.select(96, 72, 6)

    if key == 'b' or (key == 'a' and pos == 5):
        return (dungeon.seeking, [])
    if key == 'a':
        if pos == 0:
            # cast spell
            while True:
                if len(rule.group.member) == 1:
                    mpos = 0
                else:
                    gfx.qsendlist(dlist)
                    gfx.log([" ", " ", f"{'誰が呪文を唱えますか?':^16}"])
                    key, mpos = rule.group.select()
                    if key == 'b':
                        return(camp, [])
                gfx.qsendlist(dlist)
                rule.group.show(mpos)
                c = rule.group.member[mpos]
                if spell.selectspell(c, 'camp'):
                    spl = c.pop('casting')
                    tgt = spl['target']
                    lvl, school = spell.schoolbyname(spl['name'].lower())
                    if tgt == 'self' or tgt == 'mygroup' or tgt == 'all':
                        tpos = -1
                    elif tgt == 'member':
                        if len(rule.group.member) == 1:
                            c['target'] = 0
                        else:
                            gfx.qsendlist(dlist)
                            tpos = rule.selectmember('誰に唱えますか?')
                            if tpos == -1:
                                return(camp, [])
                            else:
                                c['target'] = tpos
                    c['command'] = school
                    # start spell process
                    gfx.qsendlist(dlist)
                    rule.group.show(mpos)
                    spell.cast(c, spl)
                    return(camp, [])
        elif pos == 1:
            # browse items
            gfx.qsendlist(dlist)
            rule.group.selectitem()
            return(camp, [])
        elif pos == 2:
            # inspect character
            while True:
                if len(rule.group.member) == 1:
                    pos = 0
                else:
                    gfx.qsendlist(dlist)
                    gfx.log([" ", " ", f"{'誰を調べますか?':^16}"])
                    key, pos = rule.group.select()
                    if key == 'b':
                        return(camp, [])
                while True:
                    rkey = rule.character.sheet(rule.group.member[pos])
                    if rkey == 'b':
                        return(camp, [])
                    elif rkey == 'right':
                        pos += 1
                        if pos == len(rule.group.member): pos = 0
                    elif rkey == 'left':
                        pos -= 1
                        if pos < 0:
                            pos = len(rule.group.member)-1
        elif pos == 3:
            # reorder
            pos = 0
            neworder = []
            while pos+1 < len(rule.group.member):
                gfx.qsendlist(dlist)
                ftxt = []
                for i in range(pos):
                    ftxt.append(f" {i+1} {neworder[i]['name']}") 
                gfx.log([f"{'再編成':^16}", " "] + ftxt)
                key, cpos = rule.group.select()
                if key == 'b':
                    if cpos == 0:
                        return(camp, [])
                    else:
                        pos -= 1
                        neworder.pop(-1)
                if key == 'a':
                    if rule.group.member[cpos] not in neworder:
                        neworder.append(rule.group.member[cpos])
                        pos += 1
            for c in rule.group.member:
                if c not in neworder:
                    neworder.append(c)
                    break
            rule.group.member = neworder
            return(camp, [])
        elif pos == 4:
            # equip items
            for c in rule.group.member:
                for slot in rule.character.EQUIPMENT:
                    gfx.qsendlist(dlist)
                    if rule.group.equipitem(c, slot):
                        return(camp, [])
            return(camp, [])
