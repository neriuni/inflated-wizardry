import random

import gfx
import item
import spell

import dungeon

DEBUG = False

ALIGNSTR = {'good':'G', 'neutral':'N', 'evil':'E', '?':'?'}
CLASSSTR = {'fighter':'FIG', 'mage':'MAG', 'priest':'PRI', 'thief':'THI', 'bishop':'BIS', 'samurai':'SAM', 'lord':'LOR', 'ninja':'NIN', '?':'???'}
RACESTR = {'human':'HUMAN', 'elf':'ELF', 'dwarf':'DWARF', 'gnome':'GNOME', 'hobbit':'HOBBIT', '?':'?????'}
ATTRSTR = {'str':'力', 'iq':'知恵', 'pie':'信仰心', 'vit':'生命力', 'agi':'素早さ', 'luc':'運の強さ'}

ALIGNSTRJP = {'good':'善', 'neutral':'中立', 'evil':'悪'}

CLASSRACESTRJP = {
    'fighter':'戦士',
    'mage':'魔法使い',
    'priest':'僧侶',
    'thief':'盗賊',
    'bishop':'司祭',
    'samurai':'侍',
    'lord':'君主',
    'ninja':'忍者',

    'predetor':'狩人',
    'brute':'',
    'scavenger':'',

    'human':'人間',
    'elf':'エルフ',
    'dwarf':'ドワーフ',
    'gnome':'ノーム',
    'hobbit':'ホビット',

    'beast':'獣',
    'humanoid':'亜人',
    'construct':'魔法生物',
    'demon':'悪魔',
    'dragon':'竜',
    'elemental':'精霊',
    'gas':'ガス',
    'giant':'巨人',
    'insect':'昆虫',
    'plant':'植物',
    'ooze':'スライム',
    'undead':'アンデッド',
}


PROCSTR = {
    'poison':'は,-毒をうけた。',
    'paralysis':'は,-痺れて動けなくなった。',
    'critical':'は,-首を刎ねられた!',
    'silence':'は,-黙った。',
    'sleep':'は,-眠った。',
    'confuse':'は,-混乱した。',
    'fear':'は,-恐れている。',
    'charm':'は,-魅了された。',
    'petrify':'は,-石になった。',
    'drain':'レベル-下げられた',
    'drains':'もレベルを-下げられた',
    'dead':'は,-死んだ!',
    'ashed':'は,-灰になった!',
    'lost':'は,-失われた...',
}

BREATHSTR = {
    'poison':'は、-毒の霧を-吹いた。',
    'fire':'は、-炎を-吐いた。',
    'sleep':'は、-息を-吹いた。',
    'petrify':'は、-石化ガスを-吹いた。',
    'cold':'は、-冷気を-吹いた。',
    'dead':'は、-死の光線を-発した。',
}

MADICURE = ['poison', 'paralysis', 'silence', 'sleep', 'confuse', 'fear', 'petrify']

WEEKPERYEAR = 52

class character:
    EQUIPMENT = [
        'main',
        'sub',
        'chest',
        'head',
        'arm',
        'leg',
        'trinket0',
        'trinket1',
    ]

    SLOTSTR = {
        EQUIPMENT[0]:'主',
        EQUIPMENT[1]:'副',
        EQUIPMENT[2]:'胸',
        EQUIPMENT[3]:'頭',
        EQUIPMENT[4]:'腕',
        EQUIPMENT[5]:'脚',
        EQUIPMENT[6]:'飾',
        EQUIPMENT[7]:'飾',
    }
    
    SLOTEQUIPTSTR = {
        'main':'主武器を選んでください',
        'sub':'副武器/盾を選んでください',
        'chest':'鎧を選んでください',
        'head':'兜を選んでください',
        'arm':'籠手を選んでください',
        'leg':'すね当てを選んでください',
        'trinket0':'装飾品1を選んでください',
        'trinket1':'装飾品2を選んでください',
    }

    data = [
        {
            'pc':True,
            'name':'せんしくん',
            'title':None,
            'image':'character/c103',
            'level':5,
            'alignment':'good',
            'class':'fighter',
            'race':'elf',
            'marks':0,
            'exp':7733,
            'age':14*WEEKPERYEAR,
            'rip':0,
            'ac':4,
            'hp':55,
            'hpmax':59,

            'str':18,
            'iq':11,
            'pie':13,
            'vit':18,
            'agi':16,
            'luc':9,

            'fort':0,
            'reflex':0,
            'will':0,
            'fortref':0,
            'fortwill':0,
            'refwill':0,
            'chroma':0,

            'equipment':{'main':'!long sword-1','sub':'large shield','chest':'chain mail'},

            'arcane':[0,0,0,0,0,0,0],
            'arcanemax':[0,0,0,0,0,0,0],
            'arcanelist':[[],[],[],[],[],[],[]],

            'divine':[0,0,0,0,0,0,0],
            'divinemax':[0,0,0,0,0,0,0],
            'divinelist':[[],[],[],[],[],[],[]],
        },

        {
            'pc':True,
            'name':'そうりょちゃん',
            'title':None,
            'image':'character/c071',
            'level':5,
            'alignment':'good',
            'class':'priest',
            'race':'dwarf',
            'marks':0,
            'exp':7733000,
            'age':14*WEEKPERYEAR,
            'rip':0,
            'ac':4,
            'hp':28,
            'hpmax':37,

            'str':18,
            'iq':9,
            'pie':15,
            'vit':18,
            'agi':13,
            'luc':8,

            'fort':0,
            'reflex':0,
            'will':0,
            'fortref':0,
            'fortwill':0,
            'refwill':0,
            'chroma':0,

            'equipment':{'main':'anointed mace','sub':'large shield','chest':'leather armor'},

            'arcane':[0,0,0,0,0,0,0],
            'arcanemax':[0,0,0,0,0,0,0],
            'arcanelist':[[],[],[],[],[],[],[]],

            'divine':[5,3,3,0,0,0,0],
            'divinemax':[5,3,3,0,0,0,0],
            'divinelist':[['kalki','dios','badios','milwa','porfic'],['matu','calfo','manifo'],['lomilwa','dialko','bamatu'],[],[],[],[]],
        },

        {
            'pc':True,
            'name':'しさいこ',
            'title':None,
            'image':'character/c070',
            'level':13,
            'alignment':'good',
            'class':'bishop',
            'race':'human',
            'marks':0,
            'exp':12000,
            'age':14*WEEKPERYEAR,
            'rip':0,
            'ac':4,
            'hp':66,
            'hpmax':66,

            'str':10,
            'iq':18,
            'pie':15,
            'vit':12,
            'agi':13,
            'luc':14,

            'fort':0,
            'reflex':0,
            'will':0,
            'fortref':0,
            'fortwill':0,
            'refwill':0,
            'chroma':0,

            'equipment':{'main':'anointed mace','sub':'large shield','chest':'leather armor'},

            'arcane':[8,8,8,8,8,8,8],
            'arcanemax':[8,8,8,8,8,8,8],
            'arcanelist':[['halito','katino'],['dilto'],['mahalito','molito'],['dalto','lahalito'],['madalto'],['lakanito','haman','zilwan'],['tiltowait','malor','mahaman']],

            'divine':[5,3,3,0,0,0,0],
            'divinemax':[5,3,3,0,0,0,0],
            'divinelist':[['kalki','dios','badios','milwa','porfic'],['matu','calfo','manifo'],['lomilwa','dialko','bamatu'],[],[],[],[]],
        },

        {
            'pc':True,
            'name':'ひ弱ちゃん',
            'title':None,
            'image':'character/c070',
            'level':13,
            'alignment':'good',
            'class':'bishop',
            'race':'human',
            'marks':0,
            'exp':120000,
            'age':60*WEEKPERYEAR+50,
            'rip':0,
            'ac':4,
            'hp':50,
            'hpmax':66,

            'str':10,
            'iq':18,
            'pie':15,
            'vit':2,
            'agi':13,
            'luc':14,

            'fort':0,
            'reflex':0,
            'will':0,
            'fortref':0,
            'fortwill':0,
            'refwill':0,
            'chroma':0,

            'equipment':{'main':'anointed mace','sub':'large shield','chest':'leather armor'},

            'arcane':[8,8,8,8,8,8,8],
            'arcanemax':[8,8,8,8,8,8,8],
            'arcanelist':[['halito','katino'],['dilto'],['mahalito','molito'],['dalto','lahalito'],['madalto'],['lakanito','haman','zilwan'],['tiltowait','malor','mahaman']],

            'divine':[5,3,3,0,0,0,0],
            'divinemax':[5,3,3,0,0,0,0],
            'divinelist':[['kalki','dios','badios','milwa','porfic'],['matu','calfo','manifo'],['lomilwa','dialko','bamatu'],[],[],[],[]],
        },

        {
            'pc':True,
            'name':'倒れてる',
            'title':None,
            'image':'character/c042',
            'level':13,
            'alignment':'good',
            'class':'bishop',
            'race':'human',
            'marks':0,
            'exp':12000,
            'age':14*WEEKPERYEAR,
            'rip':0,
            'ac':4,
            'hp':66,
            'hpmax':66,

            'str':10,
            'iq':18,
            'pie':15,
            'vit':12,
            'agi':13,
            'luc':14,

            'fort':0,
            'reflex':0,
            'will':0,
            'fortref':0,
            'fortwill':0,
            'refwill':0,
            'chroma':0,

            'dead':True,

            'equipment':{'main':'anointed mace','sub':'large shield','chest':'leather armor'},

            'arcane':[8,8,8,8,8,8,8],
            'arcanemax':[8,8,8,8,8,8,8],
            'arcanelist':[['halito','katino'],['dilto'],['mahalito','molito'],['dalto','lahalito'],['madalto'],['lakanito','haman','zilwan'],['tiltowait','malor','mahaman']],

            'divine':[5,3,3,0,0,0,0],
            'divinemax':[5,3,3,0,0,0,0],
            'divinelist':[['kalki','dios','badios','milwa','porfic'],['matu','calfo','manifo'],['lomilwa','dialko','bamatu'],[],[],[],[]],
        },

    ]
    
    def sheet(c):
        while True:
            character.draw(c)
            key, pos = gfx.select(248-8*8, 256-6*8, 5, 0, ['left', 'right'])
            if key == 'b' or (key == 'a' and pos == 4):
                return 'b'
            elif key == 'a':
                if pos == 0:
                    # read spell book
                    canarcane = False
                    candivine = False
                    spltype = None
                    if 'arcane' in c:
                        canarcane = False
                        for a in c['arcane']:
                            if a > 0:
                                canarcane = True
                                spltype = 'arcane'
                                break
                    if 'divine' in c:
                        candivine = False
                        for a in c['divine']:
                            if a > 0:
                                candivine = True
                                if spltype is None:
                                    spltype = 'divine'
                                break
                    if spltype is not None:
                        while True:
                            character.drawspellbook(c, spltype)
                            key = gfx.getkey()
                            if key == 'b':
                                break
                            else:
                                if spltype == 'arcane' and candivine:
                                    spltype = 'divine'
                                elif spltype == 'divine' and canarcane:
                                    spltype = 'arcane'
                elif pos == 1:
                    # use item
                    pass
                elif pos == 2:
                    # identify item
                    pass
                elif pos == 3:
                    # reserved
                    pass
            elif key == 'left' or key == 'right':
                return key


    def draw(c, menu=True):
        global ALIGNSTR, CLASSSTR, RACESTR
        b = 8
        gfx.qdraw(gfx.frame, (0,b,256//8,256//8-2))
        gfx.qdraw(gfx.text, (8, 8+b, f" {c['name']:<10}L{c['level']:>3} {ALIGNSTR[c['alignment']]}-{CLASSSTR[c['class']]} {RACESTR[c['race']]}"))
        gfx.qdraw(gfx.show, (c['image'], 8, 24+b))
        gfx.qdraw(gfx.text, (8+72, 40+b, f"{'力':>4} {c['str']:>2}"))
        gfx.qdraw(gfx.text, (8+72, 48+b, f"{'知恵':>4} {c['iq']:>2}"))
        gfx.qdraw(gfx.text, (8+72, 56+b, f"{'信仰心':>4} {c['pie']:>2}"))
        gfx.qdraw(gfx.text, (8+72, 64+b, f"{'生命力':>4} {c['vit']:>2}"))
        gfx.qdraw(gfx.text, (8+72, 72+b, f"{'素早さ':>4} {c['agi']:>2}"))
        gfx.qdraw(gfx.text, (8+72, 80+b, f"{'運の強さ':>4} {c['luc']:>2}"))

        gfx.qdraw(gfx.text, (8+72, 96+b, f"{'討伐数':>4} {c['marks']:>9}"))
        gfx.qdraw(gfx.text, (8+72+64+56, 96+b, f"死亡数 {c['rip']:>2}"))
        gfx.qdraw(gfx.text, (8+72, 104+b, f"{'経験値':>4} {c['exp']:>16}"))

        gfx.qdraw(gfx.text, (8+72+64, 56+b, f"AC {10-c['ac']:>3}"))
        gfx.qdraw(gfx.text, (8+72+64, 64+b, f"頑健 {c['fort']:>3}"))
        gfx.qdraw(gfx.text, (8+72+64, 72+b, f"反応 {c['reflex']:>3}"))
        gfx.qdraw(gfx.text, (8+72+64, 80+b, f"意志 {c['will']:>3}"))

        gfx.qdraw(gfx.text, (8+72+64, 24+b, f"H.P.{c['hp']:>4}/{c['hpmax']:>4}"))
        gfx.qdraw(gfx.text, (8+72+64, 40+b, f"状態 {status.statstr(c)}"))
        gfx.qdraw(gfx.text, (8+72+64+64, 40+b, f"年齢 {c['age']//WEEKPERYEAR:>2}"))

        gfx.qdraw(gfx.text, (8+72+64+56, 56+b, f"調和 {c['chroma']:>3}"))    # chroma
        gfx.qdraw(gfx.text, (8+72+64+56, 64+b, f"強靭 {c['fortref']:>3}"))    # fort+reflex
        gfx.qdraw(gfx.text, (8+72+64+56, 72+b, f"不屈 {c['fortwill']:>3}"))   # fort+will
        gfx.qdraw(gfx.text, (8+72+64+56, 80+b, f"集中 {c['refwill']:>3}"))    # reflex+will

        gfx.qdraw(gfx.text, (16, 120+b,  "   1  2  3  4  5  6  7"))
        gfx.qdraw(gfx.text, (16, 128+b, f"魔:{c['arcane'][0]:>2}/{c['arcane'][1]:>2}/{c['arcane'][2]:>2}/{c['arcane'][3]:>2}/{c['arcane'][4]:>2}/{c['arcane'][5]:>2}/{c['arcane'][6]:>2}"))
        gfx.qdraw(gfx.text, (16, 136+b, f"僧:{c['divine'][0]:>2}/{c['divine'][1]:>2}/{c['divine'][2]:>2}/{c['divine'][3]:>2}/{c['divine'][4]:>2}/{c['divine'][5]:>2}/{c['divine'][6]:>2}"))
        gfx.qdraw(gfx.text, (16, 144+b, "錬:"))
        gfx.qdraw(gfx.text, (16, 152+b, "超:"))


        for i, slot in enumerate(character.EQUIPMENT):
            gfx.qdraw(gfx.text, (16, 168+i*8+b, f'{character.SLOTSTR[slot]}: '))
            if slot in c['equipment']:
                itm = item.getitembyname(c['equipment'][slot])
                gfx.qdraw(gfx.text, (16+24, 168+i*8+b, f"{item.getnamebyname(c['equipment'][slot]):<10}"))
                if 'weapon' in itm['type']:
                    # its a weapon
                    if slot == 'main':
                        damage = c['damage'][0]
                        swing = c['swing'][0]
                    else:
                        damage = c['damage'][1]
                        swing = c['swing'][1]
                    gfx.qdraw(gfx.text, (16+24+88, 168+i*8+b, f'{damage[0]}D{damage[1]}+{damage[2]} x{swing}({item.RANGESTR[itm["range"]]})'))
                else:
                    gfx.qdraw(gfx.text, (16+24+88, 168+i*8+b, f"AC -{itm['ac']}"))

        if menu:
            gfx.qdraw(gfx.frame, (256-10*8,256-7*8,10,7,True))
            gfx.qdraw(gfx.text, (256-8*8, 256-6*8, "呪文書を読む"))
            gfx.qdraw(gfx.text, (256-8*8, 256-5*8, "アイテムを使う"))
            gfx.qdraw(gfx.text, (256-8*8, 256-4*8, "鑑定する"))
            gfx.qdraw(gfx.text, (256-8*8, 256-3*8, "-"))
            gfx.qdraw(gfx.text, (256-8*8, 256-2*8, "前に戻る"))

    def drawspellbook(c, spltype):
        global ALIGNSTR, CLASSSTR, RACESTR
        gfx.qdraw(gfx.frame, (0, 8,256//8,256//8-2))
        gfx.qdraw(gfx.text, (8, 16, f" {c['name']:<10}L{c['level']:>3} {ALIGNSTR[c['alignment']]}-{CLASSSTR[c['class']]} {RACESTR[c['race']]}"))
        gfx.qdraw(gfx.frame, (0, 24,32,29,True))

        for i, sl in enumerate(c[spltype+'list']):
            for j, s in enumerate(sl):
                spl = spell.getbyname(s, spltype)
                gfx.qdraw(gfx.text, (16+(i%3)*80, 32+(i//3)*9*8+j*8, spl['name']))
        gfx.qdraw(gfx.text, (8, 240, f"{'[A]呪文書の切り替え [B]戻る':>30}"))

def selectlist(clist, message='冒険者名簿'):
    dlist = gfx.qgetlist()
    charperpage = 28
    pos = 0
    while True:
        gfx.qdraw(gfx.frame, (0, 0,32,32,True))
        for i, c in enumerate(clist[(pos//charperpage)*charperpage:(pos//charperpage)*charperpage+charperpage]):
            ss = status.statstr(c, 1)
            if ss == 'OK':
                ss = RACESTR[c['race']]
            gfx.qdraw(gfx.text, (16, 16+i*8, f"{(pos//charperpage)*charperpage+i+1:>2} {c['name']:<10}L{c['level']:>3} {ALIGNSTR[c['alignment']]}-{CLASSSTR[c['class']]} {ss}"))
        gfx.qdraw(gfx.draw, (8, 16+pos*8, 10, 0), True)
        gfx.qdraw(gfx.text, (8, 256-16, "[左][右] ページ切り替え  [B]戻る  [A]決定"))
        gfx.qdraw(gfx.text, (8, 8, f"{message} ({len(clist)})"))

        key = gfx.getkey(['a', 'b', 'up', 'down', 'right', 'left'])
        if key == 'right':
            pos += len(clist)
            if pos >= len(clist):
                pos = len(clist) -1
        elif key == 'left':
            pos -= len(clist)
            if pos < 0:
                pos = len(clist) -1
        elif key == 'up':
            pos -= 1
            if pos < 0:
                pos = len(clist)-1
        elif key == 'down':
            pos += 1
            if pos == len(clist):
                pos = 0
        elif key == 'a':
            return ('a', pos)
        elif key == 'b':
            return ('b', -1)

class roster:
    member = []

    def add(c):
        refresh(c)
        roster.member.append(c)
        return True
    def remove(index):
        roster.member.pop(index)
        return True

    def select():
        return selectlist(roster.member)

class ootown:
    # format {'x':, 'y':, 'dir':, 'floorname':, 'buff':, 'gold':, 'consumables':, 'loots':, 'member':, 'wiped'}
    groups = []

    def getlist(dim=None, all=False,):
        flatg = []
        for g in ootown.groups:
            if not all and g['wiped']: continue
            if dim is not None and (g['x'], g['y']) != dim: continue
            flatg += g['member']
        return flatg

    def save(wiped=False, justsave=False):
        if len(group.member) > 0:
            c = group.member[0]
            for i, g in enumerate(ootown.groups):
                if c in g['member']:
                    ootown.groups.pop(i)
                    break

        g = {}
        g['x'] = dungeon.dungeon.x
        g['y'] = dungeon.dungeon.y
        g['dir'] = dungeon.dungeon.dir
        g['floorname'] = dungeon.dungeon.floorname

        if wiped:
            g['buff'] = []
            g['gold'] = 0
            g['consumables'] = []
            g['loots'] = []
            g['wiped'] = True
        else:
            g['buff'] = group.buff
            g['gold'] = group.gold
            g['consumables'] = group.consumables
            g['loots'] = group.loots
            g['wiped'] = False
        g['member'] = group.member
        ootown.groups.append(g)

        if not justsave:
            dungeon.dungeon.dir = dungeon.dungeon.NORTH
            group.buff = []
            group.gold = 0
            group.consumables = []
            group.loots = []
            group.clear()

    def pickchar(c, justpick=False):
        if len(group.member) == 6:
            return False
        for i, g in enumerate(ootown.groups):
            if c in g['member']:
                g['member'].remove(c)
                if len(g['member']) == 0:
                    # last person
                    if not justpick:
                        group.gold += g['gold']
                        group.consumables += g['consumables']
                        group.loots += g['loots']
                    ootown.groups.pop(i)
                group.add(c)
                return True
        return False

    def loadfromchar(c):
        for i, g in enumerate(ootown.groups):
            if c in g['member']:
                if g['wiped']:
                    return False
                group.buff = g['buff']
                group.gold = g['gold']
                group.consumables = g['consumables']
                group.loots = g['loots']
                group.clear()
                for cg in g['member']:
                    refresh(cg)
                    group.add(cg)
                dungeon.dungeon.x = g['x']
                dungeon.dungeon.y = g['y']
                dungeon.dungeon.dir = g['dir']
                dungeon.dungeon.loadfloor(g['floorname'])
                ootown.groups.pop(i)
                return True
        return False


class group:
    buff = {}
    member = []
    __orgorder = []

    def clear():
        group.member = []
        group.__orgorder = []
    def add(c):
        if len(group.member) == 6:
            return False
        refresh(c)
        group.member.append(c)
        group.__orgorder.clear()
        for n in group.member:
            group.__orgorder.append(n)
        return True
    def remove(index):
        if len(group.member) < index:
            return False
        group.member.pop(index)
        group.__orgorder.clear()
        for n in group.member:
            group.__orgorder.append(n)
        return True
    def neworder(list):
        group.member.clear()
        group.__orgorder.clear()
        for c in list:
            refresh(c)
            group.member.append(c)
            group.__orgorder.append(c)

    def battleorder():
        group.member.clear()
        tmporder = []
        pos = 0
        for c in group.__orgorder:
            if status.isactive(c) == True:
                group.member.append(c)
                c['pos'] = pos
                pos += 1
            else:
                tmporder.append(c)
        for c in tmporder:
            group.member.append(c)
            c['pos'] = pos
            pos += 1

    def iswiped():
        for c in group.member:
            if status.istreatasdead(c) == False: return False
        return True

    def moral():
        totallvl = 0
        for c in group.member:
            if status.isactive(c):
                totallvl += c['level']
        return totallvl

    charlines = []
    def refresh():
        group.charlines.clear()
        for i, c in enumerate(group.member):
            ss = status.statstr(c, 1)
            if ss == 'OK':
                ss = c['hpmax']
            tx = f" {c['name']:<9}{ALIGNSTR[c['alignment']]}-{CLASSSTR[c['class']]} {10-c['ac']:>2} {c['hp']:>4} {ss:>4}"
            group.charlines.append(tx)
    def draw(update = False):
        gfx.qdraw(gfx.frame, (0,256-80, 32,9,True))
        gfx.qdraw(gfx.text, (8, 256-72, " NAME     CLASS AC HITS STATUS"))
        #if update or len(group.charlines) == 0:
        #        group.refresh()
        group.refresh()
        for i, tx in enumerate(group.charlines):
            gfx.qdraw(gfx.text, (8, 256-8*8+i*8, tx))

    def show(no = 0):
        m = group.member[no]
        gfx.qdraw(gfx.frame, (0, 64, 14, 14,True))
        gfx.qdraw(gfx.show, (m['image'], (112-72)//2,64+112-8-80))

    def select(number = len(member), pos = 0):
        x = 8
        y = 192
        number = len(group.member)
        dlist = gfx.qgetlist()
        chkey = ['a', 'b', 'up', 'down']
        while True:
            gfx.qsendlist(dlist)
            #group.draw()
            group.show(pos)
            gfx.qdraw(gfx.draw, (x, y + pos*8, 10, 0), True)
            key = gfx.getkey(chkey)
            if key == 'a' or key == 'b':
                return (key, pos)
            elif key == 'up':
                pos -= 1
                if pos < 0:
                    pos = number -1
            elif key == 'down':
                pos += 1
                if pos == number:
                    pos = 0

    # quest items. get cleared when wiped
    questitems = ['key of gold', 'statue of frog', 'blue ribbon']
    # consumables
    consumables = []

    # items in storage. these are safe and can sell/identify directly at boltac
    storage = ['scroll of katino', 'potion of dios', '?shield-1', 'long sword+2']
    # items you got while in dungeon. get cleared when wiped
    loots = ['?plate mail', 'shield-2', '?robes', 'blade cusinart', '?robes', '?robes', 'dagger', '?robes', '?robes', '?robes', '?robes', '?robes', '?robes', '?robes', '?robes', '?potion of dios', '?shrikens', '?robes', '?robes', 'dagger of thieves', '?robes', '?robes', '?robes']

    gold = 0
    totalgold = 0

    def equipitem(c, slot, town=False):
        if town:
            itemlist = group.storage
        else:
            itemlist = group.loots

        # check itemlist
        if slot == 'trinket0' or slot == 'trinket1':
            cslot = 'trinket'
        else:
            cslot = slot
        #itemavailable = False
        #for sn in itemlist:
        #    ii = item.getitembyname(sn)
        #    if cslot in ii['slot']:
        #        itemavailable = True
        #        break
        #if not itemavailable:
        #    return

        pos = 0
        dlist = gfx.qgetlist()
        while True:
            gfx.qsendlist(dlist)

            gfx.qdraw(gfx.frame, (0, 256-8*8-8*19, 14, 4, True))
            if slot == 'main' or slot == 'sub':
                if 'main' in c['equipment']:
                    gfx.qdraw(gfx.text, (8, 256-8*8-8*18, f"{character.SLOTSTR['main']}: {item.getnamebyname(c['equipment']['main'])}"))
                else:
                    gfx.qdraw(gfx.text, (8, 256-8*8-8*18, f"{character.SLOTSTR['main']}: 素手"))
                if 'sub' in c['equipment']:
                    gfx.qdraw(gfx.text, (8, 256-8*8-8*17, f"{character.SLOTSTR['sub']}: {item.getnamebyname(c['equipment']['sub'])}"))
                else:
                    gfx.qdraw(gfx.text, (8, 256-8*8-8*17, f"{character.SLOTSTR['sub']}: 素手"))
            elif slot in c['equipment']:
                gfx.qdraw(gfx.text, (8, 256-8*8-8*18, f"{character.SLOTSTR[slot]}: {item.getnamebyname(c['equipment'][slot])}"))
            else:
                gfx.qdraw(gfx.text, (8, 256-8*8-8*18, f"{character.SLOTSTR[slot]}: 装備していない"))

            gfx.qdraw(gfx.frame, (0, 16, 32, 3, True))
            gfx.qdraw(gfx.text, (8, 24, f" {c['name']:<10}L{c['level']:>3} {ALIGNSTR[c['alignment']]}-{CLASSSTR[c['class']]} {RACESTR[c['race']]}"))

            gfx.qdraw(gfx.frame, (112, 256-8*8-8*19, 18, 17, True))

            title = character.SLOTEQUIPTSTR[slot]
            gfx.qdraw(gfx.text, (128, 256-8*8-8*18, title))

            for i, itemname in enumerate(itemlist[(pos//10)*10:(pos//10)*10+10]):
                inx = i%10
                sn = itemlist[i]
                ii = item.getitembyname(sn)
                canequip = '#'
                if cslot in ii['slot'] or 'all' in ii['slot']:
                    if c['class'] in ii['class'] or 'all' in ii['class']:
                        if c['race'] in ii['race'] or 'all' in ii['race']:
                            canequip = ''

                gfx.qdraw(gfx.text, (128, 256-8*8-8*16+inx*8, f"{(pos//10)*10+i+1} {item.getnamebyname(itemname)}"))
                gfx.qdraw(gfx.text, (256-16, 256-8*8-8*16+inx*8, canequip))
            gfx.qdraw(gfx.draw, (120, 256-8*8-8*16+(pos%10)*8, 10, 0), True)
    
            gfx.qdraw(gfx.text, (120, 256-8*8-32, "[B] パス [SEL] はずす"))
            gfx.qdraw(gfx.text, (120, 256-8*8-40, "[START] 終了"))

            gfx.qdraw(gfx.frame, (0, 64, 14, 14,True))
            if len(itemlist) > 0:
                gfx.qdraw(gfx.show, (item.getitembyname(itemlist[pos])['image'][item.isnotidentified(itemlist[pos])], 2, 64+2))
                item.draw(itemlist[pos])
            blist = gfx.qgetlist()

            key = gfx.getkey(['a', 'b', 'up', 'down', 'right', 'left', 'select', 'start'])
            if key == 'right' and len(itemlist) > 0:
                if pos//10 == len(itemlist)//10:
                    pos = pos%10
                else:
                    pos += 10
                    if pos >= len(itemlist):
                        pos = len(itemlist) -1
            elif key == 'left' and len(itemlist) > 0:
                if pos//10 == 0:
                    pos = (len(itemlist)//10)*10+pos%10
                    if pos >= len(itemlist):
                        pos = len(itemlist) -1
                else:
                    pos -= 10
            elif key == 'up' and len(itemlist) > 0:
                pos -= 1
                if pos < 0:
                    pos = len(itemlist)-1
            elif key == 'down' and len(itemlist) > 0:
                pos += 1
                if pos == len(itemlist):
                    pos = 0
            elif key == 'select':
                # unequip
                if slot in c['equipment'] and item.iscursed(c['equipment'][slot]) == 0:
                    ce = c['equipment'].pop(slot)
                    itemlist.append(ce)
                    return False
                else:
                    gfx.qsendlist(blist)
                    gfx.simplemessage('* 呪われている *')
                    gfx.getkey(['a', 'b'])
            elif key == 'a' and len(itemlist) > 0:
                # try equip
                sn = itemlist[pos]
                ii = item.getitembyname(sn)
                if cslot in ii['slot'] or 'all' in ii['slot']:
                    if c['class'] in ii['class'] or 'all' in ii['class']:
                        if c['race'] in ii['race'] or 'all' in ii['race']:
                            # curse check
                            if slot in c['equipment'] and item.iscursed(c['equipment'][slot]) == 1:
                                gfx.qsendlist(blist)
                                gfx.simplemessage('* 呪われている *')
                                gfx.getkey(['a', 'b'])
                            else:
                                # unequip
                                if slot in c['equipment']:
                                    ce = c['equipment'].pop(slot)
                                    itemlist.append(ce)
                                itemlist.pop(pos)
                                # curse check
                                if 'cursed' in ii and ii['cursed']:
                                    c['equipment'][slot] = '!'+sn
                                    gfx.qsendlist(blist)
                                    gfx.simplemessage('* 呪われている *')
                                    gfx.getkey(['a', 'b'])
                                elif 'alignment' in ii and (c['alignment'] not in ii['alignment'] and 'all' not in ii['alignment']) and 'cursedunmatch' in ii and ii['cursedunmatch']:
                                    c['equipment'][slot] = '!'+sn
                                    gfx.qsendlist(blist)
                                    gfx.simplemessage('* 呪われている *')
                                    gfx.getkey(['a', 'b'])
                                else:
                                    c['equipment'][slot] = sn
                                refresh(c)
                                return
            elif key == 'b':
                return False
            elif key == 'start':
                return True


    def selectitem(town=False, bag=None, mode='browse'):
        dlist = gfx.qgetlist()
        bagcycle = []
        equipment = []
        if bag is None:
            if town:
                if len(group.storage) > 0: bagcycle.append(group.storage)
                if mode != 'prepare':
                    if len(group.questitems) > 0: bagcycle.append(group.questitems)
            else:
                if len(group.loots) > 0: bagcycle.append(group.loots)
                if len(group.consumables) > 0: bagcycle.append(group.consumables)
                if len(group.questitems) > 0: bagcycle.append(group.questitems)
            if len(bagcycle) == 0: return None
        else:
            if len(bag) == 0: return None
            if mode == 'use' or mode == 'cursed':
                for i in bag['equipment'].keys():
                    equipment.append(bag['equipment'][i])
                bagcycle.append(equipment)
            else:
                bagcycle.append(bag)
        itemlist = bagcycle[0]
        bagpos = 0
        selecteditems = []

        pos = 0
        while True:
            gfx.qsendlist(dlist)

            gfx.qdraw(gfx.frame, (0, 256-8*8-8*19, 14, 4, True))
            if mode == 'sell':
                message = 'どれを売りますか?'
            elif mode == 'identify':
                message = 'どれを鑑定しますか?'
            elif mode == 'prepare':
                message = 'どれを持ち出しますか?'
            elif mode == 'use':
                message = 'どれを使いますか?'
            elif mode == 'cursed':
                message = 'どれを解呪しますか?'
            else:
                message = ''
            
            gfx.qdraw(gfx.text, (8, 256-8*8-8*18, message))
            if town:
                gfx.qdraw(gfx.text, (8, 256-8*8-8*17, f"資産:{group.totalgold:>9}"))
            else:
                gfx.qdraw(gfx.text, (8, 256-8*8-8*17, f"所持金:{group.gold:>8}"))

            gfx.qdraw(gfx.frame, (112, 256-8*8-8*19, 18, 17, True))
            if itemlist == group.loots:
                title = '戦利品'
            elif itemlist == group.storage:
                title = '倉庫'
            elif itemlist == group.consumables:
                title = '消耗品'
            elif itemlist == group.questitems:
                title = 'キーアイテム'
            elif itemlist == equipment:
                title = '装備品'
            title += f"({pos+1}/{len(itemlist)})"
            gfx.qdraw(gfx.text, (128, 256-8*8-8*18, title))

            for i, itemname in enumerate(itemlist[(pos//10)*10:(pos//10)*10+10]):
                inx = i%10
                #gfx.qdraw(gfx.text, (128, 256-8*8-8*16+inx*8, f"{(pos//10)*10+i+1:>2} {item.getnamebyname(itemname)}"))
                gfx.qdraw(gfx.text, (128, 256-8*8-8*16+inx*8, f"{'*' if (pos//10)*10+i in selecteditems else ''}{item.getnamebyname(itemname)}"))
                if mode == 'sell':
                    if item.isnotidentified(itemname) == 0:
                        gfx.qdraw(gfx.text, (128+8*8, 256-8*8-8*16+inx*8, f"{item.getitembyname(itemname)['cost'][1]:>7}"))
                elif mode == 'identify':
                    if item.isnotidentified(itemname) == 1:
                        gfx.qdraw(gfx.text, (128+8*8, 256-8*8-8*16+inx*8, f"{item.getitembyname(itemname)['cost'][2]:>7}"))
                elif mode == 'prepare':
                        gfx.qdraw(gfx.text, (256-16, 256-8*8-8*16+inx*8, f"{'#' if 'consumable' not in item.getitembyname(itemname)['slot'] else ''}"))
                elif mode == 'cursed':
                    if item.iscursed(itemname) == 1:
                        gfx.qdraw(gfx.text, (128+8*8, 256-8*8-8*16+inx*8, f"{item.getitembyname(itemname)['cost'][0]:>7}"))
            gfx.qdraw(gfx.draw, (120, 256-8*8-8*16+(pos%10)*8, 10, 0), True)

            gfx.qdraw(gfx.text, (120, 256-8*8-32, f"[左右] 次頁 {'[SEL] 別袋' if len(bagcycle) > 1 else ''}"))
            if mode == 'prepare' or mode == 'sell' or mode == 'identify' or mode == 'use' or mode == 'cursed':
                gfx.qdraw(gfx.text, (120, 256-8*8-40, "[A] 選択 [B] 終了"))
            elif mode == 'browse':
                gfx.qdraw(gfx.text, (120, 256-8*8-40, "[START] 捨てる"))

            gfx.qdraw(gfx.frame, (0, 64, 14, 14,True))
            gfx.qdraw(gfx.show, (item.getitembyname(itemlist[pos])['image'][item.isnotidentified(itemlist[pos])], 2, 64+2))

            item.draw(itemlist[pos])
            blist = gfx.qgetlist()
            key = gfx.getkey(['a', 'b', 'up', 'down', 'right', 'left', 'select', 'start'])
            if key == 'select':
                bagpos += 1
                if bagpos == len(bagcycle): bagpos = 0
                itemlist = bagcycle[bagpos]
                pos = 0
            elif key == 'right':
                if pos == len(itemlist) -1:
                    pos = 0
                else:
                    pos += 10
                    if pos >= len(itemlist):
                        pos = len(itemlist) -1
            elif key == 'left':
                pos -= 10
                if pos < 0:
                    pos = len(itemlist) -1
            elif key == 'up':
                pos -= 1
                if pos < 0:
                    pos = len(itemlist)-1
            elif key == 'down':
                pos += 1
                if pos == len(itemlist):
                    pos = 0
            elif key == 'a':
                if mode == 'sell':
                    if item.isnotidentified(itemlist[pos]) == 0 and item.getitembyname(itemlist[pos])['cost'][1] != 0:
                        if town:
                            group.totalgold += item.getitembyname(itemlist[pos])['cost'][1]
                            # increase stock
                            if itemlist[pos] in item.TOWNSHOP:
                                if item.TOWNSHOP[itemlist[pos]] != -1:
                                    item.TOWNSHOP[itemlist[pos]] += 1
                            else:
                                item.TOWNSHOP[itemlist[pos]] = 1
                        else:
                            group.gold += item.getitembyname(itemlist[pos])['cost'][1]
                        itemlist.pop(pos)
                        pos = 0
                        gfx.qsendlist(blist)
                        gfx.simplemessage('* 他に御用はございませんか? *')
                        gfx.getkey(['a', 'b'])
                        if len(itemlist) == 0:
                            if len(bagcycle) == 1:
                                return None
                            newbagcycle = []
                            for bc in bagcycle:
                                if bc != itemlist:
                                    newbagcycle.append(bc)
                            bagcycle = newbagcycle
                            itemlist = bagcycle[0]
                    else:
                        gfx.qsendlist(blist)
                        gfx.simplemessage('* そんな物には興味がありません。 *')
                        gfx.getkey(['a', 'b'])
                elif mode == 'identify':
                    if item.isnotidentified(itemlist[pos]) == 1:
                        gfx.qsendlist(blist)
                        if town:
                            checkmoney = group.totalgold
                        else:
                            checkmoney = group.gold

                        if checkmoney < item.getitembyname(itemlist[pos])['cost'][2]:
                            gfx.simplemessage('* 識別するには、お金が足りません。 *')
                            gfx.getkey(['a', 'b'])
                        else:
                            gfx.simplemessage('* 他に御用はございませんか? *')
                            gfx.getkey(['a', 'b'])
                            checkmoney -= item.getitembyname(itemlist[pos])['cost'][2]
                            if town:
                                group.totalgold = checkmoney
                            else:
                                group.gold = checkmoney
                            itemlist[pos] = item.identify(itemlist[pos])

                            needidentify = False
                            for ii in group.storage:
                                if item.isnotidentified(ii) == 1:
                                    needidentify = True
                                    break
                            if not needidentify:
                                for ii in group.questitems:
                                    if item.isnotidentified(ii) == 1:
                                        needidentify = True
                                        break
                                if not needidentify:
                                    gfx.qsendlist(blist)
                                    gfx.simplemessage('* 未鑑定品はお持ちでないようです。 *')
                                    gfx.getkey(['a', 'b'])
                                    return None
                    else:
                        gfx.qsendlist(blist)
                        gfx.simplemessage('* それが何であるか、すでにわかっています。 *')
                        gfx.getkey(['a', 'b'])
                elif mode == 'cursed':
                    if item.iscursed(itemlist[pos]) == 1:
                        gfx.qsendlist(blist)
                        if town:
                            checkmoney = group.totalgold
                        else:
                            checkmoney = group.gold


                        if checkmoney < item.getitembyname(itemlist[pos])['cost'][0]:
                            gfx.simplemessage('* 解呪するには、お金が足りません。 *')
                            gfx.getkey(['a', 'b'])
                        else:
                            gfx.simplemessage('* 他に御用はございませんか? *')
                            gfx.getkey(['a', 'b'])
                            checkmoney -= item.getitembyname(itemlist[pos])['cost'][0]
                            if town:
                                group.totalgold = checkmoney
                            else:
                                group.gold = checkmoney

                            for i, k in enumerate(bag['equipment'].keys()):
                                if pos == i:
                                    bag['equipment'].pop(k)
                                    refresh(bag)
                                    break

                            cursed = False
                            for i in bag['equipment'].keys():
                                if item.getitembyname(bag['equipment'][i])['cursed']:
                                    cursed = True
                                    break
                            if not cursed:
                                gfx.qsendlist(blist)
                                gfx.simplemessage('* あなたはもう呪われていませんよ。 *')
                                gfx.getkey(['a', 'b'])
                                return None
                            equipment = []
                            for i in bag['equipment'].keys():
                                equipment.append(bag['equipment'][i])
                            bagcycle = [equipment]
                            itemlist = bagcycle[0]
                    else:
                        gfx.qsendlist(blist)
                        gfx.simplemessage('* それには呪いがかかっていませんよ *')
                        gfx.getkey(['a', 'b'])
                elif mode == 'prepare':
                    if pos in selecteditems:
                        selecteditems.pop(pos)
                    elif 'consumable' in item.getitembyname(itemlist[pos])['slot']:
                        selecteditems.append(pos)
            elif key == 'start':
                if mode == 'browse':
                    itemlist.pop(pos)
                    pos = 0
                    gfx.qsendlist(blist)
                    gfx.simplemessage('* 捨てた *')
                    gfx.getkey(['a', 'b'])
                    if len(itemlist) == 0:
                        if len(bagcycle) == 1:
                            return None
                        newbagcycle = []
                        for bc in bagcycle:
                            if bc != itemlist:
                                newbagcycle.append(bc)
                        bagcycle = newbagcycle
                        itemlist = bagcycle[0]
            elif key == 'b':
                if mode == 'prepare':
                    newstorage = []
                    for i, sti in enumerate(group.storage):
                        if i in selecteditems:
                            group.consumables.append(sti)
                        else:
                            newstorage.append(sti)
                    group.storage = newstorage
                return None

def selectmember(t, range=len(group.member)):
    if len(group.member) > 1:
        group.draw()
        gfx.log([
            '',
            f' {t}',
        ])

        key, pos = group.select(range)
        if key == 'a':
            return pos
        else:
            return -1
    else:
        return 0



##### rule #####

RESIST2SAVING = {
    'poison':'fort',
    'paralysis':'fort',
    'death':'fort',
    'crossbow':'reflex',
    'critical':'reflex',
    'gas':'reflex',
    'silence':'will',
    'sleep':'will',
    'confuse':'will',
    'fear':'will',
    'charm':'will',
    'breath':'fortref',
    'fire':'fortref',
    'petrify':'fortwill',
    'drain':'fortwill',
    'cold':'refwill',
    'negative':'refwill',
    'positive':'chroma',
    'physical':'chroma',
    'nuke':'prismatic',
}

__SAVINGBONUS = {
    'fighter':[2, 0, 0],
    'mage':[0, 0, 2],
    'priest':[1, 0, 1],
    'thief':[0, 2, 0],
    'bishop':[1, 0, 2],
    'samurai':[1, 1, 1],
    'lord':[2, 0, 1],
    'ninja':[1, 2, 1],

    'human':[1, 1, 0],
    'elf':[-1, 0, 3],
    'dwarf':[3, -1, 0],
    'gnome':[1, 0, 1],
    'hobbit':[0, 3, -1],

    'predetor':[0, 3, 0],
    'brute':[3, 0, 0],
    'scavenger':[0, 0, 3],

    'beast':[2, 2, -2],
    'humanoid':[1, 1, 0],
    'construct':[2, 2, 10],
    'demon':[6, 6, 6],
    'dragon':[8, 5, 4],
    'elemental':[2, 2, 10],
    'gas':[2, 5, 10],
    'giant':[6, 0, -4],
    'insect':[0, 2, 0],
    'plant':[6, -4, 2],
    'ooze':[2, -4, 6],
    'undead':[2, -2, 10],
}

# tohit, evade, casting, speed, friendlyrate
__MOBBONUS = {
    'fighter':[4, 1, -3, 0],
    'mage':[-3, 0, 5, 0],
    'priest':[1, -2, 3, 0],
    'thief':[0, 4, -4, 2],
    'bishop':[0, -2, 4, 0],
    'samurai':[2, 0, 2, 0],
    'lord':[4, -1, 2, -1],
    'ninja':[3, 4, -7, 4],

    'predetor':[4, 3, -6, 2],
    'brute':[7, -2, 0, -2],
    'scavenger':[6, -4, 3, -2],

    'human':[0, 0, 0, 0, 11],
    'elf':[0, 0, 0, 0, 6],
    'dwarf':[0, 0, 0, 0, 8],
    'gnome':[0, 0, 0, 0, 8],
    'hobbit':[0, 0, 0, 0, 11],

    'beast':[0, 0, 0, 1, 0],
    'humanoid':[0, 0, 0, 0, 6],
    'construct':[0, 0, 0, 0, 1],
    'demon':[0, 0, 0, 1, 0],
    'dragon':[0, 0, 0, 1, 26],
    'elemental':[0, 0, 0, -1, 1],
    'gas':[0, 0, 0, -1, 0],
    'giant':[0, 0, 0, 0, 3],
    'insect':[0, 0, 0, 1, 0],
    'plant':[0, 0, 0, -1, 1],
    'ooze':[0, 0, 0, -1, 0],
    'undead':[0, 0, 0, 0, 0],
}

__THIEVERY = {
    'fighter':-5,
    'ninja':-2,
    'thief':0,
    'bishop':-20,
    'mage':-20,
    'priest':-20,
    'lord':-20,
    'samurai':-20
}

__EXP = {
    'fighter':[1000, 724, 1248, 2152, 3710, 6397, 11029, 19015, 32785, 56526, 97458, 168031, 289709],
    'mage':[1100, 796, 1372, 2366, 4079, 7033, 12126, 20907, 36046, 62149, 107153, 184747, 318529],
    'priest':[1050, 760, 1310, 2259, 3895, 6715, 11578, 19962, 34417, 59343, 102307, 176397, 304132],
    'thief':[900, 651, 1123, 1936, 3338, 5755, 9922, 17107, 29495, 50854, 87679, 151171, 260639],
    'bishop':[1200, 905, 1587, 2785, 4886, 8572, 15038, 26383, 46286, 81203, 142462, 249933, 438479],
    'samurai':[1250, 942, 1653, 2900, 5088, 8926, 15660, 27473, 48199, 84559, 148350, 260263, 456601],
    'lord':[1300, 980, 1720, 3017, 5293, 9286, 16291, 28581, 50142, 87968, 154330, 270755, 475008],
    'ninja':[1450, 1093, 1918, 3368, 5900, 10356, 18169, 31875, 55921, 98107, 172118, 301961, 529757],
}

RACEBASEATTR = {
    'human':{'str':8, 'iq':8, 'pie':5, 'vit':8, 'agi':8, 'luc':9},
    'elf':{'str':7, 'iq':10, 'pie':10, 'vit':6, 'agi':9, 'luc':6},
    'dwarf':{'str':10, 'iq':7, 'pie':10, 'vit':10, 'agi':5, 'luc':6},
    'gnome':{'str':7, 'iq':7, 'pie':10, 'vit':8, 'agi':10, 'luc':7},
    'hobbit':{'str':5, 'iq':7, 'pie':7, 'vit':6, 'agi':10, 'luc':15},
}
CLASSREQATTR = {
    'fighter':{'str':11, 'iq':0, 'pie':0, 'vit':0, 'agi':0, 'luc':0},
    'mage':{'str':0, 'iq':11, 'pie':0, 'vit':0, 'agi':0, 'luc':0},
    'priest':{'str':0, 'iq':0, 'pie':11, 'vit':0, 'agi':0, 'luc':0},
    'thief':{'str':0, 'iq':0, 'pie':0, 'vit':0, 'agi':11, 'luc':0},
    'bishop':{'str':0, 'iq':12, 'pie':12, 'vit':0, 'agi':0, 'luc':0},
    'samurai':{'str':15, 'iq':11, 'pie':10, 'vit':14, 'agi':10, 'luc':0},
    'lord':{'str':15, 'iq':12, 'pie':12, 'vit':15, 'agi':14, 'luc':15},
    'ninja':{'str':17, 'iq':17, 'pie':17, 'vit':17, 'agi':17, 'luc':17},
}
CLASSREQALIGN = {
    'fighter':['good','neutral','evil'],
    'mage':['good','neutral','evil',],
    'priest':['good','evil',],
    'thief':['neutral','evil',],
    'bishop':['good','evil',],
    'samurai':['good','neutral'],
    'lord':['good',],
    'ninja':['evil',],
}
CLASSINITIALSPELL = {
    'mage':{'arcane':[2, ['halito', 'katino']]},
    'bishop':{'arcane':[2, ['halito', 'katino']]},
    'priest':{'divine':[2, ['dios', 'badios']]},
}
CLASSPROC = {
    'fighter':{},
    'mage':{},
    'priest':{},
    'thief':{},
    'bishop':{},
    'samurai':{},
    'lord':{},
    'ninja':{'critical':0},
}
CLASSSPECIAL = {
    'fighter':{},
    'mage':{},
    'priest':{'dispell':1},
    'thief':{},
    'bishop':{'dispell':4},
    'samurai':{},
    'lord':{'dispell':9},
    'ninja':{},
}
HPROLL = {
    'fighter':10,
    'mage':4,
    'priest':8,
    'thief':6,
    'bishop':6,
    'samurai':8,
    'lord':10,
    'ninja':6,
}
DISPELLRATE = {
    'fighter':0,
    'mage':0,
    'priest':0,
    'thief':0,
    'bishop':-20,
    'samurai':0,
    'lord':-40,
    'ninja':0,
}
__LEARNSPELL = {
    # [start, perlevel]
    'mage':{'arcane':[0,2]},
    'priest':{'divine':[0,2]},
    'bishop':{'arcane':[0,4], 'divine':[3,4]},
    'lord':{'divine':[3,2]},
    'samurai':{'arcane':[3,3]},
}

random.seed()
class dice:
    result = 0
    def roll(dc=(1,100,0)):
        dice.result = 0
        if dc[1] > 1:
            for i in range(dc[0]):
                dice.result += random.randint(1, dc[1])
        else:
            dice.result = dc[0] * dc[1]
        dice.result = dice.result+dc[2]
        return dice.result
    def check(val):
        dice.result = random.randint(1, 100)
        return dice.result < val

    lastrate = 0
    def vsroll(attacker, defender, attacker_mod, defender_mod=None):
        global RESIST2SAVING
        if type(attacker_mod) is str:
            if attacker_mod == 'unresist':
                return True
            attacker_mod = attacker[attacker_mod]
        if defender_mod is None:
            defender_mod = defender[RESIST2SAVING[attacker_mod]]
        elif type(defender_mod) is str:
            defender_mod = defender[defender_mod]
        rate = ((10 + attacker['level'] + attacker_mod) - (defender['level'] + defender_mod)) * 5
        #print(f"{rate} a {attacker['level']} - {attacker_mod} d {defender['level']} - {defender_mod}")
        if rate < 5:
            rate = 5
        elif rate > 95:
            rate = 95
        dice.lastrate = rate
        r = dice.check(rate)
        return r


class status:
    STATUSSTR = {
        'lost':['喪失', '失われている'],
        'ashed':['灰','灰化している'],
        'dead':['死','死んでいる'],
        'petrify':['石化','石化している'],
        'paralysis':['麻痺','痺れている'],
        'sleep':['睡眠','眠っている'],
        'poison':['毒','中毒している'],
        'fear':['恐怖','恐れている'],
    }

    def statstr(c, message=0):
        # may not know Im confused or silenced
        for k in status.STATUSSTR.keys():
            if k in c:
                return status.STATUSSTR[k][message]
        return('OK')

    def isactive(ent):
        if 'sleep' in ent: return False
        if 'paralysis' in ent: return False
        if 'petrify' in ent: return False
        if status.isdead(ent): return False
        return True

    def istreatasdead(ent):
        if 'paralysis' in ent: return True
        if 'petrify' in ent: return True
        if status.isdead(ent): return True
        return False

    def cancast(ent):
        if 'fear' in ent: return False
        if 'confuse' in ent: return False
        if 'silence' in ent: return False
        return status.isactive(ent)

    def isdead(ent):
        if 'dead' in ent: return True
        if 'ashed' in ent: return True
        if 'lost' in ent: return True
        return False

    def isneedtemple(ent):
        if 'poison' in ent: return True
        if 'paralysis' in ent: return True
        if 'petrify' in ent: return True
        if 'dead' in ent: return True
        if 'ashed' in ent: return True
        return False

##### encounter check
def encountercheck(kick=False):
    if kick:
        return dice.check(13)
    else:
        return dice.check(1)

##### friendly mob check
def friendlycheck(m):
    if 'special' in m:
        if 'hostile' in m['special']:
            return False
    # check alignment
    isgood = False
    for c in group.member:
        if 'good' in c['alignment']:
            isgood = True
        elif 'evil' in c['alignment']:
            isgood = False
            break
    if isgood:
        return dice.check(__MOBBONUS[m['race']][4])
    return False

##### retreat check
def retreatcheck(mmember):
    ptbonus = 20 - len(group.member)*5
    if ptbonus < 0: ptbonus = 0

    plvl = 0
    for c in group.member:
        plvl += c['level']
    for mg in mmember:
        for m in mg:
            plvl -= m['level']
    if plvl > 0:
        ptbonus += 20

    return dice.check(ptbonus+75)

##### identify mob
def monsteridentify():
    known = 0
    for c in group.member:
        if status.cancast(c):
            if dice.check(c['iq'] + c['pie']):
                known += 1
    return known

##### ambush check
def surprisecheck(m):
    if 'light' in group.buff:
        if group.buff['light'] > 0:
            return 0
    agiluc = 0
    for c in group.member:
        al = c['agi'] + c['luc']
        if agiluc < al: agiluc = al
    
    spieve = (__MOBBONUS[m['race']][1] + __MOBBONUS[m['race']][3] + __MOBBONUS[m['class']][1] + __MOBBONUS[m['class']][3]) * 5
    if spieve < 5:
        spieve = 5
    elif spieve > 95:
        spieve = 95

    if dice.check(spieve):
        if dice.check(agiluc):
            return 0
        else:
            # monster surprised you!
            return -1
    if dice.check(agiluc//3):
        # you surprised the monster
        return 1
    return 0

#### calculate combat order
def speed(ent):
    spd = dice.roll((1, 10, 0)) + ent['speed']
    ent['speed'] = spd
    return spd

#### calculate range of mainhand weapon
def weaponrange(ent, mobgroupnum):
    r = 0
    if 'pc' in ent:
        r = 0
        if 'main' in ent['equipment']:
            ii = item.getitembyname(ent['equipment']['main'])
            if 'weapon' in ii['type']:
                r = ii['range']
        if ent['pos'] < 3:
            # front
            r = r * 2 + 2
        else:
            r = r * 2
    elif 'mob' in ent:
        r = ent['range']
        p = ent['pos']
        if p < 2:
            # front
            r = r * 3 + 3
        elif p < 4:
            r = r * 3
        else:
            r = r * 3 -3
    if r < 0: r = 0
    m = mobgroupnum
    if r > m:
        r = m
    ent['range'] = r
    return r

#### melee check
def hitcheck(a, d):
    global RESIST2SAVING
    dmg = 0
    swing = 0
    proc = {}
    if 'pc' in a:
        for s in range(a['swing'][0]):
            if dice.vsroll(a, d, 'tohit', 'ac'):
                # hit success
                #print(f"success {a['name']} {dice.result}")
                swing += 1
                dmg += dice.roll(a['damage'][0])
            #else:
            #    print(f"fail {a['name']} {dice.result} {a['tohit']} {d['ac']}")
        if 'slaymain' in a:
            if d['class'] in a['slaymain']:
                dmg *= 2
            if d['race'] in a['slaymain']:
                dmg *= 2
        if a['pos'] < 3 and d['pos'] < 2:
            # can swing sub weapon
            for s in range(a['swing'][1]):
                if dice.vsroll(a, d, 'tohit', 'ac'):
                    # hit success
                    swing += 1
                    dmg += dice.roll(a['damage'][1])
            if 'slaysub' in a:
                if d['class'] in a['slaysub']:
                    dmg *= 2
                if d['race'] in a['slaysub']:
                    dmg *= 2
    elif 'mob' in a:
        for s in range(a['swing']):
            if dice.vsroll(a, d, 'tohit', 'ac'):
                swing += 1
                dmg += dice.roll(a['damage'][s])
        # protection check
        if 'protect' in d:
            if a['class'] in d['protect']:
                dmg = dmg//2
            if a['race'] in d['protect']:
                dmg = dmg//2

    if swing > 0:
        # sleep check
        if 'sleep' in d:
            dmg *= 2

        # proc check
        if 'proc' in a:
            for sa in a['proc'].keys():
                if dice.vsroll(a, d, a['proc'][sa], RESIST2SAVING[sa]):
                    proc[sa] = a['proc'][sa]

    return (swing,dmg,proc)


##### update entity data
def refresh(ent):
    global __SAVINGBONUS
    if 'pc' in ent:
        l = ent['luc']//6
        f = ent['vit']//6
        r = ent['agi']//6
        if ent['iq'] > ent['pie']:
            wi = ent['iq']//6
        else:
            wi = ent['pie']//6
        cl = ent['class']
        ra = ent['race']

        ent['proc'] = {} | CLASSPROC[ent['class']]
        ent['special'] = {} | CLASSSPECIAL[ent['class']]

        # refresh ac, tohit, spellpower and other
        ac = 0
        tohit = 0
        arcanepower = 0
        divinepower = 0
        thievery = 0
        speed = 0
        swingmod = 0
        heal = 0
        slay = []
        protect = []
        for ii in character.EQUIPMENT:
            if ii in ent['equipment']:
                i = item.getitembyname(ent['equipment'][ii])
                ac += i['ac']
                tohit += i['tohit']
                arcanepower += i['arcanepower']
                divinepower += i['divinepower']
                thievery += i['thievery']
                speed += i['speed']
                swingmod += i['swing']
                heal += i['heal']
                if 'protect' in i:
                    for st in i['protect']:
                        protect.append(st)
                if i['type'] != 'weapon':
                    if 'slay' in i:
                        for st in i['slay']:
                            slay.append(st)
                if 'proc' in i:
                    ent['proc'] |= i['proc']

        ent['arcanepower'] = arcanepower + ent['iq']//3
        ent['divinepower'] = arcanepower + ent['pie']//3
        ent['tohit'] = tohit + ent['str']//3
        ent['speed'] = speed + ent['agi']//6

        ent['thievery'] = thievery + l + r*2 + __THIEVERY[cl]

        maxlvl = 0
        for i in ent['arcanemax']:
            if i == 0:
                break
            maxlvl += 1
        if maxlvl != 0:
            ent['arcanemaxlvl'] = maxlvl
        maxlvl = 0
        for i in ent['divinemax']:
            if i == 0:
                break
            maxlvl += 1
        if maxlvl != 0:
            ent['divinemaxlvl'] = maxlvl

        # get ac
        ac = ac + ent['agi']//6
        if 'shield' in ent:
            ac += ent['shield']
        if 'petrify' in ent:
            ac += 2
        if 'parry' in ent:
            ac += 2
        if 'shield' in group.buff:
            ac += group.buff['shield']
        # ninja ac mod
        if ent['class'] == 'ninja':
            if 'chest' not in ent['equipment'] and 'head' not in ent['equipment'] and 'arm' not in ent['equipment'] and 'leg' not in ent['equipment']:
                ac += 1 + ent['level']//3
        ent['ac'] = ac

        # get weapon damage
        if ent['class'] == 'fighter' or ent['class'] == 'samurai' or ent['class'] == 'lord':
            mainswing = 1 + ent['level']//5
            subswing = 1 + ent['level']//8
        elif ent['class'] == 'thief' or ent['class'] == 'ninja':
            mainswing = 2 + ent['level']//5
            subswing = 1 + ent['level']//8
        elif ent['class'] == 'priest' or ent['class'] == 'bishop':
            mainswing = 1 + ent['level']//6
            subswing = 1
        else:
            mainswing = 1
            subswing = 1

        twohand = False
        if 'main' not in ent['equipment']:
            maindmg = [2,2,ent['str']//6]
        else:
            w = item.getitembyname(ent['equipment']['main'])
            if 'weapon' in w['type']:
                if 'twohand' in w['type']: twohand = True
                maindmg = [w['damage'][0], w['damage'][1], w['damage'][2]]
                if 'dagger' in w['type'] or 'bow' in w['type']:
                    maindmg[2] += ent['agi']//6
                else:
                    maindmg[2] += ent['str']//6
                if 'weakswing' in w:
                    if w['weakswing'] > mainswing: mainswing = w['weakswing']
                ent['slaymain'] = slay.copy()
                if 'slay' in w:
                    for st in w['slay']:
                        ent['slaymain'].append(st)
            else:
                # not a weapon
                maindmg = [0,0,0]
                mainswing = 0

        if twohand:
            subdmg = [0,0,0]
            subswing = 0
        if 'sub' not in ent['equipment']:
            subdmg = [2,2,ent['str']//6]
        else:
            w = item.getitembyname(ent['equipment']['sub'])
            if 'weapon' in w['type']:
                subdmg = [w['damage'][0], w['damage'][1], w['damage'][2]]
                if 'dagger' in w['type'] or 'bow' in w['type']:
                    subdmg[2] += ent['agi']//6
                else:
                    subdmg[2] += ent['str']//6
                if 'weakswing' in w:
                    if w['weakswing'] > subswing: subswing = w['weakswing']
                ent['slaysub'] = slay.copy()
                if 'slay' in w:
                    for st in w['slay']:
                        ent['slaysub'].append(st)
            else:
                # not a weapon
                subdmg = [0,0,0]
                subswing = 0

        if mainswing != 0:
            mainswing += swingmod
            if mainswing < 0: mainswing = 1
        if subswing != 0:
            subswing += swingmod
            if subswing < 0: subswing = 1
        ent['swing'] = [mainswing, subswing]
        ent['damage'] = [(maindmg[0], maindmg[1], maindmg[2]), (subdmg[0], subdmg[1], subdmg[2])]

        # refresh saving
        rfort = __SAVINGBONUS[cl][0] + __SAVINGBONUS[ra][0] + f + l
        rref  = __SAVINGBONUS[cl][1] + __SAVINGBONUS[ra][1] + r + l
        rwill = __SAVINGBONUS[cl][2] + __SAVINGBONUS[ra][2] + wi + l
        ent['fort'] = rfort
        ent['reflex'] = rref
        ent['will'] = rwill
        ent['fortref'] = (rfort + rref)//2
        ent['fortwill'] = (rfort + rwill)//2
        ent['refwill'] = (rref + rwill)//2
        ent['chroma'] = (rref + rwill + rfort)//3
        p = rfort
        if rfort > rref:
            p = rref
        if p > rwill:
            p = rwill
        ent['prismatic'] = p
    elif 'mob' in ent:
        cl = ent['class']
        ra = ent['race']
        # refresh ac
        ac = ent['baseac']
        if 'shield' in ent:
            ac += ent['shield']
        ent['ac'] = ac + __MOBBONUS[cl][1] + __MOBBONUS[ra][1]
        ent['arcanepower'] = __MOBBONUS[cl][2] + __MOBBONUS[ra][2]
        ent['divinepower'] = __MOBBONUS[cl][2] + __MOBBONUS[ra][2]
        ent['tohit'] = __MOBBONUS[cl][0] + __MOBBONUS[ra][0]
        ent['speed'] = __MOBBONUS[cl][3] + __MOBBONUS[ra][3]

        ent['arcanemana'] = 0
        ent['divinemana'] = 0

        ent['swing'] = len(ent['damage'])
        if 'range' not in ent: ent['range'] = 0

        # refresh saving
        cl = ent['class']
        ra = ent['race']
        rfort = __SAVINGBONUS[cl][0] + __SAVINGBONUS[ra][0]
        rref  = __SAVINGBONUS[cl][1] + __SAVINGBONUS[ra][1]
        rwill = __SAVINGBONUS[cl][2] + __SAVINGBONUS[ra][2]
        ent['fort'] = rfort
        ent['reflex'] = rref
        ent['will'] = rwill
        ent['fortref'] = (rfort + rref)//2
        ent['fortwill'] = (rfort + rwill)//2
        ent['refwill'] = (rref + rwill)//2
        ent['chroma'] = (rref + rwill + rfort)//3
        p = rfort
        if rfort > rref:
            p = rref
        if p > rwill:
            p = rwill
        ent['prismatic'] = p

#### breath damage
def breath(a, d):
    global RESIST2SAVING, PROCSTR
    sf = None
    dmg = a['hp']//2
    if dice.vsroll(a, d, 0, RESIST2SAVING['breath']) == False:
        dmg = dmg//2
    if 'protect' in d:
        if a['special']['breath'] in d['protect']:
            dmg = dmg//2
    elif a['special']['breath'] in PROCSTR:
        if dice.vsroll(a, d, 0, RESIST2SAVING[a['special']['breath']]):
            if a['special']['breath'] == 'dead':
                d['dead'] = True
                if 'rip' in d: d['rip'] += 1
            else:
                d[a['special']['breath']] = dice.lastrate
            sf = f"{d['name']}{PROCSTR[a['special']['breath']]}"
    return (dmg,sf)

#### manacheck
def manacheck(cmd, spl):
    lvl, school = spell.schoolbyname(spl['name'].lower())
    if 'pc' in cmd:
        if cmd[school][lvl] > 0:
            cmd[school][lvl] -= 1
            return True
        return False
    elif 'mob' in cmd:
        if dice.check(30):
            cmd[school+'mana'] += 1
        return True

#### add user or target to txt
def addwho(astr, dstr, txt):
    if txt[0] == 'a':
        return astr+txt[1:100]
    if txt[0] == 'd':
        return dstr+txt[1:100]

#### level up
def levelup(c):
    nextexp = 0
    clvl = c['level']
    if clvl < 13:
        for l in range(clvl+1):
            nextexp += __EXP[c['class']][l]
    else:
        for l in range(13):
            nextexp += __EXP[c['class']][l]
        nextexp += (clvl+1-13)*__EXP[c['class']][12]
    if nextexp <= c['exp']:
        # level up!
        txt = [f"次のレベルに上がりました!({c['level']}→{c['level']+1})"]
        c['level'] += 1
        # learn new spell
        spltxt = ''
        spllist = [('arcane','iq'), ('divine','pie')]
        for spltype, chkatr, in spllist:
            if c['class'] in __LEARNSPELL and spltype in __LEARNSPELL[c['class']]:
                spllvl = (c['level'] + 1 - __LEARNSPELL[c['class']][spltype][0]) // __LEARNSPELL[c['class']][spltype][1]
                if spllvl < 0: spllvl = 0
                elif spllvl > 7: spllvl = 7
                for s in range(spllvl):
                    sl = c[spltype+'list'][s]
                    if len(sl) == 0:
                        c[spltype+'list'][s].append(next(iter(spell.data[spltype][s])))
                    for sb in spell.data[spltype][s].keys():
                        if sb not in sl:
                            # attempt to learn
                            if dice.roll((1, 30, 0)) < c[chkatr]:
                                txt.append(f"新しい呪文({spell.data[spltype][s][sb]['name']})を覚えた。")
                                c[spltype+'list'][s].append(sb)
                # update mana
                for i, s in enumerate(c[spltype+'list']):
                    c[spltype+'max'][i] = len(s)
                for i in range(spllvl):
                    sc = c['level']-i*__LEARNSPELL[c['class']][spltype][1]
                    if sc > 9: sc = 9
                    if c[spltype+'max'][i] < sc:
                        c[spltype+'max'][i] = sc
            for i, _ in enumerate(c[spltype]):
                c[spltype][i] = c[spltype+'max'][i]

        # update attribute
        lost = False
        for attr in ATTRSTR.keys():
            if dice.check(25):
                if c['age'] > dice.roll((1, 130, -1))*WEEKPERYEAR:
                    if c[attr] == 18 and dice.roll((1, 7, 0)) != 4:
                        continue
                    else:
                        c[attr] -= 1
                        if attr == 'vit':
                            if c['vit'] <= 2:
                                lost = True
                        txt.append(f"あなたは {ATTRSTR[attr]} を失った。({c[attr]+1}→{c[attr]})")
                elif c[attr] < 18:
                    txt.append(f"あなたは {ATTRSTR[attr]} を得た。({c[attr]}→{c[attr]+1})")
                    c[attr] += 1
        # update max hp
        trycount = c['level']
        if c['class'] == 'samurai':
            trycount += 1
        hpmod = c['vit']//3
        newhp = 0
        for _ in range(trycount):
            h = dice.roll((1, HPROLL[c['class']], hpmod))
            if h < 0: h = 1
            newhp += h
        newhp -= c['hpmax']
        if newhp < 0:
            newhp = 1
        txt.append(f"H.P.が{newhp}上がった。({c['hpmax']}→{c['hpmax']+newhp})")
        c['hpmax'] += newhp
        c['hp'] += newhp

        refresh(c)
        return txt
    return [f"次のレベルには、あと {nextexp - c['exp']}", "E.P.が必要です。"]

#### class change check
def changableclass(c):
    classlist = []
    for cls in CLASSREQATTR.keys():
        # align check
        if c['alignment'] in CLASSREQALIGN[cls]:
            # attribute check
            canchange = True
            for atr in ATTRSTR.keys():
                if c[atr] < CLASSREQATTR[cls][atr]:
                    canchange = False
                    break
            if canchange:
                classlist.append(cls)
    return classlist
