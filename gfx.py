import threading
import pyxel

import zipfile

class timer:
    tick = 0

    def __init__(self):
        self.target = -1
    def update():
        timer.tick += 1
    def set(self, time):
        self.target = timer.tick + time
    def get(self):
        if self.target == -1:
            return 0
        t = self.target - timer.tick
        if t < 0:
            return 0
        else:
            return t

class image:
    data = {}
    zfile = 'data.zip'

    font = []
    maze = []

    activeimage = []

    def init():
        with zipfile.ZipFile(image.zfile) as zf:
            d = []
            with zf.open('font') as zt:
                for fl in zt:
                    d.append(eval(fl.rstrip()))
            image.font = d
            d = []
            with zf.open('maze') as zt:
                for fl in zt:
                    d.append(eval(fl.rstrip()))
            image.maze = d
            pyxel.colors.from_list(d[1])

    def blt(x, y, img, sx, sy, w, h, tcol):
        if img == 1:
            d = image.font[2]
            img_width, img_height = image.font[0]
        elif img == 0:
            d = image.activeimage[2]
            img_width, img_height = image.activeimage[0]
        elif img == 2:
            d = image.maze[2]
            img_width, img_height = image.maze[0]
        screen_ptr = pyxel.screen.data_ptr()
        for j in range(h):
            for i in range(w):
                #print(f"{sy+j} {sx+i} - {img_width} {img_height} {len(d)}")
                p = d[(sy + j)*img_width + sx + i]
                #print(p)
                if p != tcol:
                    screen_ptr[(y + j) * core.width + x + i] = p

    def load(dataname):
        d = []
        if dataname in image.data:
            image.activeimage = image.data[dataname]
            pyxel.colors.from_list(image.data[dataname][1])
        else:
            with zipfile.ZipFile(image.zfile) as zf:
                with zf.open(dataname) as zt:
                    for fl in zt:
                        d.append(eval(fl.rstrip()))
            image.data[dataname] = d
            image.activeimage = d
            pyxel.colors.from_list(d[1])
    def filelist(dataname):
        with zipfile.ZipFile(image.zfile) as zf:
            with zf.open(dataname+'list') as zt:
                return eval(zt.readline().rstrip())

# Draw text
# \x08 : up
# \x09 : down
# \x0a : right
# \x0b : left
# \x10 : infinity left
# \x10 : infinity right
# \x18 : white box
# \x19 : black box
# \x7f : dragon
# \x80 : spade
# \x81 : heart
# \x82 : club
# \x83 : diamond
# \x84 : black circle
# \x85 : white circle
# \xa0 : of
# \xa2 : potion
# \xa3 : scroll
def text(x, y, text, clear=False):
    if clear:
        pyxel.rect(x, y, len(text)*8, 8, 0)
    for char in text:
        code = ord(char)
        if code >= 0x00 and code <= 0xaf:
            image.blt(x, y, 1, (code%16)*8, (code//16)*8, 8, 8, 0)
            x += 8
        elif code in core.fonts:
            font = core.fonts[code]
            dwidth, font_width, font_height, offset_x, offset_y, bitmap = font
            ax = x
            ay = y+1
            color = 1
            screen_ptr = pyxel.screen.data_ptr()
            ax = ax + core.fontboundingbox[2] + offset_x
            ay = ay + core.fontboundingbox[1] + core.fontboundingbox[3] - font_height - offset_y -1
            for j in range(font_height):
                for i in range(font_width):
                    if (bitmap[j] >> i) & 1:
                        screen_ptr[(ay + j) * pyxel.width + ax + i] = color
            x += font[0]

# show image
def show(file, x, y, clear=False, w=0, h=0):
    if file != core.currentimg:
        core.currentimg = file
        #pyxel.load('image/'+file+'.pyxres', tilemap=False, sound=False, music=False)
        #pyxel.load('image/'+file+'.pyxres')
        image.load(file)
    if w > 0:
        aw = w
    else:
        aw = image.activeimage[0][0]
    if h > 0:
        ah = h
    else:
        ah = image.activeimage[0][1]
    if clear == True:
        pyxel.rect(x, y, aw, ah, 0)
    image.blt(x, y, 0, 0, 0, aw, ah, 0)

def frame(x, y, w, h, clear=False):
    if clear == True:
        pyxel.rect(x+1, y+1, w*8-2, h*8-2, 0)
    image.blt(x, y, 1, 0, 0, 8, 8, 0) #ul
    image.blt(x+w*8-8, y, 1, 2*8, 0, 8, 8, 0) #ur
    image.blt(x, y+h*8-8, 1, 5*8, 0, 8, 8, 0) #dl
    image.blt(x+w*8-8, y+h*8-8, 1, 7*8, 0, 8, 8, 0) #dr
    for i in range(w-2):
        image.blt(x+i*8+8, y, 1, 8, 0, 8, 8, 0) #upper
        image.blt(x+i*8+8, y+h*8-8, 1, 6*8, 0, 8, 8, 0) #lower
    for i in range(h-2):
        image.blt(x, y+i*8+8, 1, 3*8, 0, 8, 8, 0) #left
        image.blt(x+w*8-8, y+i*8+8, 1, 4*8, 0, 8, 8, 0) #right

def draw(tx, ty, sx, sy):
    image.blt(tx, ty, 1, sx*8, sy*8, 8, 8, 0)

def clear():
    pyxel.cls(0)

def rect(x, y, w, h):
    pyxel.rect(x, y, w, h, 0)

def blt(x, y, sx, sy):
    image.blt(x, y, 2, sx, sy, 8, 8, 0)

class core(threading.Thread):
    width = 0
    height = 0
    title = None

    keydelay = 5
    eventlock = threading.Condition()
    drawlock = threading.Lock()

    currentimg = None
    img_w = 0
    img_h = 0

    def __init__(self, width, height, title):
        threading.Thread.__init__(self)
        core.width = width
        core.height = height
        core.title = title

    fonts = {}
    fontboundingbox = [0,0,0,0]
    def __parse_bdf():
        code = None
        bitmap = None
        dwidth = 0
        with open("misaki_gothic_2nd.bdf", "r") as f:
            for line in f:
                if line.startswith("ENCODING"):
                    code = int(line.split()[1])
                elif line.startswith("DWIDTH"):
                    dwidth = int(line.split()[1])
                elif line.startswith("BBX"):
                    bbx_data = list(map(int, line.split()[1:]))
                    font_width, font_height, offset_x, offset_y = bbx_data[0], bbx_data[1], bbx_data[2], bbx_data[3]
                elif line.startswith("BITMAP"):
                    bitmap = []
                elif line.startswith("ENDCHAR"):
                    core.fonts[code] = (dwidth, font_width, font_height, offset_x, offset_y, bitmap)
                    bitmap = None
                elif line.startswith("FONTBOUNDINGBOX"):
                    # 0:width 1:height 2:offset_x 3:offset_y
                    core.fontboundingbox = list(map(int, line.split()[1:]))
                elif bitmap is not None:
                    hex_string = line.strip()
                    bin_string = bin(int(hex_string, 16))[2:].zfill(len(hex_string) * 4)
                    bitmap.append(int(bin_string[::-1], 2))

    def run(self):
        self.keysickness = timer()

        pyxel.init(core.width, core.height, title=core.title)
        #pyxel.mouse(True)
        core.__parse_bdf()
        #pyxel.load('image/base.pyxres', tilemap=False, sound=False, music=False)
        #pyxel.load('image/base.pyxres')
        image.init()
        pyxel.run(self.update, self.draw)

    keys = []
    checkkeys = {
        pyxel.KEY_UP:'up',
        pyxel.KEY_DOWN:'down',
        pyxel.KEY_RIGHT:'right',
        pyxel.KEY_LEFT:'left',
        pyxel.KEY_X:'a',
        pyxel.KEY_Z:'b',
        pyxel.KEY_W:'start',
        pyxel.KEY_Q:'select',
    }
    def update(self):
        timer.update()
        if self.keysickness.get() == 0:
            for k in core.checkkeys:
                if pyxel.btn(k):
                    if core.eventlock.acquire():
                        if core.checkkeys[k] not in core.keys:
                            core.keys.append(core.checkkeys[k])
                        core.eventlock.notify()
                        core.eventlock.release()
                        self.keysickness.set(core.keydelay)
                        return

    frontlist = []
    backlist = []
    pastlist = []
    def draw(self):
        if core.drawlock.acquire(blocking=False):
            if len(core.backlist) > 0:
                #if core.pastlist != core.backlist:
                    pyxel.cls(0)
                    for i, fn in enumerate(core.backlist):
                        func, arg, cond = fn
                        if type(cond) is bool:
                            if cond is True:
                                func(*arg)
                        else:
                            c = cond.get()
                            if c > 0:
                                func(*arg)
                            else:
                                # remove endtry
                                core.backlist[i][2] = False
                    core.pastlist = core.backlist.copy()
            core.drawlock.release()


# wait until detecting key input
def getkey(keys=['a', 'b', 'up', 'down'], timeout=0):
    if timeout <= 0:
        entimer = False
    else:
        entimer = True
    if len(core.frontlist) > 0:
        if core.drawlock.acquire():
            core.backlist = core.frontlist.copy()
            core.drawlock.release()
            core.frontlist.clear()
    r = None
    if core.eventlock.acquire():
        while True:
            while not core.eventlock.wait(1):
                if entimer:
                    timeout -= 1
                    if timeout <= 0:
                        core.eventlock.release()
                        return 'time'
            for k in keys:
                if k in core.keys:
                    core.keys.clear()
                    core.eventlock.release()
                    return k

# one line message
def simplemessage(txt):
    tl = len(txt)
    qdraw(frame, (8*((32 - tl)//2)-8, 88, tl+2, 3, True))
    qdraw(text, (8*((32 - tl)//2), 96, txt))

# control display list
def qdraw(funcname, funcargs, condition=True):
    core.frontlist.append([funcname, funcargs, condition])
def qclear():
    core.frontlist.clear()
def qsendlist(newlist):
    if len(newlist) > 0:
        if core.drawlock.acquire():
            core.frontlist = newlist.copy()
            core.drawlock.release()
def qgetlist():
    return core.frontlist.copy()

# selection
def select(x, y, number, pos = 0, outkey = []):
    dlist = qgetlist()
    chkey = ['a', 'b', 'up', 'down'] + outkey
    while True:
        qsendlist(dlist)
        qdraw(draw, (x, y + pos*8, 10, 0), True)
        key = getkey(chkey)
        if key in outkey or key == 'a' or key == 'b':
            return (key, pos)
        elif key == 'up':
            pos -= 1
            if pos < 0:
                pos = number -1
        elif key == 'down':
            pos += 1
            if pos == number:
                pos = 0

# log window
# 12 lines 16 words
def log(texts, center=False, format=False):
    offset = 0
    qdraw(frame, (112, 64, 18, 14,True))
    if format:
        rettxt = texts.split('+')
        texts = []
        for ftxt in rettxt:
            sftxt = ftxt.split('-')
            btxt = ''
            for t in sftxt:
                if len(t) + len(btxt) > 16:
                    texts.append(btxt)
                    btxt = ''
                btxt += t
            if len(btxt) > 0:
                texts.append(btxt)
            texts.append(' ')
        offset = 8
    for i, t in enumerate(texts):
        if i == 12:
            return
        if center:
            qdraw(text, (120, 72+i*8+offset, f'{t:^16}'))
        else:
            qdraw(text, (120, 72+i*8+offset, t))

# word input
selectword = [
    [
        'ア イ ウ エ オ ハ ヒ フ ヘ ホ ァ ィ',
        'カ キ ク ケ コ マ ミ ム メ モ ェ ォ',
        'サ シ ス セ ソ ヤ   ユ   ヨ ッ ャ',
        'タ チ ツ テ ト ラ リ ル レ ロ ュ ョ',
        'ナ ニ ヌ ネ ノ ワ ン ゛ ゜ ー 決定 ',
    ],
    [
        'あ い う え お は ひ ふ へ ほ っ ゃ',
        'か き く け こ ま み む め も ゅ ょ',
        'さ し す せ そ や   ゆ   よ ゛ ゜',
        'た ち つ て と ら り る れ ろ ー  ',
        'な に ぬ ね の わ   を   ん 決定 ',
    ],
    [
        '0 1 2 3 4 5 6 7 8 9 - +',
        'A B C D E F G H I J / *',
        'K L M N O P Q R S T ! ?',
        'U V W X Y Z . , ` # $ &',
        '\x7f \x80 \x81 \x82 \x83 \x84 \x85 \xa0 \xa2 \xa3 決定 '
    ],
    [
        '0 1 2 3 4 5 6 7 8 9 - +',
        'a b c d e f g h i j / *',
        'k l m n o p q r s t ! ?',
        'u v w x y z . , ` # $ &',
        '\x7f \x80 \x81 \x82 \x83 \x84 \x85 \xa0 \xa2 \xa3 決定 '
    ]
]
dakuon = {
    'か':'が', 'き':'ぎ', 'く':'ぐ', 'け':'げ', 'こ':'ご',
    'さ':'ざ', 'し':'じ', 'す':'ず', 'せ':'ぜ', 'そ':'ぞ',
    'た':'だ', 'ち':'ぢ', 'つ':'づ', 'て':'で', 'と':'ど',
    'は':'ば', 'ひ':'び', 'ふ':'ぶ', 'へ':'べ', 'ほ':'ぼ',
    'カ':'ガ', 'キ':'ギ', 'ク':'グ', 'ケ':'ゲ', 'コ':'ゴ',
    'サ':'ザ', 'シ':'ジ', 'ス':'ズ', 'セ':'ゼ', 'ソ':'ゾ',
    'タ':'ダ', 'チ':'ヂ', 'ツ':'ヅ', 'テ':'デ', 'ト':'ド',
    'ハ':'バ', 'ヒ':'ビ', 'フ':'ブ', 'ヘ':'ベ', 'ホ':'ボ',
}
handakuon = {
    'は':'ぱ', 'ひ':'ぴ', 'ふ':'ぷ', 'へ':'ぺ', 'ほ':'ぽ',
    'ハ':'パ', 'ヒ':'ピ', 'フ':'プ', 'ヘ':'ペ', 'ホ':'ポ',
}
def wordinput(word=8, message=' >'):
    s = ''
    dlist = qgetlist()
    a = 96
    b = 120
    mode = 0
    x = 0
    y = 0
    while True:
        qsendlist(dlist)
        # word selection
        qdraw(frame, (24, b, 26, 12, True))
        for i, t in enumerate(selectword[mode]):
            qdraw(text, (40, 8+b+16*i, t))
        qdraw(draw, (40+x*8, 16+b+y*16, 8, 0))

        # command input
        qdraw(frame, (24, a, 26, 3, True))
        qdraw(text, (32, a+8, f"{message}{s}\x18"))

        key = getkey(['up', 'down', 'right', 'left', 'select', 'a', 'b', 'start'])
        if key == 'a':
            c = selectword[mode][y][x]
            if c == '決':
                if len(s) > 0 and len(s) <= word:
                    return s
            elif c != ' ':
                if c == '゛' and len(s) > 0 and s[-1] in dakuon:
                    s = s[:-1] + dakuon[s[-1]]
                elif c == '゜' and len(s) > 0 and s[-1] in handakuon:
                    s = s[:-1] + handakuon[s[-1]]
                elif len(s) < word:
                    s += c
        elif key == 'b':
            if len(s) > 0:
                s = s[:-1]
        elif key == 'select':
            mode += 1
            if mode == 4: mode = 0
        elif key == 'start':
            if len(s) > 0:
                return s
        elif key == 'up':
            y -= 1
            if y < 0: y = 4
        elif key == 'down':
            y += 1
            if y > 4: y = 0
        elif key == 'right':
            x += 2
            if x > 22: x = 0
        elif key == 'left':
            x -= 2
            if x < 0: x = 22
