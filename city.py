import gfx

import rule
import dungeon
import camp
import item
import spell

import os

import storage

# Center of Town
def town():
    while True:
        gfx.qdraw(gfx.frame, (8,24, (256-16)//8, (192+16)//8))
        gfx.qdraw(gfx.frame, (80, 64, 13, 7, True))
        gfx.qdraw(gfx.text, (96, 72, "ギルガメッシュの酒場"))
        gfx.qdraw(gfx.text, (96, 80, "冒険者の宿"))
        gfx.qdraw(gfx.text, (96, 88, "ボルタック商店"))
        gfx.qdraw(gfx.text, (96, 96, "カント寺院"))
        gfx.qdraw(gfx.text, (96, 104, "町外れ"))

        gfx.qdraw(gfx.frame, (104-16, 16, 11, 3, True))
        gfx.qdraw(gfx.text, (112-16, 24, "Llylgamyn"))
        rule.group.draw()
        blist = gfx.qgetlist()

        key, pos = gfx.select(88, 72, 5)

        if key == 'b' or (key == 'a' and pos == 4):
            return (edgeoftown, [])
        if key == 'a':
            if pos == 0:
                return (tavern, [])
            elif pos == 3:
                for c in rule.roster.member:
                    if rule.status.isneedtemple(c):
                        return(temple, [])
                gfx.qsendlist(blist)
                gfx.simplemessage(f"寺院を必要としている人はいません")
                gfx.getkey(['a', 'b'], 4)
            if pos == 1:
                if len(rule.group.member) > 0:
                    return (inn, [])
                else:
                    gfx.qsendlist(blist)
                    gfx.simplemessage(f"グループに誰もいません")
                    gfx.getkey(['a', 'b'], 4)
            elif pos == 2:
                if len(rule.group.member) > 0:
                    return(tradingpost, [])
                else:
                    gfx.qsendlist(blist)
                    gfx.simplemessage(f"グループに誰もいません")
                    gfx.getkey(['a', 'b'], 4)

# Edge of Town
def edgeoftown():
    gfx.qdraw(gfx.frame, (8,24, (256-16)//8, (192+16)//8))
    gfx.qdraw(gfx.frame, (88, 64, 11, 7, True))
    gfx.qdraw(gfx.text, (104, 72, "訓練場へ行く"))
    gfx.qdraw(gfx.text, (104, 80, "迷宮に入る"))
    gfx.qdraw(gfx.text, (104, 88, "冒険を再開する"))
    gfx.qdraw(gfx.text, (104, 96, "ゲームを中断する"))
    gfx.qdraw(gfx.text, (104, 104, "城に戻る"))

    gfx.qdraw(gfx.frame, (104, 16, 6, 3, True))
    gfx.qdraw(gfx.text, (112, 24, "町はずれ"))
    rule.group.draw()
    dlist = gfx.qgetlist()

    key, pos = gfx.select(96, 72, 5)

    if key == 'b' or (key == 'a' and pos == 4):
        return (town, [])
    if key == 'a':
        if pos == 0:
            # training grounds
            for _ in range(len(rule.group.member)):
                c = rule.group.member[0]
                rule.group.remove(0)
                rule.roster.add(c)
            return (training, [])
        elif pos == 2:
            # restart party out of town
            if len(rule.ootown.getlist()) == 0:
                gfx.qsendlist(dlist)
                gfx.simplemessage('迷宮に潜っているグループはありません')
                gfx.getkey(['a', 'b'])
                return (edgeoftown, [])
            else:
                return (restart, [])
        elif pos == 3:
            # save
            for _ in range(len(rule.group.member)):
                c = rule.group.member[0]
                rule.group.remove(0)
                rule.roster.add(c)
            return (save, [])
        elif pos == 1:
            # enter maze
            if len(rule.group.member) > 0:
                gfx.qsendlist(dlist)
                if rule.group.iswiped():
                    gfx.simplemessage('行動可能なメンバーがいません')
                    gfx.getkey(['a', 'b'])
                    return (edgeoftown, [])
                rule.group.selectitem(town=True, mode='prepare')

                dungeon.dungeon.openning()
                dungeon.dungeon.loadfloor()
                
                return (camp.camp, [])
            else:
                gfx.qsendlist(dlist)
                gfx.simplemessage('グループが編成されていません')
                gfx.getkey(['a', 'b'])
                return (edgeoftown, [])


# Gilgamesh's Tavern
def tavern():
    if len(rule.group.member) > 0:
        gfx.qdraw(gfx.frame, (8,24, (256-16)//8, (192+16)//8))
        gfx.qdraw(gfx.frame, (80, 64, 13, 11, True))
        gfx.qdraw(gfx.text, (96, 72, "ギルガメッシュの酒場"))
        gfx.qdraw(gfx.text, (96, 88, "仲間に入れる"))
        gfx.qdraw(gfx.text, (96, 96, "仲間からはずす"))
        gfx.qdraw(gfx.text, (96, 104, "メンバーを見る"))
        gfx.qdraw(gfx.text, (96, 112, "倉庫を見る"))
        gfx.qdraw(gfx.text, (96, 120, "再編成"))
        gfx.qdraw(gfx.text, (96, 128, "装備を整える"))
        gfx.qdraw(gfx.text, (96, 136, "外へ出る"))

        gfx.qdraw(gfx.frame, (104-16, 16, 11, 3, True))
        gfx.qdraw(gfx.text, (112-16, 24, "Llylgamyn"))
        rule.group.draw()
        dlist = gfx.qgetlist()

        key, pos = gfx.select(88, 88, 7)

        if key == 'b' or (key == 'a' and pos == 6):
            return (town, [])
        if key == 'a':
            if pos == 0:
                if len(rule.group.member) == 6:
                    gfx.qsendlist(dlist)
                    gfx.simplemessage("パーティはいっぱいです")
                    gfx.getkey(['a', 'b'], 4)
                elif len(rule.roster.member) > 0:
                    while True:
                        key, mpos = rule.roster.select()
                        if key == 'a':
                            c = rule.roster.member[mpos]
                            rule.roster.remove(mpos)
                            rule.group.add(c)
                        if key == 'b' or len(rule.group.member) == 6:
                            break
                else:
                    gfx.qsendlist(dlist)
                    gfx.simplemessage("酒場には誰もいません。")
                    gfx.getkey(['a', 'b'], 4)
            elif pos == 1:
                if len(rule.group.member) == 1:
                    mpos = 0
                else:
                    gfx.qsendlist(dlist)
                    gfx.qdraw(gfx.frame, (80, 96, 12, 3, True))
                    gfx.log([" ", " ", f"{'誰をはずしますか?':^16}"])
                    key, mpos = rule.group.select()
                    if key == 'b':
                        return (tavern, [])
                c = rule.group.member[mpos]
                rule.group.remove(mpos)
                rule.roster.add(c)
            elif pos == 2:
                # inspect character
                while True:
                    if len(rule.group.member) == 1:
                        pos = 0
                    else:
                        gfx.qsendlist(dlist)
                        gfx.qdraw(gfx.frame, (80, 96, 12, 3, True))
                        gfx.log([" ", " ", f"{'誰を調べますか?':^16}"])
                        key, pos = rule.group.select()
                        if key == 'b':
                            return(tavern, [])
                    while True:
                        rkey = rule.character.sheet(rule.group.member[pos])
                        if rkey == 'b':
                            return(tavern, [])
                        elif rkey == 'right':
                            pos += 1
                            if pos == len(rule.group.member): pos = 0
                        elif rkey == 'left':
                            pos -= 1
                            if pos < 0:
                                pos = len(rule.group.member)-1
            elif pos == 3:
                gfx.qsendlist(dlist)
                rule.group.selectitem(town=True)
            elif pos == 4:
                # reorder
                pos = 0
                neworder = []
                while pos+1 < len(rule.group.member):
                    gfx.qsendlist(dlist)
                    ftxt = []
                    for i in range(pos):
                        ftxt.append(f" {i+1} {neworder[i]['name']}") 
                    gfx.log([f"{'再編成':^16}", " "] + ftxt)
                    key, cpos = rule.group.select()
                    if key == 'b':
                        if cpos == 0:
                            return(camp, [])
                        else:
                            pos -= 1
                            neworder.pop(-1)
                    if key == 'a':
                        if rule.group.member[cpos] not in neworder:
                            neworder.append(rule.group.member[cpos])
                            pos += 1
                for c in rule.group.member:
                    if c not in neworder:
                        neworder.append(c)
                        break
                rule.group.member = neworder
            elif pos == 5:
                for c in rule.group.member:
                    for slot in rule.character.EQUIPMENT:
                        gfx.qsendlist(dlist)
                        if rule.group.equipitem(c, slot, town=True):
                            return (tavern, [])
    else:
        gfx.qdraw(gfx.frame, (8,24, (256-16)//8, (192+16)//8))
        gfx.qdraw(gfx.frame, (80, 64, 13, 7, True))
        gfx.qdraw(gfx.text, (96, 72, "ギルガメッシュの酒場"))
        gfx.qdraw(gfx.text, (96, 88, "仲間に入れる"))
        gfx.qdraw(gfx.text, (96, 96, "倉庫を見る"))
        gfx.qdraw(gfx.text, (96, 104, "外へ出る"))

        gfx.qdraw(gfx.frame, (104-16, 16, 11, 3, True))
        gfx.qdraw(gfx.text, (112-16, 24, "Llylgamyn"))
        rule.group.draw()
        dlist = gfx.qgetlist()

        key, pos = gfx.select(88, 88, 3)

        if key == 'b' or (key == 'a' and pos == 2):
            return (town, [])
        if key == 'a':
            if pos == 0:
                if len(rule.roster.member) > 0:
                    while True:
                        key, mpos = rule.roster.select()
                        if key == 'a':
                            c = rule.roster.member[mpos]
                            rule.roster.remove(mpos)
                            rule.group.add(c)
                            if len(rule.roster.member) == 0:
                                break
                        if key == 'b' or len(rule.group.member) == 6:
                            break
                else:
                    gfx.qsendlist(dlist)
                    gfx.simplemessage("酒場には誰もいません。")
                    gfx.getkey(['a', 'b'], 4)
            if pos == 1:
                gfx.qsendlist(dlist)
                rule.group.selectitem(town=True)

    return (tavern, [])

# Advanturer's Inn
def inn(c=None):
    # check in
    gfx.qdraw(gfx.frame, (8,24, (256-16)//8, (192+16)//8))
    gfx.qdraw(gfx.frame, (104-16, 16, 11, 3, True))
    gfx.qdraw(gfx.text, (112-16, 24, "Llylgamyn"))
    rule.group.draw()
    dlist = gfx.qgetlist()

    if c is None:
        gfx.qdraw(gfx.frame, (80, 96, 12, 3, True))
        gfx.log([" ", " ", f"{'冒険者の宿':^16}", f"{'誰が泊まりますか?':^16}","", f"{'[B]で抜けます':^16}"])
        blist = gfx.qgetlist()
        key, mpos = rule.group.select()
        if key == 'a':
            c = rule.group.member[mpos]
            if rule.status.istreatasdead(c):
                gfx.qsendlist(blist)
                gfx.simplemessage(f"{c['name']}は行動できません！")
                gfx.getkey(['a', 'b'], 4)
                return (inn, [])
        elif key == 'b':
            return (town, [])
    
    # select room
    roomprice = [0, 10, 50, 200, 500]
    roomheal = [0, 1, 3, 7, 10]
    gfx.qsendlist(dlist)
    basepos = 48
    gfx.qdraw(gfx.frame, (0, basepos, 32, 14, True))
    gfx.qdraw(gfx.text, (16, basepos+8, f"いらっしゃい、{c['name']}さん。どこに泊まりますか?"))
    gfx.qdraw(gfx.text, (16, basepos+24, f"馬小屋             (無料!)"))
    gfx.qdraw(gfx.text, (16, basepos+32, f"簡易寝台             {roomprice[1]:>3} GP/一週間"))
    gfx.qdraw(gfx.text, (16, basepos+40, f"エコノミールーム         {roomprice[2]:>3} GP/一週間"))
    gfx.qdraw(gfx.text, (16, basepos+48, f"スイートルーム          {roomprice[3]:>3} GP/一週間"))
    gfx.qdraw(gfx.text, (16, basepos+56, f"ロイヤルスイート         {roomprice[4]:>3} GP/一週間"))
    gfx.qdraw(gfx.text, (16, basepos+64, f"前に戻る"))
    gfx.qdraw(gfx.text, (16, basepos+80, f"あなたのグループは {rule.group.totalgold} G.P. 持っています"))

    key, mpos = gfx.select(8, basepos+24, 6, 0)
    if key == 'b' or (key == 'a' and mpos == 5):
        return (inn, [])
    elif key == 'a':
        # enjoy the stay
        birthday = False
        if mpos != 0:
            stay = 0
            while rule.group.totalgold >= roomprice[mpos] and c['hp'] < c['hpmax']:
                rule.group.totalgold -= roomprice[mpos]
                c['hp'] += roomheal[mpos]
                if c['hp'] > c['hpmax']: c['hp'] = c['hpmax']
                stay += 1

                gfx.qsendlist(dlist)
                gfx.qdraw(gfx.frame, (0, basepos, 32, 14, True))
                gfx.qdraw(gfx.text, (16, basepos+8, f"{c['name']+'の体力は、回復していく..':^30}"))
                gfx.qdraw(gfx.text, (16, basepos+24, f"         滞在期間 {stay:>12}週"))
                gfx.qdraw(gfx.text, (16, basepos+32, f"   HIT POINTS ({c['hp']:>5}/{c['hpmax']:>5})"))
                gfx.qdraw(gfx.text, (16, basepos+48, f"         GOLD {rule.group.totalgold:>13}"))
                gfx.qdraw(gfx.text, (16, basepos+64, f"{'[B]で抜けます':>27}"))
                key = gfx.getkey()
                if key == 'b' or c['hp'] == c['hpmax']:
                    break
            if c['age']%rule.WEEKPERYEAR + stay > rule.WEEKPERYEAR:
                # happy birthday!
                birthday = True
            c['age'] += stay
        else:
            gfx.qsendlist(dlist)
            gfx.qdraw(gfx.frame, (0, basepos, 32, 14, True))
            gfx.qdraw(gfx.text, (16, basepos+16, f"{c['name']+'は寝ています。':^30}"))
            key = gfx.getkey()

        # level up check
        txt = rule.levelup(c)
        if birthday:
            txt = [f"ハッピーバースデー!({(c['age']//rule.WEEKPERYEAR)-1}→{c['age']//rule.WEEKPERYEAR})"] + txt
        if c['vit'] < 1:
            txt.append(f"あなたは衰弱し,死にました!")
            c['lost'] = True
        elif c['vit'] <= 2 and c['age'] >= 50*rule.WEEKPERYEAR:
            txt.append(f"あなたは老衰で死にました!")
            c['lost'] = True
        gfx.qsendlist(dlist)
        gfx.qdraw(gfx.frame, (0, basepos, 32, 14, True))
        for i, t in enumerate(txt):
            gfx.qdraw(gfx.text, (16, basepos+8+8*i, t))
        key = gfx.getkey()
        if 'lost' in c:
            return(inn, [])
        return (inn, [c])

# Boltac's Trading Post
def tradingpost(c=None):
    # check in
    gfx.qdraw(gfx.frame, (8,24, (256-16)//8, (192+16)//8))
    gfx.qdraw(gfx.frame, (104-16, 16, 11, 3, True))
    gfx.qdraw(gfx.text, (112-16, 24, "Llylgamyn"))
    rule.group.draw()
    dlist = gfx.qgetlist()

    if c is None:
        #gfx.qdraw(gfx.frame, (80, 96, 12, 3, True))
        gfx.log([" ", " ", f"{'ボルタック商店':^16}", f"{'誰が入りますか?':^16}","", f"{'[B]で抜けます':^16}"])
        blist = gfx.qgetlist()
        key, mpos = rule.group.select()
        if key == 'a':
            c = rule.group.member[mpos]
            if rule.status.istreatasdead(c):
                gfx.qsendlist(blist)
                gfx.simplemessage(f"{c['name']}は行動できません！")
                gfx.getkey(['a', 'b'], 4)
                return (tradingpost, [])
        elif key == 'b':
            return (town, [])

    # select service
    gfx.qsendlist(dlist)
    basepos = 48
    gfx.qdraw(gfx.frame, (0, basepos, 32, 14, True))
    gfx.qdraw(gfx.text, (16, basepos+8, f"いらっしゃい、{c['name']}さん"))
    gfx.qdraw(gfx.text, (16, basepos+16, f"あなたのグループは {rule.group.totalgold} G.P. 持っています"))

    gfx.qdraw(gfx.text, (96, basepos+32, f"買う"))
    gfx.qdraw(gfx.text, (96, basepos+40, f"売る"))
    gfx.qdraw(gfx.text, (96, basepos+48, f"呪いを解く"))
    gfx.qdraw(gfx.text, (96, basepos+56, f"識別する"))
    gfx.qdraw(gfx.text, (96, basepos+64, f"店を出る"))
    blist = gfx.qgetlist()
    key, pos = gfx.select(88, basepos+32, 5)

    if key == 'b' or (key == 'a' and pos == 4):
        return (tradingpost, [])
    if key == 'a':
        if pos == 0:
            # buy item
            gfx.qsendlist(dlist)
            buyitem(c, rule.group.totalgold, item.TOWNSHOP)
            return (tradingpost, [c])
        elif pos == 1:
            # sell item
            gfx.qsendlist(dlist)
            rule.group.selectitem(town=True, mode='sell')
            return (tradingpost, [c])
        elif pos == 2:
            # remove curse
            cursed = False
            for i in c['equipment'].keys():
                if item.getitembyname(c['equipment'][i])['cursed']:
                    cursed = True
                    break
            if not cursed:
                gfx.qsendlist(blist)
                gfx.simplemessage('* あなたは呪われていませんよ。 *')
                gfx.getkey(['a', 'b'])
                return (tradingpost, [c])
            gfx.qsendlist(dlist)
            rule.group.selectitem(town=True, bag=c, mode='cursed')

            return (tradingpost, [c])
        elif pos == 3:
            # identify
            gfx.qsendlist(dlist)
            needidentify = False
            for ii in rule.group.storage:
                if item.isnotidentified(ii) == 1:
                    needidentify = True
                    break
            if not needidentify:
                for ii in rule.group.questitems:
                    if item.isnotidentified(ii) == 1:
                        needidentify = True
                        break
                if not needidentify:
                    gfx.qsendlist(blist)
                    gfx.simplemessage('* 未鑑定品はお持ちでないようです。 *')
                    gfx.getkey(['a', 'b'])
                    return (tradingpost, [c])
            rule.group.selectitem(town=True, mode='identify')
            return (tradingpost, [c])

def buyitem(c, gp, itemlists):
    dlist = gfx.qgetlist()
    pos = 0
    while True:
        itemlist = [i for i in itemlists.keys()]
        gfx.qsendlist(dlist)

        gfx.qdraw(gfx.frame, (0, 256-8*8-8*19, 14, 4, True))
        gfx.qdraw(gfx.text, (8, 256-8*8-8*18, f"どれを買いますか?"))
        gfx.qdraw(gfx.text, (8, 256-8*8-8*17, f"資産:{rule.group.totalgold:>9}"))

        gfx.qdraw(gfx.frame, (112, 256-8*8-8*19, 18, 17, True))
        title = f'商品({pos+1}/{len(itemlist)})'
        gfx.qdraw(gfx.text, (128, 256-8*8-8*18, title))

        for i, itemname in enumerate(itemlist[(pos//10)*10:(pos//10)*10+10]):
            inx = i%10
            #gfx.qdraw(gfx.text, (128, 256-8*8-8*16+inx*8, f"{(pos//10)*10+i+1:>2} {item.getnamebyname(itemname)}"))
            if 'all' in item.getitembyname(itemname)['class'] or c['class'] in item.getitembyname(itemname)['class']:
                canequip = ''
            else:
                canequip = '#'
            gfx.qdraw(gfx.text, (128, 256-8*8-8*16+inx*8, f"{canequip}{item.getnamebyname(itemname)}"))
            gfx.qdraw(gfx.text, (128+8*8, 256-8*8-8*16+inx*8, f"{item.getitembyname(itemname)['cost'][0]:>7}"))
        gfx.qdraw(gfx.draw, (120, 256-8*8-8*16+(pos%10)*8, 10, 0), True)

        gfx.qdraw(gfx.text, (128, 256-8*8-32, "[左][右] ページ切り替え"))
        gfx.qdraw(gfx.text, (128, 256-8*8-40, "[A] 購入 [B] 戻る"))

        gfx.qdraw(gfx.frame, (0, 64, 14, 14,True))
        gfx.qdraw(gfx.show, (item.getitembyname(itemlist[pos])['image'][item.isnotidentified(itemlist[pos])], 2, 64+2))

        item.draw(itemlist[pos])
        blist = gfx.qgetlist()
        key = gfx.getkey(['a', 'b', 'up', 'down', 'right', 'left'])
        if key == 'right':
            if pos == len(itemlist) -1:
                pos = 0
            else:
                pos += 10
                if pos >= len(itemlist):
                    pos = len(itemlist) -1
        elif key == 'left':
            pos -= 10
            if pos < 0:
                pos = len(itemlist) -1
        elif key == 'up':
            pos -= 1
            if pos < 0:
                pos = len(itemlist)-1
        elif key == 'down':
            pos += 1
            if pos == len(itemlist):
                pos = 0
        elif key == 'a':
            gfx.qsendlist(blist)
            if rule.group.totalgold < item.getitembyname(itemlist[pos])['cost'][0]:
                gfx.simplemessage('* あなたの所持金では、買えませんよ *')
                gfx.getkey(['a', 'b'])
            else:
                gfx.simplemessage('* きっと、お気に召しますよ *')
                gfx.getkey(['a', 'b'])
                rule.group.totalgold -= item.getitembyname(itemlist[pos])['cost'][0]
                if 'consumable' in item.getitembyname(itemlist[pos])['slot']:
                    rule.group.consumables.append(itemlist[pos])
                else:
                    rule.group.storage.append(itemlist[pos])
                if itemlists[itemlist[pos]] != -1:
                    itemlists[itemlist[pos]] -= 1
                    if itemlists[itemlist[pos]] == 0:
                        itemlists.pop(itemlist[pos])
        elif key == 'b':
            return False


# Temple of Cant
def temple():
    # select patient
    gfx.qdraw(gfx.frame, (8,24, (256-16)//8, (192+16)//8))
    gfx.qdraw(gfx.frame, (104-16, 16, 11, 3, True))
    gfx.qdraw(gfx.text, (112-16, 24, "Llylgamyn"))
    dlist = gfx.qgetlist()

    basepos = 40
    gfx.qdraw(gfx.frame, (0, 40, 32, 26, True))
    gfx.qdraw(gfx.text, (8, basepos+8, f"{'カント寺院へようこそ':^30}"))
    gfx.qdraw(gfx.text, (8, basepos+16, f"{'誰を助けたいのですか?':^30}"))
    gfx.qdraw(gfx.text, (8, 256-24, f"{'[上/下]選択 [A]決定 [B]寺院を去る':>30}"))

    sickguys = []
    for c in rule.roster.member:
        if rule.status.isneedtemple(c):
            sickguys.append(c)
    for i, c in enumerate(sickguys):
        gfx.qdraw(gfx.text, (16, basepos+32+i*8, f"{c['name']:<10}L{c['level']:>3} {rule.ALIGNSTR[c['alignment']]}-{rule.CLASSSTR[c['class']]} {rule.status.statstr(c, 1)}"))

    key, pos = gfx.select(8, basepos+32, len(sickguys))
    if key == 'b':
        return (town, [])
    c = sickguys[pos]

    donationcost = 0
    if 'ashed' in c: donationcost = c['level']*500
    elif 'dead' in c: donationcost = c['level']*250
    elif 'petrified' in c: donationcost = c['level']*200
    elif 'paralyzed' in c: donationcost = c['level']*100

    # donation
    gfx.qsendlist(dlist)
    rule.group.draw()
    gfx.qdraw(gfx.frame, (0, basepos, 32, 14, True))
    gfx.qdraw(gfx.text, (8, basepos+16, f"{'寄付の額は、'+str(donationcost)+'G.P.':^30}"))
    gfx.qdraw(gfx.text, (8, basepos+24, f"{'寄付金を納めますか?(所持金:'+str(rule.group.totalgold)+'G.P.)':^30}"))
    gfx.qdraw(gfx.text, (8, basepos+40, f"{'[A]寄付する [B]納めない':^30}"))
    blist = gfx.qgetlist()
    key = gfx.getkey(['a', 'b'])

    if key == 'a' and rule.group.totalgold >= donationcost:
        rule.group.totalgold -= donationcost

        # murmur
        gfx.qsendlist(dlist)
        rule.group.draw()
        gfx.qdraw(gfx.frame, (0, basepos, 32, 14, True))
        gfx.qdraw(gfx.text, (8, basepos+16, f"  ささやき"))
        gfx.getkey(['a', 'b'], 1)

        # chant
        gfx.qsendlist(dlist)
        rule.group.draw()
        gfx.qdraw(gfx.frame, (0, basepos, 32, 14, True))
        gfx.qdraw(gfx.text, (8, basepos+16, f"  ささやき - えいしょう"))
        gfx.getkey(['a', 'b'], 1)

        # pray
        gfx.qsendlist(dlist)
        rule.group.draw()
        gfx.qdraw(gfx.frame, (0, basepos, 32, 14, True))
        gfx.qdraw(gfx.text, (8, basepos+16, f"  ささやき - えいしょう - いのり"))
        gfx.getkey(['a', 'b'], 1)

        # invoke!
        gfx.qsendlist(dlist)
        rule.group.draw()
        gfx.qdraw(gfx.frame, (0, basepos, 32, 14, True))
        gfx.qdraw(gfx.text, (8, basepos+16, f"  ささやき - えいしょう - いのり - ねんじろ！"))
        gfx.getkey(['a', 'b'], 1)

        # wait miracle happens
        if 'ashed' in c:
            if rule.dice.check(c['vit']*3+40):
                c.pop('ashed')
                if 'dead' in c:
                    c.pop('dead')
                c['hp'] = c['hpmax']
                t = f"{c['name']+'は、元気になりました。':^30}"
                c['age'] += rule.dice.roll((1, 52, 0))
            else:
                c['lost'] = True
                t = f"{c['name']+'は、埋葬されました。':^30}"
        elif 'dead' in c:
            if rule.dice.check(c['vit']*3+50):
                c.pop('dead')
                t = f"{c['name']+'は、元気になりました。':^30}"
                c['age'] += rule.dice.roll((1, 52, 0))
            else:
                c['ashed'] = True
                t = f"{c['name']+'は、今やKADORTOを使わねばならない。':^30}"
        elif 'petrified' in c:
            c.pop('petrified')
            t = f"{c['name']+'は、元気になりました。':^30}"
        elif 'paralyzed' in c:
            c.pop('paralyzed')
            t = f"{c['name']+'は、元気になりました。':^30}"

        gfx.qsendlist(dlist)
        rule.group.draw()
        gfx.qdraw(gfx.frame, (0, basepos, 32, 14, True))
        gfx.qdraw(gfx.text, (8, basepos+16, f"  ささやき - えいしょう - いのり - ねんじろ！"))
        gfx.qdraw(gfx.text, (8, basepos+8*12, t))
        gfx.getkey(['a', 'b'])
        for c in rule.roster.member:
            if rule.status.isneedtemple(c):
                return(temple, [])
        return (town, [])
    else:
        gfx.qsendlist(blist)
        gfx.simplemessage("* ケチな背教者め! 出ていけ! *")
        gfx.getkey(['a', 'b'], 4)
        return (town, [])

# Restart party
def restart():
    clist = rule.ootown.getlist()
    if len(clist) == 0:
        return (town, [])
    key, cpos = rule.selectlist(clist, message='冒険の再開')
    if key == 'b':
        return (town, [])
    elif key == 'a':
        for _ in range(len(rule.group.member)):
            c = rule.group.member[0]
            rule.group.remove(0)
            rule.roster.add(c)
        if rule.ootown.loadfromchar(clist[cpos]):
            return (camp.camp, [])
        return (town, [])

# Save Game
def save():
    gfx.qdraw(gfx.frame, (8,24, (256-16)//8, (192+16)//8))
    gfx.qdraw(gfx.frame, (0, 216, 256//8, 3, True))
    gfx.qdraw(gfx.text, (8, 224, f"{'[A]でWizardryを続けられます。':^30}"))
    for _ in range(len(rule.group.member)):
        c = rule.group.member[0]
        rule.group.remove(0)
        rule.roster.add(c)

    storage.saveall()

    gfx.getkey(['a', 'b'])
    return (town, [])

# Training Grounds
def training():
    gfx.qdraw(gfx.frame, (8,24, (256-16)//8, (192+16)//8))
    gfx.qdraw(gfx.frame, (80, 64, 13, 8, True))
    gfx.qdraw(gfx.text, (96, 72, "キャラクタを作る"))
    gfx.qdraw(gfx.text, (96, 80, "キャラクタを見る"))
    gfx.qdraw(gfx.text, (96, 88, "キャラクタを消す"))
    gfx.qdraw(gfx.text, (96, 96, "名前を変える"))
    gfx.qdraw(gfx.text, (96, 104, "職業を変える"))
    gfx.qdraw(gfx.text, (96, 112, "街に戻る"))

    gfx.qdraw(gfx.frame, (104-16+24, 16, 5, 3, True))
    gfx.qdraw(gfx.text, (112-16+24, 24, "訓練場"))
    dlist = gfx.qgetlist()

    key, pos = gfx.select(88, 72, 6)

    if key == 'b' or (key == 'a' and pos == 5):
        return (town, [])
    if key == 'a':
        if pos == 0:
            # create character
            create()
            return (training, [])
        elif pos == 1:
            # check character(all)
            clist = rule.roster.member.copy()
            clist += rule.ootown.getlist()
            if len(clist) == 0:
                return (training, [])
            key, cpos = rule.selectlist(clist, message='キャラクター名簿')
            if key == 'b':
                return (training, [])
            elif key == 'a':
                rkey = rule.character.sheet(clist[cpos])
                if rkey == 'b':
                    return (training, [])
        elif pos == 2:
            # delete character(all)
            clist = rule.roster.member.copy()
            clist += rule.ootown.getlist()
            if len(clist) == 0:
                return (training, [])
            key, cpos = rule.selectlist(clist, message='キャラクターの消去')
            if key == 'b':
                return (training, [])
            elif key == 'a':
                c = clist[cpos]
                rule.character.draw(c, menu=False)
                gfx.qdraw(gfx.frame, (0, 256-5*8, 32, 5, True))
                gfx.qdraw(gfx.text, (8, 256-5*8+8,  f"{'このキャラクターを抹消しますか?':^30}"))
                gfx.qdraw(gfx.text, (96, 256-5*8+16, "はい"))
                gfx.qdraw(gfx.text, (96, 256-5*8+24, "いいえ"))
                key, pos = gfx.select(88, 256-5*8+16, 2)

                if key == 'b' or key == 'a' and pos == 1:
                    return (training, [])
                if key == 'a' and pos == 0:
                    if c in rule.roster.member:
                        rule.roster.member.remove(c)
                    else:
                        rule.ootown.pickchar(c)
                    rule.character.draw(c, menu=False)
                    gfx.simplemessage(f"登録を抹消しました")
                    gfx.getkey(['a', 'b'], 4)
                    return (training, [])

        elif pos == 3:
            # rename character(all)
            clist = rule.roster.member.copy()
            clist += rule.ootown.getlist()
            if len(clist) == 0:
                return (training, [])
            key, cpos = rule.selectlist(clist, message='名前の変更')
            if key == 'b':
                return (training, [])
            elif key == 'a':
                c = clist[cpos]
                gfx.qdraw(gfx.frame, (8,24, (256-16)//8, (192+16)//8))
                gfx.qdraw(gfx.frame, (0, 40, 32, 3, True))
                gfx.qdraw(gfx.text, (8, 48, f" {c['name']:<10}L{c['level']:>3} {rule.ALIGNSTR[c['alignment']]}-{rule.CLASSSTR[c['class']]} {rule.RACESTR[c['race']]}"))
                gfx.qdraw(gfx.frame, (0, 256-5*8, 32, 5, True))
                gfx.qdraw(gfx.text, (8, 256-5*8+8,  f"[上][下][左][右]: カーソルを動かす"))
                gfx.qdraw(gfx.text, (8, 256-5*8+16, f"[A]: 文字選択 [B]: 一文字削除"))
                gfx.qdraw(gfx.text, (8, 256-5*8+24, f"[SELECT]: ページ切り替え [START]: 決定"))
                dlist = gfx.qgetlist()
                c['name'] = gfx.wordinput(message=' 新しい名前は >')

                gfx.qsendlist(dlist)
                gfx.simplemessage(f"変更しました")
                gfx.getkey(['a', 'b'])

                return (training, [])
        elif pos == 4:
            # change class(in town)
            clist = rule.roster.member.copy()
            while True:
                if len(clist) == 0:
                    return (training, [])
                key, cpos = rule.selectlist(clist, message='名前の変更')
                if key == 'b':
                    return (training, [])
                elif key == 'a':
                    c = clist[cpos]
                    cclass = rule.changableclass(c)
                    if c['class'] in cclass:
                        cclass.remove(c['class'])
                    if len(cclass) != 0:
                        gfx.qdraw(gfx.frame, (8,24, (256-16)//8, (192+16)//8))

                        gfx.qdraw(gfx.frame, (0, 40, 32, 3, True))
                        gfx.qdraw(gfx.text, (8, 48, f" {c['name']:<10}L{c['level']:>3} {rule.ALIGNSTR[c['alignment']]}-{rule.CLASSSTR[c['class']]} {rule.RACESTR[c['race']]}"))

                        gfx.qdraw(gfx.frame, (88, 96, 11, 10, True))
                        for i, cc in enumerate(cclass):
                            gfx.qdraw(gfx.text, (104, 96+8+i*8, rule.CLASSRACESTRJP[cc]))

                        gfx.qdraw(gfx.frame, (0, 256-5*8, 32, 5, True))
                        gfx.qdraw(gfx.text, (8, 256-5*8+16, f"{'職業を選んでください':^30}"))
                        dlist = gfx.qgetlist()
                        key, pos = gfx.select(96, 104, len(cclass))
                        if key == 'a':
                            # changing job
                            c['class'] = cclass[pos]
                            for a in rule.ATTRSTR.keys():
                                c[a] = rule.RACEBASEATTR[c['race']][a]
                            for school in spell.data.keys():
                                for i, sn in enumerate(c[school+'list']):
                                    c[school+'max'][i] = len(sn)
                                    c[school][i] = len(sn)
                            if c['class'] in rule.CLASSINITIALSPELL:
                                for school in spell.data.keys():
                                    if school in rule.CLASSINITIALSPELL[c['class']]:
                                        if c[school][0] == 0:
                                            c[school][0] = rule.CLASSINITIALSPELL[c['class']][school][0]
                                            c[school+'max'][0] = rule.CLASSINITIALSPELL[c['class']][school][0]
                                            c[school+'list'][0] += rule.CLASSINITIALSPELL[c['class']][school][1]
                            c['age'] += rule.dice.roll((1,3,3)) * rule.WEEKPERYEAR + 44
                            rule.refresh(c)

                            gfx.qsendlist(dlist)
                            gfx.simplemessage(f"{rule.CLASSRACESTRJP[c['class']]}へ転職しました。")
                            gfx.getkey(['a', 'b'])
                            return (training, [])


def create():
    gfx.qdraw(gfx.frame, (8,24, (256-16)//8, (192+16)//8))
    gfx.qdraw(gfx.frame, (104-16+24, 16, 5, 3, True))
    gfx.qdraw(gfx.text, (112-16+24, 24, "訓練場"))
    dlist = gfx.qgetlist()

    c = {
        'pc':True,
        'name':'????????',
        'title':None,
        'image':'characters/c001',
        'level':1,
        'alignment':'?',
        'class':'?',
        'race':'?',
        'marks':0,
        'exp':0,
        'age':0,
        'rip':0,
        'ac':0,
        'hp':0,
        'hpmax':0,

        'str':0,
        'iq':0,
        'pie':0,
        'vit':0,
        'agi':0,
        'luc':0,

        'fort':0,
        'reflex':0,
        'will':0,
        'fortref':0,
        'fortwill':0,
        'refwill':0,
        'chroma':0,

        'equipment':{},

        'arcane':[0,0,0,0,0,0,0],
        'arcanemax':[0,0,0,0,0,0,0],
        'arcanelist':[[],[],[],[],[],[],[]],

        'divine':[0,0,0,0,0,0,0],
        'divinemax':[0,0,0,0,0,0,0],
        'divinelist':[[],[],[],[],[],[],[]],
    }

    b = 96

    # set race
    rstr = list(rule.RACESTR.keys())
    rstr = rstr[:-1]
    while True:
        gfx.qsendlist(dlist)
        gfx.qdraw(gfx.frame, (0, 40, 32, 3, True))
        gfx.qdraw(gfx.text, (8, 48, f" {c['name']:<10}L{c['level']:>3} {rule.ALIGNSTR[c['alignment']]}-{rule.CLASSSTR[c['class']]} {rule.RACESTR[c['race']]}"))
        gfx.qdraw(gfx.frame, (88, b, 11, len(rstr)+2, True))
        for i, r in enumerate(rstr):
            gfx.qdraw(gfx.text, (104, b+8+i*8, rule.CLASSRACESTRJP[r]))
        gfx.qdraw(gfx.frame, (0, 256-5*8, 32, 5, True))
        gfx.qdraw(gfx.text, (8, 256-5*8+16,  f"{'種族を選んでください':^30}"))
        key, pos = gfx.select(96, b+8, len(rstr))
        if key == 'a':
            c['race'] = rstr[pos]
            # initial age calulation is based on NES version. apple version will be 18*52+0-299 weeks
            c['age'] = rule.dice.roll((1, 3, 13)) * rule.WEEKPERYEAR
            break

    # set alignment
    astr = list(rule.ALIGNSTR.keys())
    astr = astr[:-1]
    while True:
        gfx.qsendlist(dlist)
        gfx.qdraw(gfx.frame, (0, 40, 32, 3, True))
        gfx.qdraw(gfx.text, (8, 48, f" {c['name']:<10}L{c['level']:>3} {rule.ALIGNSTR[c['alignment']]}-{rule.CLASSSTR[c['class']]} {rule.RACESTR[c['race']]}"))
        gfx.qdraw(gfx.frame, (88, b, 11, len(astr)+2, True))
        for i, r in enumerate(astr):
            gfx.qdraw(gfx.text, (104, b+8+i*8, rule.ALIGNSTRJP[r]))
        gfx.qdraw(gfx.frame, (0, 256-5*8, 32, 5, True))
        gfx.qdraw(gfx.text, (8, 256-5*8+16,  f"{'性格を選んでください':^30}"))
        key, pos = gfx.select(96, b+8, len(astr))
        if key == 'a':
            c['alignment'] = astr[pos]
            break

    # set attribute
    def generatebonus():
        for a in rule.ATTRSTR.keys():
            c[a] = rule.RACEBASEATTR[c['race']][a]
        # following apple style bonus calc
        b = rule.dice.roll((1, 4, 6))
        if rule.dice.check(9): b += 10
        if b < 20 and rule.dice.check(9): b += 10
        return b

    bonus = generatebonus()
    cclass = []
    atstr = list(rule.ATTRSTR.keys())
    pos = 0
    blist = None
    while True:
        gfx.qsendlist(dlist)
        gfx.qdraw(gfx.frame, (0, 40, 32, 3, True))
        gfx.qdraw(gfx.text, (8, 48, f" {c['name']:<10}L{c['level']:>3} {rule.ALIGNSTR[c['alignment']]}-{rule.CLASSSTR[c['class']]} {rule.RACESTR[c['race']]}"))

        # attribute
        gfx.qdraw(gfx.frame, (40, b, 11, 10, True))
        for i, r in enumerate(atstr):
            gfx.qdraw(gfx.text, (56, b+8+i*8, f"{rule.ATTRSTR[r]:>4}  {c[r]:>2}"))
        gfx.qdraw(gfx.text, (56, b+64, f"{'ボーナス':>4}  {bonus:>2}"))

        # class
        gfx.qdraw(gfx.frame, (40+88, b, 11, 10, True))
        for i, cc in enumerate(cclass):
            gfx.qdraw(gfx.text, (128+16, b+8+i*8, rule.CLASSRACESTRJP[cc]))

        blist = gfx.qgetlist()
        # tips
        gfx.qdraw(gfx.frame, (0, 256-5*8, 32, 5, True))
        gfx.qdraw(gfx.text, (8, 256-5*8+8,  f"[上][下]: カーソルを動かす"))
        gfx.qdraw(gfx.text, (8, 256-5*8+16, f"[左][右]: 特性のポイントを変える"))
        gfx.qdraw(gfx.text, (8, 256-5*8+24, f"[SELECT]: ボーナス振り直し [A]: 決定"))

        key, pos = gfx.select(88, b+8, len(rule.ATTRSTR), pos=pos, outkey=['left', 'right', 'select'])
        if key =='right':
            if bonus > 0 and c[atstr[pos]] < 18:
                bonus -= 1
                c[atstr[pos]] += 1
                cclass = rule.changableclass(c)
        elif key =='left':
            if c[atstr[pos]] > rule.RACEBASEATTR[c['race']][atstr[pos]]:
                bonus += 1
                c[atstr[pos]] -= 1
                cclass = rule.changableclass(c)
        elif key == 'select':
            bonus = generatebonus()
            cclass = []
        elif key == 'a':
            if bonus == 0 and len(cclass) > 0:
                break

    # select class
    while True:
        gfx.qsendlist(blist)
        gfx.qdraw(gfx.frame, (0, 256-5*8, 32, 5, True))
        gfx.qdraw(gfx.text, (8, 256-5*8+16, f"{'職業を選んでください':^30}"))
        key, pos = gfx.select(136, b+8, len(cclass))
        if key == 'a':
            c['class'] = cclass[pos]
            # hp
            # following apple style but not actually same
            h = rule.HPROLL[c['class']] + c['vit']//3
            if rule.dice.check(50): h = h*9//10
            if rule.dice.check(50): h = h*9//10
            c['hp'] = h
            c['hpmax'] = h
            # spell
            if c['class'] in rule.CLASSINITIALSPELL:
                for school in spell.data.keys():
                    if school in rule.CLASSINITIALSPELL[c['class']]:
                        c[school][0] = rule.CLASSINITIALSPELL[c['class']][school][0]
                        c[school+'max'][0] = rule.CLASSINITIALSPELL[c['class']][school][0]
                        c[school+'list'][0] += rule.CLASSINITIALSPELL[c['class']][school][1]
            break
    
    # select portrait
    tfiles = os.listdir('image/characters')
    files = []
    for f in tfiles:
        if '.pyxres' in f:
            files.append(f[:-7])
    fpos = 0
    while True:
        gfx.qsendlist(dlist)
        gfx.qdraw(gfx.frame, (0, 40, 32, 3, True))
        gfx.qdraw(gfx.text, (8, 48, f" {c['name']:<10}L{c['level']:>3} {rule.ALIGNSTR[c['alignment']]}-{rule.CLASSSTR[c['class']]} {rule.RACESTR[c['race']]}"))

        gfx.qdraw(gfx.text, (8, 256-6*8, f"{files[fpos]:>29}"))
        gfx.qdraw(gfx.show, (f'characters/{files[fpos]}', 88, b))
        gfx.qdraw(gfx.frame, (0, 256-5*8, 32, 5, True))
        gfx.qdraw(gfx.text, (8, 256-5*8+8,  f"{'ポートレートを選んでください':^30}"))
        gfx.qdraw(gfx.text, (8, 256-5*8+16, f"[左][右]: 次画像へ変える"))
        gfx.qdraw(gfx.text, (8, 256-5*8+24, f"[A]: 決定"))
        key = gfx.getkey(['a', 'right', 'left'])
        if key == 'a':
            c['image'] = f'characters/{files[fpos]}'
            break
        elif key == 'right':
            fpos += 1
            if fpos == len(files): fpos = 0
        elif key == 'left':
            fpos -= 1
            if fpos < 0: fpos = len(files)-1

    # set name
    gfx.qsendlist(dlist)
    gfx.qdraw(gfx.frame, (0, 40, 32, 3, True))
    gfx.qdraw(gfx.text, (8, 48, f" {c['name']:<10}L{c['level']:>3} {rule.ALIGNSTR[c['alignment']]}-{rule.CLASSSTR[c['class']]} {rule.RACESTR[c['race']]}"))
    gfx.qdraw(gfx.frame, (0, 256-5*8, 32, 5, True))
    gfx.qdraw(gfx.text, (8, 256-5*8+8,  f"[上][下][左][右]: カーソルを動かす"))
    gfx.qdraw(gfx.text, (8, 256-5*8+16, f"[A]: 文字選択 [B]: 一文字削除"))
    gfx.qdraw(gfx.text, (8, 256-5*8+24, f"[SELECT]: ページ切り替え [START]: 決定"))
    c['name'] = gfx.wordinput(message=' 新しい名前は >')

    # final confirmation
    rule.refresh(c)
    while True:
        rule.character.draw(c, menu=False)
        gfx.qdraw(gfx.frame, (0, 256-5*8, 32, 5, True))
        gfx.qdraw(gfx.text, (8, 256-5*8+8,  f"{'このキャラクターを使いますか?':^30}"))
        gfx.qdraw(gfx.text, (96, 256-5*8+16, "はい"))
        gfx.qdraw(gfx.text, (96, 256-5*8+24, "いいえ"))
        key, pos = gfx.select(88, 256-5*8+16, 2)

        if key == 'b' or key == 'a' and pos == 1:
            return False
        if key == 'a' and pos == 0:
            rule.group.totalgold += rule.dice.roll((1, 100, 89))
            rule.roster.add(c)
            storage.saveall()
            return True
