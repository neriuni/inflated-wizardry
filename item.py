import gfx
import rule

MELEESTR = {
    'sword':['を-切り裂いた', 'を-叩き切った', 'を-切りつけた'],
    'dagger':['を-激しく突いた', 'を-突き刺した', 'を-刺した'],
    'mace':['を-叩いた', 'を-叩きつけた', 'を-打った'],
    'hand':['を-蹴った', 'を-殴った'],

    'predetor':['に-飛びかかった', 'を-噛みついた', 'を-引き裂いた', 'に-飛びかかった'],
    'brute':['に-突進した', 'を-叩きつけた', 'に-突き進んだ', 'に-突撃した'],
    'scavenger':['に-のしかかった', 'を-締め付けた', 'を-齧った', 'を-刺した', 'を-飲みこんだ'],

    'fighter':['を-切り裂いた', 'を-叩き切った', 'を-切りつけた', 'を-叩いた', 'を-叩きつけた', 'を-打った'],
    'lord':['を-切り裂いた', 'を-叩き切った', 'を-切りつけた','を-叩いた', 'を-叩きつけた', 'を-打った'],
    'mage':['を-叩いた', 'を-叩きつけた', 'を-打った'],
    'priest':['を-叩いた', 'を-叩きつけた', 'を-打った'],
    'bishop':['を-叩いた', 'を-叩きつけた', 'を-打った'],
    'thief':['を-突いた', 'を-突き刺した', 'を-刺した'],
    'samurai':['を-切り裂いた', 'を-叩き切った', 'を-切りつけた'],
    'ninja':['を-突いた', 'を-突き刺した', 'を-刺した','を-切り裂いた', 'を-切りつけた', 'を-蹴った', 'を-殴った'],
}

RANGESTR = ['C', 'S', 'M', 'L']

TYPESTR = {
    'shield':'盾',
    'weapon':'武器',
    'sword':'刀剣',
    'mace':'鈍器',
    'dagger':'小剣',
    'armor':'防具',
    'quest':'キーアイテム',
    'consumable':'消耗品',
    'scroll':'\xa3',
    'potion':'\xa2',
    'misc':'貴重品',

    'chest':'鎧/服',
    'head':'兜/帽子',
    'arm':'籠手/手袋',
    'leg':'すね当て/靴',
    'trinket':'装飾品'
}

# ?: un-identified
# !: cursed
def getnamebyname(n):
    pos = 0
    notidentified = False
    cursed = False
    for c in n:
        if c == '?':
            notidentified = True
        elif c == '!':
            cursed = True
        else:
            break
        pos += 1
    if notidentified:
        ii = data[n[pos:100]]['unknownjp']
    else:
        ii = data[n[pos:100]]['namejp']
    if cursed:
        ii = '!'+ii
    return ii

def getitembyname(n):
    pos = 0
    for c in n:
        if c == '?':
            pass
        elif c == '!':
            pass
        else:
            break
        pos += 1
    return data[n[pos:100]]

def iscursed(n):
    for c in n:
        if c == '?':
            pass
        elif c == '!':
            return 1
        else:
            return 0
    return 0

def isnotidentified(n):
    for c in n:
        if c == '?':
            return 1
        elif c == '!':
            pass
        else:
            return 0
    return 0

def identify(n):
    if n[0] == '?':
        return n[1:]
    elif n[0] == '!':
        if n[1] == '?':
            return '!'+n[2:]
    return n

def draw(n):
    global TYPESTR
    identified = True
    cursed = False
    for c in n:
        if c == '?':
            identified = False
        elif c == '!':
            cursed = True
        else:
            break
    ii = getitembyname(n)
    gfx.qdraw(gfx.frame, (0,256-80, 32,9,True))

    typetxt = ''

    txt = ' 職業: '
    if 'all' in ii['class']:
        txt += 'すべて-'
    else:
        for k in ii['class']:
            txt += rule.CLASSRACESTRJP[k][0]
        txt += '-'
    txt += ' 種族: '
    if 'all' in ii['race']:
        txt += 'すべて-'
    else:
        for k in ii['class']:
            txt += rule.CLASSRACESTRJP[k][0]
        txt += '-'

    if identified:
        itxt = ''
        if ii['cursed']:
            itxt += '呪われている '
        for k in TYPESTR.keys():
            if k in ii['type'] and TYPESTR[k] not in itxt:
                itxt += TYPESTR[k] + ' '
            if k in ii['slot'] and TYPESTR[k] not in itxt:
                itxt += TYPESTR[k] + ' '

        if 'alignment' in ii:
            txt += ' 属性: '
            for k in ii['alignment']:
                txt += rule.ALIGNSTRJP[k]
            txt += '-'
        
        if 'weapon' in ii['type']:
            txt += f" 攻撃力: {ii['damage'][0]}D{ii['damage'][1]}{'' if ii['damage'][2] == 0 else '+'+str(ii['damage'][2])}- 射程: {RANGESTR[ii['range']]}-"

        # attribute
        if 'ac' in ii and ii['ac'] != 0: txt += f" AC: {ii['ac']}-"
        if 'swing' in ii and ii['swing'] != 0: txt += f" 回数補正: {ii['swing']}-"
        if 'tohit' in ii and ii['tohit'] != 0: txt += f" 命中補正: {ii['tohit']}-"
        if 'arcanepower' in ii and ii['arcanepower'] != 0: txt += f" 魔修正: {ii['arcanepower']}-"
        if 'divinepower' in ii and ii['divinepower'] != 0: txt += f" 僧修正: {ii['divinepower']}-"
        if 'thievery' in ii and ii['thievery'] != 0: txt += f" 盗修正: {ii['thievery']}-"
        if 'speed' in ii and ii['speed'] != 0: txt += f" 速度: {ii['speed']}-"
        if 'protect' in ii:
            txt += f" 保護:"
            for k in ii['protect']:
                txt += f" {rule.CLASSRACESTRJP[k]}"
            txt += '-'
        if 'slay' in ii:
            txt += f" 倍打:"
            for k in ii['slay']:
                txt += f" {rule.CLASSRACESTRJP[k]}"
            txt += '-'
    else:
        itxt = ' *未鑑定*'

    gfx.qdraw(gfx.text, (8, 256-72, f" {getnamebyname(n)}"))
    gfx.qdraw(gfx.text, (8, 256-72, f"{itxt:>30}"))
    ltxt = txt.split('-')
    texts = []
    btxt = ''
    for t in ltxt:
        if len(t) + len(btxt) > 29:
            texts.append(btxt)
            btxt = ''
        btxt += t
    if len(btxt) > 0:
        texts.append(btxt)

    for i, t in enumerate(texts):
        gfx.qdraw(gfx.text, (8, 256-56+i*8, f"{t}"))

    if identified and 'lore' in ii:
        ltxt = ii['lore'].split('-')
        texts = []
        btxt = ''
        for t in ltxt:
            if len(t) + len(btxt) > 29:
                texts.append(btxt)
                btxt = ''
            btxt += t
        if len(btxt) > 0:
            texts.append(btxt)

        for i, t in enumerate(texts):
            gfx.qdraw(gfx.text, (8, 256-32+i*8, f" {t}"))

def init():
    global data
    for k in data.keys():
        i = data[k]
        if 'type' not in i: i['type'] = ['misc']
        if 'slot' not in i: i['slot'] = ['consumable']
        if 'class' not in i: i['class'] = ['all']
        if 'race' not in i: i['race'] = ['all']
        if 'ac' not in i: i['ac'] = 0
        if 'cost' not in i: i['cost'] = (0,0,0)
        if 'cursed' not in i: i['cursed'] = False
        if 'unknown' not in i: i['unknown'] = '?' + i['name']
        if 'unknownjp' not in i: i['unknownjp'] = '?' + i['namejp']
        if 'swing' not in i: i['swing'] = 0
        if 'ac' not in i: i['ac'] = 0
        if 'range' not in i: i['range'] = 0
        if 'damage' not in i: i['damage'] = (0,0,0)
        if 'tohit' not in i: i['tohit'] = 0
        if 'arcanepower' not in i: i['arcanepower'] = 0
        if 'divinepower' not in i: i['divinepower'] = 0
        if 'thievery' not in i: i['thievery'] = 0
        if 'speed' not in i: i['speed'] = 0
        if 'heal' not in i: i['heal'] = 0
        if 'afterbreak' not in i: i['afterbreak'] = 'none'
        if 'break' not in i: i['break'] = 0
        if 'use' not in i: i['use'] = []
        if 'align' not in i: i['align'] = ['all']
        if 'lore' not in i: i['lore'] = ''



TOWNSHOP = {
    'long sword':-1,
    'short sword':-1,
    'anointed mace':-1,
    'anointed flail':-1,
    'staff':-1,
    'dagger':-1,
    'short sword+1':2,
    'staff of mogref':1,
    'staff+2':-1,

    'robes':-1,
    'leather armor':-1,
    'chain mail':-1,
    'breast plate':-1,
    'plate mail':-1,
    'leather armor+1':-1,
    'chain mail+1':-1,
    'plate mail+1':1,
    'breast plate+1':-1,

    'small shield':-1,
    'large shield':-1,
    'shield+1':-1,

    'helm':-1,

    'gloves of copper':-1,

    'potion of dios':-1,
    'potion of latumofis':-1,
    'scroll of katino':1,
    'scroll of badios(b)':-1,
    'scroll of halito':25,
    'potion of sopic':1,
}

data = {
    'broken item':{
        'name':'BROKEN ITEM',
        'namejp':'がらくた',
        'image':['item/statue7','item/statue7'],
    },
    # weapon
    # 刃物の日本語訳表現
    # long sword: 長剣
    # sword: 剣
    # short sword: 短剣
    # dagger: 小剣
    'long sword':{
        'name':'LONG SWORD',
        'namejp':'長剣',
        'unknown':'?SWORD',
        'unknownjp':'?剣',
        'image':['item/sword1','item/sword1'],
        'type':['weapon', 'sword'],
        'slot':['main'],
        'class':['fighter', 'samurai', 'lord', 'ninja'],
        'damage':(1,8,0),
        'cost':(25, 12, 12),
    },
    'short sword':{
        'name':'SHORT SWORD',
        'namejp':'短剣',
        'unknown':'?SWORD',
        'unknownjp':'?剣',
        'image':['item/dagger1','item/dagger1'],
        'type':['weapon', 'sword'],
        'slot':['main'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'thief'],
        'damage':(1,6,0),
        'cost':(15, 7, 7),
    },
    'anointed mace':{
        'name':'ANOINTED MACE',
        'namejp':'清められた鎚矛',
        'unknown':'?STICK',
        'unknownjp':'?棒',
        'image':['item/mace1','item/mace1'],
        'type':['weapon', 'mace'],
        'slot':['main'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'priest', 'bishop'],
        'damage':(2,3,0),
        'cost':(30, 15, 15),
    },
    'anointed flail':{
        'name':'ANOINTED FLAIL',
        'namejp':'清められた竿',
        'unknown':'?STICK',
        'unknownjp':'?棒',
        'image':['item/mace2','item/mace1'],
        'type':['weapon', 'mace'],
        'slot':['main'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'priest'],
        'damage':(1,7,0),
        'cost':(150, 75, 75),
    },
    'staff':{
        'name':'STAFF',
        'namejp':'杖',
        'unknownjp':'?棒',
        'image':['item/staff1','item/staff1'],
        'type':['weapon', 'mace'],
        'slot':['main'],
        'damage':(1,5,0),
        'cost':(10, 5, 5),
    },
    'dagger':{
        'name':'DAGGER',
        'namejp':'小剣',
        'image':['item/dagger2','item/dagger2'],
        'type':['weapon', 'dagger'],
        'slot':['main'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'thief', 'mage'],
        'damage':(1,5,0),
        'cost':(5, 2, 2),
    },
    'long sword+1':{
        'name':'LONG SWORD+1',
        'namejp':'長剣+1',
        'unknown':'?SWORD',
        'unknownjp':'?剣',
        'image':['item/sword2','item/sword1'],
        'type':['weapon', 'sword'],
        'slot':['main'],
        'class':['fighter', 'samurai', 'lord', 'ninja'],
        'swing':1,
        'tohit':1,
        'damage':(1,8,1),
        'cost':(15000, 7500, 7500),
    },
    'short sword+1':{
        'name':'SHORT SWORD+1',
        'namejp':'短剣+1',
        'unknown':'?SWORD',
        'unknownjp':'?剣',
        'image':['item/dagger3','item/dagger1'],
        'type':['weapon', 'sword'],
        'slot':['main'],
        'class':['fighter', 'thief', 'samurai', 'lord', 'ninja'],
        'swing':1,
        'tohit':1,
        'damage':(1,6,1),
        'cost':(15000, 7500, 7500),
    },
    'mace+1':{
        'name':'MACE+1',
        'namejp':'鎚矛+1',
        'unknown':'?STICK',
        'unknownjp':'?棒',
        'image':['item/mace3','item/mace1'],
        'type':['weapon', 'mace'],
        'slot':['main'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'priest', 'bishop'],
        'swing':1,
        'tohit':1,
        'damage':(2,4,1),
        'cost':(15000, 7500, 7500),
    },
    'staff of mogref':{
        'name':'STAFF\xa0MOGREF',
        'namejp':'鉄身の杖',
        'unknown':'?ROD',
        'unknownjp':'?杖',
        'image':['item/staff2','item/staff1'],
        'type':['weapon', 'mace'],
        'slot':['main'],
        'class':['mage', 'bishop'],
        'use':'mogref',
        'break':25,
        'afterbreak':'staff',
        'damage':(1,6,0),
        'cost':(3000, 1500, 1500),
    },
    'long sword-1':{
        'name':'LONG SWORD-1',
        'namejp':'長剣-1',
        'unknown':'?SWORD',
        'unknownjp':'?剣',
        'image':['item/sword3','item/sword1'],
        'type':['weapon', 'sword'],
        'slot':['main'],
        'class':['fighter', 'samurai', 'lord', 'ninja'],
        'damage':(1,8,0),
        'tohit':-1,
        'cost':(1000, 0, 500),
        'cursed':True,
    },
    'short sword-1':{
        'name':'SHORT SWORD-1',
        'namejp':'短剣-1',
        'unknown':'?SWORD',
        'unknownjp':'?剣',
        'image':['item/dagger8','item/dagger1'],
        'type':['weapon', 'sword'],
        'slot':['main'],
        'class':['fighter', 'thief', 'samurai', 'lord', 'ninja'],
        'tohit':-1,
        'damage':(1,6,0),
        'cost':(1000, 0, 500),
        'cursed':True,
    },
    'mace-1':{
        'name':'MACE-1',
        'namejp':'鎚矛-1',
        'unknown':'?STICK',
        'unknownjp':'?杖',
        'image':['item/mace4','item/mace1'],
        'type':['weapon', 'mace'],
        'slot':['main'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'priest', 'bishop'],
        'tohit':-1,
        'damage':(2,3,0),
        'cost':(1000, 0, 500),
        'cursed':True,
    },
    'staff+2':{
        'name':'staff+2',
        'namejp':'杖+2',
        'unknown':'?STICK',
        'unknownjp':'?杖',
        'image':['item/staff3','item/staff1'],
        'type':['weapon', 'mace'],
        'slot':['main'],
        'swing':2,
        'tohit':2,
        'damage':(1,4,2),
        'cost':(2500, 1250, 1250),
    },
    'slayer of dragons':{
        'name':'SLAYER\xa0DRAGONS',
        'namejp':'竜殺し',
        'unknown':'?SWORD',
        'unknownjp':'?剣',
        'image':['item/sword4','item/sword1'],
        'type':['weapon', 'sword'],
        'slot':['main'],
        'class':['fighter', 'samurai', 'lord', 'ninja'],
        'swing':1,
        'tohit':2,
        'damage':(1,10,1),
        'slay':['dragon'],
        'protect':['dragon'],
        'cost':(10000, 5000, 5000),
    },
    'long sword+2':{
        'name':'LONG SWORD+2',
        'namejp':'長剣+2',
        'unknown':'?SWORD',
        'unknownjp':'?剣',
        'image':['item/sword5','item/sword1'],
        'type':['weapon', 'sword'],
        'slot':['main'],
        'class':['fighter', 'samurai', 'lord', 'ninja'],
        'swing':2,
        'tohit':2,
        'damage':(1,10,2),
        'cost':(4000, 2000, 2000),
    },
    'short sword+2':{
        'name':'SHORT SWORD+2',
        'namejp':'短剣+2',
        'unknown':'?SWORD',
        'unknownjp':'?剣',
        'image':['item/dagger5','item/dagger1'],
        'type':['weapon', 'sword'],
        'slot':['main'],
        'class':['fighter', 'thief', 'samurai', 'lord', 'ninja'],
        'swing':2,
        'tohit':2,
        'damage':(1,6,2),
        'cost':(4000, 2000, 2000),
    },
    'mace+2':{
        'name':'MACE+2',
        'namejp':'鎚矛+2',
        'unknown':'?STICK',
        'unknownjp':'?杖',
        'image':['item/mace5','item/mace1'],
        'type':['weapon', 'mace'],
        'slot':['main'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'priest', 'bishop'],
        'swing':2,
        'tohit':2,
        'damage':(1,8,2),
        'cost':(4000, 2000, 2000),
    },
    'were slayer':{
        'name':'WERE SLAYER',
        'namejp':'獣殺し',
        'unknown':'?SWORD',
        'unknownjp':'?剣',
        'image':['item/dagger6','item/sword1'],
        'type':['weapon', 'sword'],
        'slot':['main'],
        'class':['fighter', 'samurai', 'lord', 'ninja'],
        'swing':2,
        'tohit':1,
        'damage':(1,10,1),
        'slay':['beast'],
        'protect':['beast'],
        'cost':(10000, 5000, 5000),
    },
    'mage masher':{
        'name':'MAGE MASHER',
        'namejp':'魔術師つぶし',
        'unknown':'?SWORD',
        'unknownjp':'?剣',
        'image':['item/dagger7','item/dagger1'],
        'type':['weapon', 'sword'],
        'slot':['main'],
        'class':['fighter', 'thief', 'samurai', 'lord', 'ninja'],
        'swing':2,
        'tohit':1,
        'damage':(1,6,1),
        'cost':(10000, 5000, 5000),
        'slay':['mage'],
        'protect':['mage'],
    },
    'mace of poison':{
        'name':'MACE\xa0POISON',
        'namejp':'蛇の鎚矛',
        'unknown':'?STICK',
        'unknownjp':'?杖',
        'image':['item/staff5','item/mace1'],
        'type':['weapon', 'mace'],
        'slot':['main'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'priest', 'bishop'],
        'swing':2,
        'tohit':2,
        'damage':(1,8,0),
        'cost':(10000, 5000, 5000),
        'protect':['poison', 'insect'],
    },
    'staff of montino':{
        'name':'STAFF\xa0MONTINO',
        'namejp':'静寂の杖',
        'unknown':'?STICK',
        'unknownjp':'?杖',
        'image':['item/staff6','item/staff1'],
        'type':['weapon', 'mace'],
        'slot':['main'],
        'use':'montino',
        'break':10,
        'afterbreak':'staff',
        'swing':1,
        'tohit':1,
        'damage':(1,5,1),
        'cost':(15000, 7500, 7500),
    },
    'blade cusinart':{
        'name':"BLADE CUSINART`",
        'namejp':'カシナートの剣',
        'unknown':'?SWORD',
        'unknownjp':'?剣',
        'image':['item/sword6','item/sword1'],
        'type':['weapon', 'sword'],
        'slot':['main'],
        'class':['fighter', 'samurai', 'lord', 'ninja'],
        'swing':4,
        'tohit':4,
        'damage':(1,3,9),
        'cost':(15000, 7500, 7500),
        'lore':'Cuisinart社製フードプロセッサー'
    },
    'short sword-2':{
        'name':'SHORT SWORD-2',
        'namejp':'短剣-2',
        'unknown':'?SWORD',
        'unknownjp':'?剣',
        'image':['item/dagger8','item/dagger1'],
        'type':['weapon', 'sword'],
        'slot':['main'],
        'class':['fighter', 'thief', 'samurai', 'lord', 'ninja'],
        'tohit':1,
        'swing':1,
        'damage':(1,6,0),
        'cost':(8000, 0, 4000),
        'cursed':True,
    },
    'dagger+2':{
        'name':'DAGGER+2',
        'namejp':'小剣+2',
        'unknown':'?DAGGER',
        'unknownjp':'?小剣',
        'image':['item/dagger9','item/dagger2'],
        'type':['weapon', 'dagger'],
        'slot':['main'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'thief', 'mage'],
        'tohit':2,
        'swing':2,
        'damage':(1,4,2),
        'cost':(8000, 4000, 4000),
    },
    'mace-2':{
        'name':'MACE-2',
        'namejp':'鎚矛-2',
        'unknown':'?STICK',
        'unknownjp':'?棒',
        'image':['item/mace6','item/mace1'],
        'type':['weapon', 'mace'],
        'slot':['main'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'priest', 'bishop'],
        'damage':(1,8,0),
        'cost':(8000, 0, 4000),
        'cursed':True,
    },
    'staff-2':{
        'name':'staff-2',
        'namejp':'杖-2',
        'unknown':'?STAFF',
        'unknownjp':'?杖',
        'image':['item/staff8','item/staff1'],
        'type':['weapon', 'mace'],
        'slot':['main'],
        'swing':1,
        'tohit':-2,
        'damage':(1,4,0),
        'cost':(8000, 0, 4000),
    },
    'dagger of speed':{
        'name':'DAGGER\xa0SPEED',
        'namejp':'長剣',
        'unknown':'?DAGGER',
        'unknownjp':'?剣',
        'image':['item/dagger10','item/dagger2'],
        'type':['weapon', 'dagger'],
        'slot':['main'],
        'class':['thief', 'mage', 'ninja'],
        'tohit':-1,
        'swing':7,
        'ac':-3,
        'damage':(1,4,0),
        'cost':(30000, 15000, 15000),
    },
    'sword+3(e)':{
        'name':'SWORD+3(E)',
        'namejp':'長剣',
        'unknown':'?SWORD',
        'unknownjp':'?剣',
        'image':['item/sword7','item/sword1'],
        'type':['weapon', 'sword'],
        'slot':['main'],
        'class':['fighter', 'samurai', 'lord', 'ninja'],
        'swing':4,
        'tohit':7,
        'damage':(1,10,3),
        'cost':(50000, 25000, 25000),
        'alignment':['evil'],
        'acunmatch':-1,
        'cursedunmatch':True,
    },
    'ssword+3(e)':{
        'name':'S-SWORD+3(E)',
        'namejp':'長剣',
        'unknown':'?SWORD',
        'unknownjp':'?剣',
        'image':['item/sword11','item/sword1'],
        'type':['weapon', 'sword'],
        'slot':['main'],
        'class':['fighter', 'thief', 'samurai', 'lord', 'ninja'],
        'swing':4,
        'tohit':7,
        'damage':(1,6,3),
        'cost':(50000, 25000, 25000),
        'alignment':['evil'],
        'acunmatch':-1,
        'cursedunmatch':True,
    },
    'dagger of thieves':{
        'name':'DAGGER\xa0THIEVES',
        'namejp':'盗賊の小剣',
        'unknown':'?SWORD',
        'unknownjp':'?小剣',
        'image':['item/dagger11','item/dagger1'],
        'type':['weapon', 'dagger'],
        'slot':['main'],
        'class':['thief', 'ninja'],
        'swing':1,
        'tohit':1,
        'damage':(1,6,1),
        'cost':(50000, 25000, 25000),
        'use':['classninja'],
        'break':100,
    },
    'murasameblade':{
        'name':'MURAMASA BLADE!',
        'namejp':'村雨!',
        'unknown':'?SWORD',
        'unknownjp':'?刀',
        'image':['item/weapon1','item/sword30'],
        'type':['weapon', 'sword'],
        'slot':['main'],
        'class':['samurai'],
        'damage':(10,5,0),
        'cost':(1000000, 500000, 500000),
        'use':['str1'],
        'break':50,
    },
    'shrikens':{
        'name':'SHRIKENS',
        'namejp':'手裏剣',
        'unknown':'?STARS',
        'unknownjp':'?星型の物体',
        'image':['item/sword9','item/dagger1'],
        'type':['weapon', 'dagger'],
        'slot':['main'],
        'class':['ninja'],
        'damage':(1,5,10),
        'cost':(50000, 25000, 25000),
        'use':['hp1'],
        'break':50,
        'protect':['poison', 'drain'],
    },

    # shield
    'small shield':{
        'name':'SMALL SHIELD',
        'namejp':'小型の盾',
        'unknown':'?SHIELD',
        'unknownjp':'?盾',
        'image':['item/shield1','item/shield1'],
        'type':['shield'],
        'slot':['sub'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'thief', 'priest', 'bishop'],
        'ac':2,
        'cost':(20, 10, 10),
    },
    'large shield':{
        'name':'LARGE SHIELD',
        'namejp':'盾',
        'unknown':'?SHIELD',
        'unknownjp':'?盾',
        'image':['item/shield2','item/shield2'],
        'type':['shield'],
        'slot':['sub'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'priest'],
        'ac':3,
        'cost':(40, 20, 20),
    },
    'shield+1':{
        'name':'SHIELD+1',
        'namejp':'盾+1',
        'unknown':'?SHIELD',
        'unknownjp':'?盾',
        'image':['item/shield3','item/shield2'],
        'type':['shield'],
        'slot':['sub'],
        'class':['fighter', 'thief', 'samurai', 'lord', 'ninja', 'priest'],
        'ac':4,
        'cost':(1500, 0, 750),
    },
    'shield-1':{
        'name':'SHIELD-1',
        'namejp':'盾-1',
        'unknown':'?SHIELD',
        'unknownjp':'?盾',
        'image':['item/shield4','item/shield2'],
        'type':['shield'],
        'slot':['sub'],
        'class':['fighter', 'thief', 'samurai', 'lord', 'priest'],
        'ac':-1,
        'cost':(1500, 0, 750),
        'cursed':True,
    },
    'shield+2':{
        'name':'SHIELD+2',
        'namejp':'盾+2',
        'unknown':'?SHIELD',
        'unknownjp':'?盾',
        'image':['item/shield5','item/shield2'],
        'type':['shield'],
        'slot':['sub'],
        'class':['fighter', 'thief', 'samurai', 'lord', 'ninja', 'priest'],
        'ac':5,
        'cost':(7000, 3500, 3500),
    },
    'evil shield+3':{
        'name':'EVIL SHIELD+3',
        'namejp':'悪の盾+3',
        'unknown':'?SHIELD',
        'unknownjp':'?盾',
        'image':['item/shield6','item/shield2'],
        'type':['shield'],
        'slot':['sub'],
        'class':['fighter', 'thief', 'samurai', 'lord', 'ninja', 'priest'],
        'alignment':['evil'],
        'ac':5,
        'acunmatch':-1,
        'cursedunmatch':True,
        'cost':(25000, 12500, 12500),
        'cursed':False,
    },
    'shield-2':{
        'name':'SHIELD-2',
        'namejp':'盾-2',
        'unknown':'?SHIELD',
        'unknownjp':'?盾',
        'image':['item/shield7','item/shield2'],
        'type':['shield'],
        'slot':['sub'],
        'class':['fighter', 'thief', 'samurai', 'lord', 'priest'],
        'ac':-2,
        'cost':(8000, 0, 4000),
        'cursed':True,
    },
    'shield+3':{
        'name':'SHIELD+3',
        'namejp':'盾+3',
        'unknown':'?SHIELD',
        'unknownjp':'?盾',
        'image':['item/shield8','item/shield2'],
        'type':['shield'],
        'slot':['sub'],
        'class':['fighter', 'thief', 'samurai', 'lord', 'ninja', 'priest'],
        'ac':6,
        'cost':(250000, 125000, 125000),
    },


    # armor
    'robes':{
        'name':'ROBES',
        'namejp':'ローブ',
        'unknown':'?CLOTHING',
        'unknownjp':'?服',
        'image':['item/robe1','item/robe1'],
        'type':['armor'],
        'slot':['chest'],
        'ac':1,
        'cost':(15, 7, 7),
    },
    'leather armor':{
        'name':'LEATHER ARMOR',
        'namejp':'革の鎧',
        'unknown':'?ARMOR',
        'unknownjp':'?鎧',
        'image':['item/leather1','item/leather1'],
        'type':['armor'],
        'slot':['chest'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'priest', 'thief', 'bishop'],
        'ac':2,
        'cost':(50, 25, 25),
    },
    'chain mail':{
        'name':'CHAIN MAIL',
        'namejp':'鎖帷子',
        'unknown':'?ARMOR',
        'unknownjp':'?鎧',
        'image':['item/chain1','item/chain1'],
        'type':['armor'],
        'slot':['chest'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'priest'],
        'ac':3,
        'cost':(90, 45, 45),
    },
    'breast plate':{
        'name':'BREAST PLATE',
        'namejp':'胸当て',
        'unknown':'?ARMOR',
        'unknownjp':'?鎧',
        'image':['item/plate1','item/plate1'],
        'type':['armor'],
        'slot':['chest'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'priest'],
        'ac':4,
        'cost':(200, 100, 100),
    },
    'plate mail':{
        'name':'PLATE MAIL',
        'namejp':'板金鎧',
        'unknown':'?ARMOR',
        'unknownjp':'?鎧',
        'image':['item/full1','item/full1'],
        'type':['armor'],
        'slot':['chest'],
        'class':['fighter', 'samurai', 'lord', 'ninja'],
        'ac':5,
        'cost':(750, 375, 375),
    },
    'leather armor+1':{
        'name':'LEATHER ARMOR+1',
        'namejp':'革の鎧+1',
        'unknown':'?ARMOR',
        'unknownjp':'?鎧',
        'image':['item/leather2','item/leather1'],
        'type':['armor'],
        'slot':['chest'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'priest', 'thief', 'bishop'],
        'ac':3,
        'cost':(1500, 750, 750),
    },
    'chain mail+1':{
        'name':'CHAIN MAIL+1',
        'namejp':'鎖帷子+1',
        'unknown':'?ARMOR',
        'unknownjp':'?鎧',
        'image':['item/chain2','item/chain1'],
        'type':['armor'],
        'slot':['chest'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'priest'],
        'ac':4,
        'cost':(1500, 750, 750),
    },
    'plate mail+1':{
        'name':'PLATE MAIL+1',
        'namejp':'板金鎧+1',
        'unknown':'?ARMOR',
        'unknownjp':'?鎧',
        'image':['item/full2','item/full1'],
        'type':['armor'],
        'slot':['chest'],
        'class':['fighter', 'samurai', 'lord', 'ninja'],
        'ac':6,
        'cost':(1500, 750, 750),
    },
    'breast plate+1':{
        'name':'BREAST PLATE',
        'namejp':'胸当て+1',
        'unknown':'?ARMOR',
        'unknownjp':'?鎧',
        'image':['item/plate2','item/plate1'],
        'type':['armor'],
        'slot':['chest'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'priest'],
        'ac':5,
        'cost':(1500, 750, 750),
    },
    'leather armor-1':{
        'name':'LEATHER ARMOR-1',
        'namejp':'革の鎧-1',
        'unknown':'?ARMOR',
        'unknownjp':'?鎧',
        'image':['item/leather3','item/leather1'],
        'type':['armor'],
        'slot':['chest'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'priest', 'thief', 'bishop'],
        'ac':1,
        'cost':(1500, 0, 750),
        'cursed':True,
    },
    'chain mail-1':{
        'name':'CHAIN MAIL-1',
        'namejp':'鎖帷子-1',
        'unknown':'?ARMOR',
        'unknownjp':'?鎧',
        'image':['item/chain3','item/chain1'],
        'type':['armor'],
        'slot':['chest'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'priest'],
        'ac':2,
        'cost':(1500, 0, 750),
        'cursed':True,
    },
    'breast plate-1':{
        'name':'BREAST PLATE-1',
        'namejp':'胸当て-1',
        'unknown':'?ARMOR',
        'unknownjp':'?鎧',
        'image':['item/plate3','item/plate1'],
        'type':['armor'],
        'slot':['chest'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'priest'],
        'ac':3,
        'cost':(1500, 0, 750),
        'cursed':True,
    },
    'leather armor+2':{
        'name':'LEATHER ARMOR+1',
        'namejp':'革の鎧+1',
        'unknown':'?ARMOR',
        'unknownjp':'?鎧',
        'image':['item/leather4','item/leather1'],
        'type':['armor'],
        'slot':['chest'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'priest', 'thief', 'bishop'],
        'ac':4,
        'cost':(6000, 3000, 3000),
    },
    'chain mail+2':{
        'name':'CHAIN MAIL+2',
        'namejp':'鎖帷子+2',
        'unknown':'?ARMOR',
        'unknownjp':'?鎧',
        'image':['item/chain4','item/chain1'],
        'type':['armor'],
        'slot':['chest'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'priest'],
        'ac':5,
        'cost':(6000, 3000, 3000),
    },
    'plate mail+2':{
        'name':'PLATE MAIL+2',
        'namejp':'板金鎧+2',
        'unknown':'?ARMOR',
        'unknownjp':'?鎧',
        'image':['item/full3','item/full1'],
        'type':['armor'],
        'slot':['chest'],
        'class':['fighter', 'samurai', 'lord', 'ninja'],
        'ac':7,
        'cost':(6000, 3000, 3000),
    },
    'echain mail+2':{
        'name':'CHAIN(E)+2',
        'namejp':'悪の鎖帷子+2',
        'unknown':'?CHAIN',
        'unknownjp':'?鎧',
        'image':['item/chain5','item/chain1'],
        'type':['armor'],
        'slot':['chest'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'priest'],
        'ac':5,
        'acunmatch':-1,
        'cursedunmatch':True,
        'cost':(8000, 4000, 4000),
        'cursed':False,
        'alignment':['evil'],
    },
    'nplate mail+2':{
        'name':'PLATE MAIL+2',
        'namejp':'中立の鎧+2',
        'unknown':'?ARMOR',
        'unknownjp':'?鎧',
        'image':['item/full4','item/full1'],
        'type':['armor'],
        'slot':['chest'],
        'class':['fighter', 'samurai', 'lord', 'ninja'],
        'ac':7,
        'cost':(750, 375, 375),
        'acunmatch':-1,
        'cursedunmatch':True,
        'cost':(8000, 4000, 4000),
        'cursed':False,
        'alignment':['neutral'],
    },
    'robes of curses':{
        'name':'ROBES\xa0CURSES',
        'namejp':'呪いのローブ',
        'unknown':'?CLOTHING',
        'unknownjp':'?服',
        'image':['item/robe2','item/robe1'],
        'type':['armor'],
        'slot':['chest'],
        'ac':-2,
        'tohit':-2,
        'cost':(8000, 0, 4000),
        'cursed':True,
    },
    'leather armor-2':{
        'name':'LEATHER-2',
        'namejp':'革の鎧-2',
        'unknown':'?ARMOR',
        'unknownjp':'?鎧',
        'image':['item/leather5','item/leather1'],
        'type':['armor'],
        'slot':['chest'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'priest', 'thief', 'bishop'],
        'ac':0,
        'cost':(8000, 0, 4000),
        'cursed':True,
    },
    'chain mail-2':{
        'name':'CHAIN-2',
        'namejp':'鎖帷子-2',
        'unknown':'?ARMOR',
        'unknownjp':'?鎧',
        'image':['item/chain6','item/chain1'],
        'type':['armor'],
        'slot':['chest'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'priest'],
        'ac':1,
        'cost':(8000, 0, 4000),
        'cursed':True,
    },
    'breast plate-2':{
        'name':'BREAST PLATE-2',
        'namejp':'胸当て-2',
        'unknown':'?ARMOR',
        'unknownjp':'?鎧',
        'image':['item/plate6','item/plate1'],
        'type':['armor'],
        'slot':['chest'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'priest'],
        'ac':2,
        'cost':(8000, 0, 4000),
        'cursed':True,
    },
    'breast plate+2':{
        'name':'BREAST PLATE+2',
        'namejp':'胸当て+2',
        'unknown':'?ARMOR',
        'unknownjp':'?鎧',
        'image':['item/plate7','item/plate1'],
        'type':['armor'],
        'slot':['chest'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'priest'],
        'ac':6,
        'cost':(10000, 5000, 5000),
    },
    'breast plate+3':{
        'name':'BREAST PLATE+3',
        'namejp':'胸当て+3',
        'unknown':'?ARMOR',
        'unknownjp':'?鎧',
        'image':['item/plate8','item/plate1'],
        'type':['armor'],
        'slot':['chest'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'priest'],
        'ac':7,
        'cost':(100000, 50000, 50000),
    },
    'garb of lords':{
        'name':'GARB\xa0LORDS',
        'namejp':'君主の聖衣',
        'unknown':'?ARMOR',
        'unknownjp':'?鎧',
        'image':['item/plate9','item/plate1'],
        'type':['armor'],
        'slot':['chest'],
        'class':['lord', 'ninja'],
        'slay':['beast', 'undead', 'demon'],
        'proc':['critical'],
        'heal':1,
        'use':['groupheal'],
        'break':50,
        'ac':10,
        'cost':(1000000, 500000, 500000),
    },
    'cold chain mail':{
        'name':'COLD CHAIN MAIL',
        'namejp':'氷の鎖帷子',
        'unknown':'?ARMOR',
        'unknownjp':'?鎧',
        'image':['item/chain7','item/chain1'],
        'type':['armor'],
        'slot':['chest'],
        'class':['fighter', 'samurai', 'lord', 'ninja', 'priest'],
        'ac':6,
        'protect':['fire'],
        'cost':(150000, 75000, 75000),
    },
    'eplate mail+3':{
        'name':'PLATE MAIL+3',
        'namejp':'悪の鎧+3',
        'unknown':'?ARMOR',
        'unknownjp':'?鎧',
        'image':['item/full11','item/full1'],
        'type':['armor'],
        'slot':['chest'],
        'class':['fighter', 'priest', 'samurai', 'lord', 'ninja'],
        'ac':9,
        'cost':(750, 375, 375),
        'acunmatch':-1,
        'cursedunmatch':True,
        'cost':(150000, 75000, 75000),
        'cursed':False,
        'alignment':['evil'],
    },

    # helmet
    'helm':{
        'name':'HELM',
        'namejp':'兜',
        'unknown':'?HELM',
        'unknownjp':'?兜',
        'image':['item/helm1','item/helm1'],
        'type':['armor'],
        'slot':['head'],
        'class':['fighter', 'samurai', 'lord', 'ninja'],
        'ac':1,
        'cost':(100, 50, 50),
    },
    'helm+1':{
        'name':'HELM+1',
        'namejp':'兜+1',
        'unknown':'?HELM',
        'unknownjp':'?兜',
        'image':['item/helm2','item/helm1'],
        'type':['armor'],
        'slot':['head'],
        'class':['fighter', 'samurai', 'lord', 'ninja'],
        'ac':2,
        'cost':(3000, 1500, 1500),
    },
    'ehelm+2':{
        'name':'EVIL HELM+2',
        'namejp':'悪の兜+2',
        'unknown':'?HELM',
        'unknownjp':'?兜',
        'image':['item/helm3','item/helm1'],
        'type':['armor'],
        'slot':['head'],
        'class':['fighter', 'samurai', 'lord', 'ninja'],
        'ac':3,
        'acunmatch':-1,
        'cursedunmatch':True,
        'cost':(8000, 4000, 4000),
        'cursed':False,
        'alignment':['evil'],
        'use':['badios'],
        'break':0,
    },
    'ring of malor':{
        'name':'RING\xa0MALOR',
        'namejp':'幻姿の冠',
        'unknown':'?HELM',
        'unknownjp':'?兜',
        'image':['item/helm4','item/helm1'],
        'type':['armor'],
        'slot':['head'],
        'class':['fighter', 'samurai', 'lord', 'ninja'],
        'ac':2,
        'cost':(25000, 12500, 12500),
        'use':['malor'],
        'break':100,
    },
    'helmet of curses':{
        'name':'HELMET\xa0CURSES',
        'namejp':'呪いの兜',
        'unknown':'?HELM',
        'unknownjp':'?兜',
        'image':['item/helm5','item/helm1'],
        'type':['armor'],
        'slot':['head'],
        'class':['fighter', 'samurai', 'lord', 'ninja'],
        'ac':-2,
        'cost':(50000, 0, 25000),
    },

    # gloves
    'gloves of copper':{
        'name':'GLOVES\xa0COPPER',
        'namejp':'銅の小手',
        'unknown':'?GLOVES',
        'unknownjp':'?手甲',
        'image':['item/glove1','item/glove1'],
        'type':['armor'],
        'slot':['head'],
        'class':['fighter', 'samurai', 'lord', 'ninja'],
        'ac':1,
        'cost':(6000, 3000, 3000),
    },
    'gloves of silver':{
        'name':'GLOVES\xa0SILVER',
        'namejp':'銀の小手',
        'unknown':'?GLOVES',
        'unknownjp':'?手甲',
        'image':['item/glove1','item/glove1'],
        'type':['armor'],
        'slot':['head'],
        'class':['fighter', 'samurai', 'lord', 'ninja'],
        'ac':3,
        'cost':(60000, 30000, 30000),
    },

    # その他のアイテム
    'potion of dios':{
        'name':'\xa2\xa0DIOS',
        'namejp':'\xa2薬石',
        'unknown':'?\xa2',
        'unknownjp':'?\xa2',
        'image':['item/potion1','item/potion1'],
        'type':['potion'],
        'slot':['consumable'],
        'use':'dios',
        'break':100,
        'cost':(500, 250, 250),
    },
    'potion of latumofis':{
        'name':'\xa2\xa0LATUMOFIS',
        'namejp':'\xa2解毒',
        'unknown':'?\xa2',
        'unknownjp':'?\xa2',
        'image':['item/potion1','item/potion1'],
        'type':['potion'],
        'slot':['consumable'],
        'use':'latumofis',
        'break':100,
        'cost':(300, 150, 150),
    },
    'scroll of katino':{
        'name':'\xa3KATINO',
        'namejp':'\xa3眠り',
        'unknown':'?\xa3',
        'unknownjp':'?\xa3',
        'image':['item/scroll1','item/scroll1'],
        'type':['scroll'],
        'slot':['consumable'],
        'use':'katino',
        'break':100,
        'cost':(500, 250, 250),
    },
    'scroll of badios(a)':{
        'name':'\xa3BADIOS',
        'namejp':'\xa3苦痛',
        'unknown':'?\xa3',
        'unknownjp':'?\xa3',
        'image':['item/scroll1','item/scroll1'],
        'type':['scroll'],
        'slot':['consumable'],
        'use':'badios',
        'break':25,
        'cost':(500, 250, 250),
    },
    'scroll of halito':{
        'name':'\xa3HALITO',
        'namejp':'\xa3小炎',
        'unknown':'?\xa3',
        'unknownjp':'?\xa3',
        'image':['item/scroll1','item/scroll1'],
        'type':['scroll'],
        'slot':['consumable'],
        'use':'halito',
        'break':100,
        'cost':(500, 250, 250),
    },
    'amulet of jewels':{
        'name':'AMULET\xa0JEWELS',
        'namejp':'宝石の護符',
        'unknown':'?AMULET',
        'unknownjp':'?護符',
        'image':['item/staff4','item/trinket49'],
        'type':['misc'],
        'slot':['consumable'],
        'use':'dumapic',
        'break':0,
        'cost':(5000, 2500, 2500),
    },
    'scroll of badios(b)':{
        'name':'\xa3BADIOS',
        'namejp':'\xa3苦痛',
        'unknown':'?\xa3',
        'unknownjp':'?\xa3',
        'image':['item/scroll1','item/scroll1'],
        'type':['scroll'],
        'slot':['consumable'],
        'use':'badios',
        'break':100,
        'cost':(500, 250, 250),
    },
    'potion of sopic':{
        'name':'\xa2\xa0SOPIC',
        'namejp':'\xa2隠れ身',
        'unknown':'?\xa2',
        'unknownjp':'?\xa2',
        'image':['item/potion1','item/potion1'],
        'type':['potion'],
        'slot':['consumable'],
        'use':'sopic',
        'break':100,
        'cost':(1500, 750, 750),
    },
    'scroll of lomilwa':{
        'name':'\xa3LOMILWA',
        'namejp':'\xa3増光',
        'unknown':'?\xa3',
        'unknownjp':'?\xa3',
        'image':['item/scroll1','item/scroll1'],
        'type':['scroll'],
        'slot':['consumable'],
        'use':'lomilwa',
        'break':100,
        'cost':(2500, 1250, 1250),
    },
    'scroll of dilto':{
        'name':'\xa3DILTO',
        'namejp':'\xa3暗闇',
        'unknown':'?\xa3',
        'unknownjp':'?\xa3',
        'image':['item/scroll1','item/scroll1'],
        'type':['scroll'],
        'slot':['consumable'],
        'use':'dilto',
        'break':100,
        'cost':(2500, 1250, 1250),
    },
    'potion of dial':{
        'name':'\xa2\xa0DIAL',
        'namejp':'\xa2施療',
        'unknown':'?\xa2',
        'unknownjp':'?\xa2',
        'image':['item/potion1','item/potion1'],
        'type':['potion'],
        'slot':['consumable'],
        'use':'dial',
        'break':100,
        'cost':(5000, 2500, 2500),
    },
    'ring of porfic':{
        'name':'RING\xa0PORFIC',
        'namejp':'盾の指輪',
        'unknown':'?RING',
        'unknownjp':'?指輪',
        'image':['item/ring1','item/ring1'],
        'type':['misc'],
        'slot':['trinket'],
        'ac':1,
        'use':'porfic',
        'break':5,
        'cost':(10000, 5000, 5000),
    },
    'amulet of manifo':{
        'name':'AMULET\xa0MANIFO',
        'namejp':'彫像の護符',
        'unknown':'?AMULET',
        'unknownjp':'?護符',
        'image':['item/statue1','item/trinket49'],
        'type':['misc'],
        'slot':['trinket'],
        'class':['priest'],
        'use':'manifo',
        'break':10,
        'cost':(15000, 7500, 7500),
    },
    'rod of flame':{
        'name':'ROD\xa0FLAME',
        'namejp':'炎の魔法棒',
        'unknown':'?ROD',
        'unknownjp':'?棒',
        'image':['item/staff7','item/staff1'],
        'type':['misc'],
        'slot':['trinket'],
        'class':['mage', 'bishop', 'samurai'],
        'use':'mahalito',
        'break':10,
        'protect':['fire'],
        'cost':(25000, 12500, 12500),
    },
    'amulet of makanito':{
        'name':'AMULET\xa0MAKANITO',
        'namejp':'致死の護符',
        'unknown':'?AMULET',
        'unknownjp':'?護符',
        'image':['item/statue1','item/trinket49'],
        'type':['misc'],
        'slot':['trinket'],
        'use':'makanito',
        'break':5,
        'cost':(20000, 10000, 10000),
    },
    'potion of badial':{
        'name':'\xa2\xa0DIAL',
        'namejp':'\xa2征伐',
        'unknown':'?\xa2',
        'unknownjp':'?\xa2',
        'image':['item/scroll1','item/scroll1'],
        'type':['scroll'],
        'slot':['consumable'],
        'use':'badial',
        'break':100,
        'cost':(8000, 4000, 4000),
    },
    'ring of healing':{
        'name':'RING\xa0HEALING',
        'namejp':'回復の指輪',
        'unknown':'?RING',
        'unknownjp':'?指輪',
        'image':['item/ring2','item/ring1'],
        'type':['misc'],
        'slot':['trinket'],
        'heal':1,
        'cost':(300000, 150000, 150000),
    },
    'priests ring':{
        'name':'PRIESTS RING',
        'namejp':'僧侶の指輪',
        'unknown':'?RING',
        'unknownjp':'?指輪',
        'image':['item/ring3','item/ring1'],
        'type':['misc'],
        'slot':['trinket'],
        'protect':['undead'],
        'cost':(500000, 250000, 250000),
    },
    'ring of death':{
        'name':'RING\xa0DEATH!',
        'namejp':'死の指輪!',
        'unknown':'?RING',
        'unknownjp':'?指輪',
        'image':['item/ring4','item/ring1'],
        'type':['misc'],
        'slot':['trinket'],
        'heal':-3,
        'cost':(500000, 0, 250000),
    },
    'amulet of werdna':{
        'name':'AMULET\xa0WERDNA',
        'namejp':'ワードナの護符',
        'unknown':'?AMULET',
        'unknownjp':'?護符',
        'image':['item/potion2','item/trinket34'],
        'type':['misc'],
        'slot':['trinket'],
        'alignment':['good', 'neutral'],
        'ac':10,
        'heal':5,
        'protect':['fighter','mage','priest','thief','humanoid','giant','dragon','beast','undead','demon','insect','fire','cold','poison','paralysis','petrify','drain','critical',],
        'acunmatch':-1,
        'cursedunmatch':True,
        'use':['malor', 'groupheal'],
        'break':0,
        'cost':(0, 0, 0),
        'cursed':False,
    },
    'statue of bear':{
        'name':'STATUE\xa0BEAR',
        'namejp':'熊の彫像',
        'unknown':'?STATUE',
        'unknownjp':'?彫像',
        'image':['item/statue2','item/statue2'],
        'type':['quest'],
        'slot':['quest'],
    },
    'statue of frog':{
        'name':'STATUE\xa0FROG',
        'namejp':'蛙の彫像',
        'unknown':'?STATUE',
        'unknownjp':'?彫像',
        'image':['item/statue3','item/statue3'],
        'type':['quest'],
        'slot':['quest'],
    },
    'key of bronze':{
        'name':'KEY\xa0BRONZE',
        'namejp':'銅の鍵',
        'unknown':'?KEY',
        'unknownjp':'?鍵',
        'image':['item/key1','item/key1'],
        'type':['quest'],
        'slot':['quest'],
    },
    'key of silver':{
        'name':'KEY\xa0SILVER',
        'namejp':'銀の鍵',
        'unknown':'?KEY',
        'unknownjp':'?鍵',
        'image':['item/key1','item/key1'],
        'type':['quest'],
        'slot':['quest'],
    },
    'key of gold':{
        'name':'KEY\xa0GOLD',
        'namejp':'金の鍵',
        'unknown':'?KEY',
        'unknownjp':'?鍵',
        'image':['item/key1','item/key1'],
        'type':['quest'],
        'slot':['quest'],
    },
    'blue ribbon':{
        'name':'BLUE RIBBON',
        'namejp':'青リボン記章',
        'unknown':'?RIBBON',
        'unknownjp':'?リボン',
        'image':['item/trinket1','item/trinket43'],
        'type':['quest'],
        'slot':['quest'],
        'lore':'狂王に認められた証。-関係者専用の-直通エレベータを-使用することが-できるようになる。',
    },
}

